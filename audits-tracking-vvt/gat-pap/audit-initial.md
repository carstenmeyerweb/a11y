## Dev structurels

### 1. Limite de temps de session - Bloquant

Source : paragraphe 11, p. 16

**Solutions Temesis :**

● Soit permettre à l’utilisateur de supprimer la limite de temps 

● Soit permettre à l’utilisateur d’augmenter la limite de temps 

● Soit la limite de temps avant la fin de la session est de vingt heures au moins

Prévoir un module de prolongation de session avec une popin ou une alerte qui s'intercale au dessus du contenu principal :

![audit-initial-reco-session](C:\laragon\www\a11y\audits-tracking\vivaticket\gat-pap\assets\audit-initial-reco-session.png)



On passe le panier à 20 h de validité en paramétrage possible

Idée : une popin qui alerte que les produits du panier vont être supprimés et que l'utilisateur peut prolonger la session par un bouton.






### 2. Fichiers justificatif de vente et billets - Bloquant

Source : paragraphe 12, p 17, paragraphe 19, p32

**Solutions Temesis :**

● Soit rendre les PDF accessibles, soit en proposer une alternative accessible au format .docx ou HTML. 

● L’alternative des billets, au format HTML, n’est pas accessible. 

● L’alternative du justificatif de vente pourrait être la page web depuis laquelle il est téléchargeable. Il faudrait que cette page reprenne la totalité des informations présentes dans le PDF et qu’elle soit elle-même accessible. Ce qui n’est pas le cas aujourd’hui.

**Question à Temesis :** fichiers HTML bruts rendus totalement accessibles vraiment impossible en téléchargement ici ?

Adrien justif : https://jira.bestunion.com/browse/GAT-7889

Adrien billet : https://jira.bestunion.com/browse/GAT-7922

Liés au ticket BNF : https://jira.bestunion.com/browse/GAT-5014




### 3. Récapitulatif de commande - Majeur

Source : paragraphe 13, p 21

![Tableaux de récap](assets/audit-initial-reco-recap.png)



**Problèmes :**

● 5.5 - Majeur : Le titre  de chaque tableau est identique : « Récapitulatif de votre commande »

● 5.6 - Majeur : La première colonne du tableau est vide (l’icône est masquée aux TA) lorsque l’icône décorative est affichée



**Solutions Temesis :**

● Soit n’utiliser qu’un seul tableau pour récapituler les produits dans le panier, soit distinguer les titres des tableaux pour qu’ils soient contextualisés 

● Inclure le picto dans la colonne « Visite guidée » et supprimer la première colonne vide





Solution VVT : mettre caption dynamique nom de famille

**Question à Temesis :** solution ARIA pour masquer des TD inutiles aux aveugles ?


### 4. Calendrier accessible - Majeur et bloquant

Source : paragraphe 16, p 26

![assets/](C:\laragon\www\a11y\audits-tracking\vivaticket\gat-pap\assets\audit-initial-reco-cal-a11y.png)



![](C:\laragon\www\a11y\audits-tracking\vivaticket\gat-pap\assets\audit-initial-reco-cal-a11y-id.png)



11.1 - Bloquant : Erreur dans la liaison des étiquettes à leurs 

11.10 - Majeur : Absence d’indication visuelle indiquant le caractère obligatoire des champs 

11.5 - Majeur : Le champ « Jours disponible » n’est pas regroupé avec les champs « Choix du mois » et « Choix de l’année » 

12.8 - Majeur : Après la sélection du jour avec le champ « Jours disponible », le focus n’est pas déplacé 



A voir : nouvelle vue séance - boutons radio à côté du modèle grille et liste

**JIRA pour Adrien :** https://jira.bestunion.com/browse/GAT-7807



**Solutions Temesis**

● Rétablir la valeur de l’id du champ concerné dans l’attribut for des étiquettes à associer (normalement fait par Laura)

● Inclure le champ « Jours disponible » dans le regroupement (normalement fait par Laura)

● Après le choix d’un jour via le champ « Jours disponible » déplacer le focus en JavaScript sur le premier bouton radio permettant de choisir une séance

Question à Temesis : OK pour nous


### 5. Emails de service - Majeur

Source : paragraphe 25, p 38

6.1 - Majeur : L’intitulé et l’attribut title du lien « Vivaticket » de l’entête ne reprennent pas son intitulé visible

Solution Temesis :

* Modifier l’attribut alt de l’image et l’attribut title du lien de sorte qu’ils reprennent au moins l’intitulé visible : « Vivaticket »

* Supprimer les p vides 

* Ajouter un attribut role=”presentation” sur les tableaux 

   utilisés dans les cellules du tableau de récapitulatif de commande







**Jira à prévoir une fois le template réalisé**

## Dev front GAT

### Messages d'erreurs

#### Messages d'erreurs du tunnel

Les messages intégrés dans les div#error[role="alert"] ne sont pas sémantiques

##### Problèmes

Le message d'erreur n'est pas dans une balise sémantique, de plus il  contient des doubles <br> pour simuler des paragraphes.

###### Cas d'un message long :

```
<div id="error" role="alert">
  Ce type de billet ne peut pas être vendu seul : Enfant < 4 ans
  <br>
  Vous devez ajouter des éléments au panier : Adulte
  <br>
  <br>
  Vous devez ajouter des éléments au panier.
</div>
```

###### Cas d'un message court :

```
<div id="error" role="alert">
 
    Vous devez ajouter des éléments au panier.    

</div>
```

##### Solutions

Systématiser les paragraphes même pour une seule phrase.
 Supprimer les doubles <br> pour simuler le passage d'un paragraphe à un autre.

###### Cas d'un message long :

```
<div id="error" role="alert">
  <p>  
    Ce type de billet ne peut pas être vendu seul : Enfant < 4 ans
    <br>
    Vous devez ajouter des éléments au panier : Adulte
  </p>
  <p>
    Vous devez ajouter des éléments au panier.
  </p>
</div>
```

###### Cas d'un message court :

```
<div id="error" role="alert">
  <p>  
    Vous devez ajouter des éléments au panier.    
  </p>
</div>
```

 **JIRA pour Laura** : https://jira.bestunion.com/browse/GAT-7751



### Page sélection de produits

Critère 11.4 du RGAA 4.1 (à creuser à la restitution de l'audit)

Remplacer l’attribut aria-label sur chaque champ de sélection de quantité par un attribut title :

```HTML
<!-- Avant -->
<select aria-label="Sélectionnez la quantité de Adulte 10,20 €">
<!-- Après -->
<select title="Sélectionnez la quantité de Adulte 10,20 €">
```

**JIRA pour Laura** : https://jira.bestunion.com/browse/GAT-7750



### Page calendrier avec séances

#### Jour visuel div.day

1. Jours inactifs et full : supprimer l'attribut tabindex="0" et aria-checked="false"

2. Jours cliquables : modifier l'attribut aria-checked="true/false" en aria-pressed="true/false"

3. Seconder l'information liée à la couleur sur les éléments sélectionnés ou à sélectionner

   1. Mois en cours :
      Ajouter un title pour expliciter qu'il s'agit du mois en cours 
      `<button class="monthChooser active" data-month="12" title="mois sélectionné">Déc.</button>`

   2. Jour sélectionné :

      Ajouter la balise strong pour le jour selectionné

      `<div class="dayDate"><strong>16</strong></div>`

      Ajouter un style dans le module annulant la mise en forme pour éviter des regressions d'affichage

      `.dayDate > strong { font-weight: normal; }`

   3. Séance sélectionnée :
      Ajouter la balise strong pour l'horaire sélectionné

      `<div class="hour"><i class="fa fa-clock-o" aria-hidden="true"></i>`
      `  <strong>Séance de <span>09:00</span></strong> `
      `</div>`
      
      Ajouter un style dans le module annulant la mise en forme pour éviter des regressions d'affichage
      
      `.day-body .hour > strong { font-weight: normal; }`


#### Jour version liste déroulante

Reco Temesis : pour chaque champ obligatoire, fournir une indication visuelle l’indiquant. Par exemple un texte dans l’étiquette ou un astérisque. Si un astérisque est utilisé, il doit être expliqué au début du formulaire (visuellement et dans l’ordre du code). Par exemple « * champs obligatoires » 

1. Ajouter une mention : les champs avec un * sont obligatoires
2. Ajouter des * devant tous les labels de champs 
3. Inclure le champ « Jours disponible » dans le regroupement 
4. Erreur W3C : supprimer l'attribut name sur la div.form-a11y
5. Erreurs W3C : ajouter une option avec valeur vide et l'étiquette choisir pour chaque champ select avec attribut  required
5. Erreurs W3C : dynamiser les for des labels de selects avec les id dynamiques correspondants (pour gérer le calendrier accordéon)

##### JIRA pour Laura : https://jira.bestunion.com/browse/GAT-7712

##### Panier

1. Dynamiser le caption du tableau

   `<caption class="sr-only">Récapitulatif de votre commande <strong>Votre visite</strong></caption>`

2. Gérer le titre div.ticket-info avec l'insertion d'un <h2> :

   `<div class="ticket-info"><h2>Votre sélection</h2></div>`

3. Supprimer les attributs colspan="0" dans le <th> du prix

4. Supprimer le premier <th> et les premiers <td> du tableau et déplacer le <i> avec l'icône billet dans le td texte produit

5. Gérer les CSS pour compenser le changement de structure :

    - Espace de gauche 1er <th> : mettre un padding-left de 1.75rem

    - Gestion du titre du panier : 

      `.ticket-info h2 {
          margin: 0;
          color: #fff;
          font-size: 0.813rem;
          font-weight: normal;
      }`

   

#### Tests

Repasser la page sur validator.w3.org jusqu'à la disparition des messages de type "Error"

##### JIRA pour Laura : https://jira.bestunion.com/browse/GAT-7717



### Confirmation de commande

Jira fait : https://jira.bestunion.com/browse/GAT-7760







## Dev front Dagger

### Divers éléments d'interface

#### Bordure de champ à #ccc non conforme, à passer à #8C8C8C

* à faire

#### Spinner de chargement à #969796 non conforme, à passer à #404140 et la zonne claire à #919291

* Fait sur Dagger : jangofett + GATPAP le 10/01/2022

  



### Chemin de fer de tunnel

#### Critère non conforme : 10.11 

https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode-rgaa/criteres/#crit-10-11

![audit-initial-reco-tunnel-top](C:\laragon\www\a11y\audits-tracking\vivaticket\gat-pap\assets\audit-initial-reco-tunnel-top.png)



Fait : à remonter dans jango



### Calendrier

Seconder l'information liée à la couleur sur les éléments sélectionnés ou à sélectionner avec des pictogrammes insérés en CSS 



### Confirmation de commande 



#### Anomalies

1. Tableau de commande dans le Payment Module : HTML

   - Modifier les <td> de gauche en <th> avec un scope="row" comme dans le tableau des totaux du récap

2. Tableau de commande dans le Payment Module : CSS mobile
   Revoir le CSS en mobile pour passer les th et les td en display: block pour que tout rentre en 320px de large

   Exemple : le CSS du nouveau Ruinart 
   Tester le reflow sur https://www.w3.org/TR/WCAG21/#reflow avec l'ARC toolkit de Google Chrome

   ![](C:\laragon\www\a11y\audits-tracking\vivaticket\gat-pap\assets\audit-initial-reco-confirmation-com.png)
   
3. Téléchargements des documents : HTML + CSS éventuel

Ajouter un contenu dans l’en-tête de colonne, masqué visuellement, ajoutant du contexte pour les aveugles. Par exemple : « Documents de votre commande »

![audit-initial-reco-confirm](C:\laragon\www\a11y\audits-tracking\vivaticket\gat-pap\assets\audit-initial-reco-confirm.png)







**JIRA pour Laura :** https://jira.bestunion.com/browse/GAT-7760

## Contribution CMS

### Page familles



### Page produits



### Pages annexes







## Récap des non conformités

1. 1.1 - Chaque image porteuse d’information a-t-elle une alternative textuelle ? NC
2. 3.1 - Dans chaque page web, l’information ne doit pas être donnée uniquement par la
couleur. Cette règle est-elle respectée ? NC
3. 3.3 - Dans chaque page web, les couleurs utilisées dans les composants d’interface ou
les éléments graphiques porteurs d’informations sont-elles suffisamment contrastées
(hors cas particuliers) ? NC
4. 5.3 - Pour chaque tableau de mise en forme, le contenu linéarisé reste-t-il
compréhensible ? NC
5. 5.5 - Pour chaque tableau de données ayant un titre, celui-ci est-il pertinent ? NC
6. 5.6 - Pour chaque tableau de données, chaque en-tête de colonnes et chaque en-tête
de lignes sont-ils correctement déclarés ? NC
7. 6.1 - Chaque lien est-il explicite (hors cas particuliers) ? NC
8. 7.1 - Chaque script est-il, si nécessaire, compatible avec les technologies d’assistance
? NC
9. 8.1 - Chaque page web est-elle définie par un type de document ? NC
10. 8.2 - Pour chaque page web, le code source généré est-il valide selon le type de
document spécifié (hors cas particuliers) ? NC
11. 8.3 - Dans chaque page web, la langue par défaut est-elle présente ? NC
12. 8.5 - Chaque page web a-t-elle un titre de page ? NC
13. 8.8 - Dans chaque page web, le code de langue de chaque changement de langue
est-il valide et pertinent ? NC
14. 8.9 - Dans chaque page web, les balises ne doivent pas être utilisées uniquement à
des fins de présentation. Cette règle est-elle respectée ? NC
15. 9.1 - Dans chaque page web, l’information est-elle structurée par l’utilisation
appropriée de titres ? NC
16. 9.2 - Dans chaque page web, la structure du document est-elle cohérente (hors cas
particuliers) ? NC
17. 9.3 - Dans chaque page web, chaque liste est-elle correctement structurée ? NC
18. 10.11 - Pour chaque page web, les contenus peuvent-ils être présentés sans avoir
recours à la fois à un défilement vertical pour une fenêtre ayant une hauteur de
256px, ou à un défilement horizontal pour une fenêtre ayant une largeur de 320px
(hors cas particuliers) ? NC
19. 10.13 - Dans chaque page web, les contenus additionnels apparaissant à la prise de
focus ou au survol d’un composant d’interface sont-ils contrôlables par l’utilisateur
(hors cas particuliers) ? NC
20. 11.1 - Chaque champ de formulaire a-t-il une étiquette ? NC
21. 11.5 - Dans chaque formulaire, les champs de même nature sont-ils regroupés, si
nécessaire ? NC
22. 11.7 - Dans chaque formulaire, chaque légende associée à un regroupement de
champs de même nature est-elle pertinente ? NC
23. 11.10 - Dans chaque formulaire, le contrôle de saisie est-il utilisé de manière
pertinente (hors cas particuliers) ? NC
24. 12.6 - Les zones de regroupement de contenus présentes dans plusieurs pages web
(zones d’en-tête, de navigation principale, de contenu principal, de pied de page et de
moteur de recherche) peuvent-elles être atteintes ou évitées ? NC
25. 12.8 - Dans chaque page web, l’ordre de tabulation est-il cohérent ? NC
26. 13.1 - Pour chaque page web, l’utilisateur a-t-il le contrôle de chaque limite de temps
modifiant le contenu (hors cas particuliers) ? NC
27. 13.3 - Dans chaque page web, chaque document bureautique en téléchargement
possède-t-il, si nécessaire, une version accessible (hors cas particuliers) ? NC
