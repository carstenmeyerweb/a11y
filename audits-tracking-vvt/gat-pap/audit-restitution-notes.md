Questions - réponses avec Aurélien

Bonjour Carsten,

 

1. Limite     de temps : techniquement, nous pourrons la prolonger à 20 heures mais     nous savons que pour nos clients cette notion risque de surprendre. Quels     sont les aménagements que vous conseillez habituellement pour le cas de la     vente en ligne ?

*une fenêtre modale (conforme) qui apparaît 30s avant expiration de la session qui laisse la possibilité de la prolonger*



Ok pour durée de panier limitée

Idée : une popin qui alerte que les produits du panier vont être supprimés et que l'utilisateur peut prolonger la session par un bouton.

 

1. Justificatifs     de vente et billets : nous pourrons effectuer les corrections pour     les versions HTML mais est-ce que le principe d’avoir ces fichiers     exclusivement en téléchargement et en pièces jointes des emails est-il     conforme au RGAA ?

*ce n'est pas idéal coté usage de mon point de vue mais conforme*

1. Calendrier     visuel - seconder l’info liée à la couleur : est-ce qu’une     solution exclusivement CSS d’ajout de picto ou de forme suffit ?

*je n'ai pas le cas sous les yeux mais sur le principe il faut mise en forme différent donc oui picto, bordure, soulignement, gras ou autre traitement graphique + si il s'agit d'indiquer le jour actif il y a l'attribut aria-current="true" sinon cela passera par un intitulé différent*

1. Titrage     en page d’accueil et produits dans le cas d’un <h1> optionnel dans     le CMS

*je laisserai gaetan échanger avec vous sur ce point* 

 

1. Nous avons des champs select de quantités de produits sans labels associés mais     avec un aria-label qui sortent en non-conformes : est-ce nécessaire     de doubler avec un title ?

*oui le title est obligatoire à minima et le aria-label peut etre supprimer dans ce cas* 

 

 

Deux points techniques :

1. Notre     url de site est passée de https://votresite-test.tickeasy.com à https://votresite-sandbox.tickeasy.com     pour des raisons d’hébergement indépendantes à l’audit mais nous allons la     rétablir.
2. Nous     aimerions avoir la grille d’évaluation complète avec les non-conformités     pour toutes les pages pour ne pas passer à côté d’une anomalie.     Actuellement nous n’avons qu’une page de synthèse dans la grille.

*Nous utilisons généralement la grille que pour le calcul du taux d'où le fait que nous produisons uniquement une grille simple, les non conformité par page sont présentées dans le rapport détaillé*





Mode reflow : word-break





Astérisque : au dessus de sélectionner une date



Aria-hidden sur le calendrier visuel

<p>nbsp;</p> display none