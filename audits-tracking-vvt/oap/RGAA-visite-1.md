# Vérification de l'accessibilité

Application web testée : **GTS piscines** (OAP)

Référentiel utilisé : [RGAA 4.1](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode-rgaa/criteres)

Date de visite : 21/12/2021
Auditeur : Carsten MEYER

## Connexion

Slug : /backoffice/login

### Problèmes détectés :

* Zoom texte à 200% sur Firefox : défaut de lisibilité des étiquettes de champ
* Champs obligatoires : ajouter une mention "* Champs obligatoires" sous le titre "Se connecter"
* Intitulé de l'étiquette Email : utiliser une étiquette plus explicite, donnant le format comme par exemple "Adresse e-mail, par exemple mon-email@example.com", cette indication peut être déportée dans un bloc à part relié par un aria-describedby.
* Rattacher les messages d'erreurs spécifiques aux inputs par un aria-describedby ou utiliser l'API HTML5
* Bouton voir le mot de passe : placer le libellé sr-only dans la balise button
* Conserver le message d'erreur global dans un paragraphe avec le role alert. 



## Accueil connecté

Slug : /backoffice/home

### Problèmes détectés

* Menu ouvert : prévoir un bouton de fermeture du menu

* Prévoir un menu d'évitement donnant accès au <main> de la page et à la recherche

* Tous les formulaires : retours d'erreurs mal balisés pour l'accessibilité avec les composants Vuetify

  

Travail à continuer avec un accès préprod

## Conclusion

**Points positifs :**

* Bons contrastes de textes et de composants pour l'accessibilité
* Bonne syntaxe HTML de base



**Points négatifs :** 

* Point bloquant : la librairie Vuetify n'est pas adaptée pour un codage accessible des formulaires et de certains composants
* Besoin d'un environnement de préprod pour continuer l'accompagnement : la solution Ngrok temporaire dépanne, mais à terme je vais avoir besoin d'une préprod donnant un instantané de l'appli à tester.
