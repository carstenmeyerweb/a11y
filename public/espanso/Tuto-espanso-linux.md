# Espanso



## Doc

Docs en ligne : https://espanso.org/docs/get-started/

Spécifique linux : https://www.makeuseof.com/type-faster-increase-productivity-using-espanso/

## Premiers pas

$ espanso start : démarre espanso

$ espanso restart : redémarrage

$ espanso status : vérifie si espanso tourne

### Raccourcis clavier



alt + Espace : ouvre la recherche

## Configuration



$ espanso edit : ouvre le fichier de configuration via le terminal

$ espanso path : donne le chemin



### Bugs connus sur Ubuntu FR

#### Pas d'accès clavier aux expansions

Si espanso ne fonctionne pas quand on tape par exemple :date dans un fichier texte, vérifier si le clavier FR est bien pris en compte.

Pour corriger, en ligne de commande aller dans :

$ espanso edit /home/koena/.config/espanso/default.yml

Éditer le fichier **default.yml** en ajoutant :

```json
keyboard_layout: { layout: fr }
```

#### Fenêtre de recherche espanso qui ouvre un menu fenêtre

Aller dans les paramètres d'Ubuntu > Clavier > Voir et personnaliser les raccourcis clavier > Fenêtres > Activer le menu fenêtre > Supprimer 

