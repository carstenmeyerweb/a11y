/*
*   This content is licensed according to the W3C Software License at
*   https://www.w3.org/Consortium/Legal/2015/copyright-software-and-document
*
*   File:   menubar-2-init.js
*
*   Desc:   Creates a menubar to control the styling of text in a textarea element
*/

window.addEventListener('load', function () {
  var menubar = new Menubar(document.getElementById('menubar1'));
  menubar.init();
});

