// The autoComplete.js Engine instance creator

// Sites Autocomplete
const autoCompleteJSON = new autoComplete({
        name: "departements",
        selector: "#autoCompleteS1",
        data: {
                src: async function () {
                        // Loading placeholder text
                        document.querySelector("#readDepartements .auto-complete").setAttribute("placeholder", "Loading...");
                        // Fetch External Data Source
                        const source = await fetch("json/departements.json");
                        const data = await source.json();
                        // Post Loading placeholder text
                        document.querySelector("#readDepartements .auto-complete").setAttribute("placeholder", autoCompleteJSON.placeHolder);
                        // Returns Fetched data
                        return data;
                }
        },
        trigger: {
                event: ["input", "focus"],
        },
        placeHolder: "Chercher un département",
        searchEngine: "strict",
        highlight: true,
        maxResults: 15,
        resultItem: {
                content: (data, element) => {
                        // Prepare Value's Key
                        const key = Object.keys(data).find((key) => data[key] === element.innerText);
                        // Modify Results Item
                        element.style = "display: flex; justify-content: space-between;";
                        element.innerHTML = `<span style="text-overflow: ellipsis; white-space: nowrap; overflow: hidden;">
              ${element.innerHTML}</span>`;
                },
        },
        noResults: (dataFeedback, generateList) => {
                // Generate autoComplete List
                generateList(autoCompleteJSON, dataFeedback, dataFeedback.results);
                // No Results List Item
                const result = document.createElement("li");
                result.setAttribute("class", "no_result");
                result.innerHTML = `<span style="display: flex; align-items: center; font-weight: 100; color: rgba(0,0,0,1);">Pas de départements pour "${dataFeedback.query}"</span>`;
                document.querySelector(`#${autoCompleteJSON.resultsList.idName}`).appendChild(result);
                document.querySelector("#readDepartements .auto-complete").setAttribute("aria-invalid", "true");
        },
        onSelection: (feedback) => {
                document.querySelector("#readDepartements .auto-complete").blur();
                // Prepare User's Selected Value
                const selection = feedback.selection.value;
                // Render selected choice to selection div
                document.querySelector("#readDepartements .selection").innerHTML = selection;
                // Replace Input value with the selected value
                document.querySelector("#readDepartements .auto-complete").value = selection;
                document.querySelector("#readDepartements .auto-complete").setAttribute("aria-invalid", "false");
        },
});
