# Anomalies a11y Koena.net

1. TAC : non valide W3C

2. Form Newsletter : champ mail doit donner des conseils de saisie explicite

3. Form Contact : champ nom sans auto-complete given-name

4. Zoom texte 200% : champ url difficile à utiliser, colonnage à revoir, Diagnostic gratuit en diagonale tronqué, quelques rapprochements dans le menu footer, en anglais, "See the text version of the video"

4. Dates abrégées : est-ce FALC ?

4. Liens "Voir toutes les actus" sans balise p englobante

4. Menu principal pas étiqueté comme nav principale avec un aria-label

4. aria-expanded="false" sur le menu desktop pourtant bien ouvert

4. Tester le retour en haut de page

   
   
   