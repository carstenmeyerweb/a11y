# Suivi et recommandations d'accessibilité

## Dictionnaire de l'Académie française 

**Module de recherche avancée** 

Itération : 4

Date d'inspection : 25/11/2022

Url : http://testv4.dictionnaire-academie.fr/

Auditeur : [Carsten Meyer](mailto:cm@mobyou.net)

Norme : Référentiel Général d'Amélioration de l'Accessibilité (RGAA) 4.1 - équivalent WCAG niveau AA



## Contexte

Suite aux modifications profondes de la structure HTML, le code JavaScript et CSS a été corrigé et factorisé pour intégrer les recommandations faites en démo avec les fichiers a11y.js et a11y.css.

De nombreuses difficultés d'adaptations apparaissent, les scripts initiaux n'étant pas prévus pour l'accessibilité :

* De nombreux mécanismes d'interactions sont appliqués sur des éléments inertes : 

  * Titres h4,
  * Élément de liste li,
  * Élément span.

* Les mécanismes de déploiement des listes déroulantes multiniveaux n'utilisent pas un masquage complet : les éléments sont cachés mais perceptibles aux non-voyants et toujours présents dans les éléments tabulables auxclavier.

  



### Ouverture/fermeture du bloc de recherche avancée

#### Code HTML

✅**Corrigé** et conforme.

#### Code JavaScript

**Partiellement** conforme.

Le JavaScript fonctionne visuellement mais il doit également permuter le paramètre aria-expanded du bouton comme ceci :

* si fermé : aria-expanded="false"
* si ouvert : aria-expanded="false"

❌**Non corrigé**



La gestion de la touche Échap ne me semble pas nécessaire ici vu qu'il s'agit d'un volet complet de configuration.

✅**Corrigé**



### Liste des domaines

#### Code HTML

✅**Corrigé** et conforme aux attentes que ce soit en niveau 1 ou 2 de hiérarchie.



#### Code JavaScript

**Partiellement** conforme.

Éléments restant à corriger pour le premier niveau et le second :

1. La fermeture de la liste doit provoquer un masquage complet avec la propriété CSS display à "none". Tant que ce mécanisme n'est pas corrigé, la navigation au clavier est confuse et laborieuse.

   ✅**Corrigé**

2. La touche Echap doit être effective sur l'ouverture du premier niveau : voir avec le script que j'ai proposé mais il faudrait annuler un script créé par Diagonal qui renvoie le curseur tout en haut dans la page.

   ❌**Non corrigé**
   
   

### Volet de résultats

#### Code HTML

✅**Corrigé** et conforme



#### Code JavaScript

**Partiellement** conforme.

Éléments restant à corriger pour le premier niveau et le second :

1. Le paramètre aria-expanded est bien modifié selon l'état ouvert ou fermé mais il est inversé, il doit fonctionner comme ceci :
   1. Panneau fermé : aria-expanded="false"
   2. Panneau ouvert : aria-expanded="true"
   
   ❌**Non corrigé**
   
2. La gestion du focus : quand on vient de la recherche avancée, le focus doit être amené sur le conteneur #resultatRechAvancee auquel il faudra appliquer un tabindex à -1 avant de pointer dessus avec la méthode $( "#resultatRechAvancee" ).focus();
   ❌**Non corrigé**

## Fenêtres de dialogue en cas d'erreur

### Anomalie

Lorsqu'une alerte apparaît, le focus ne se positionne pas sur celle-ci.

❌**Non corrigé**

### Solution proposée

Le composant Dialog de la librairie JQuery UI est accessible. 

Nous recommandons cet exemple : 
https://jqueryui.com/dialog/#modal-form



## Qualité du code

Le passage au validateur du W3C nous indique environ 124 erreurs à corriger. 

Erreurs les plus gênantes pour l'accessibilité :

#### Identifiants dupliqués

Duplicate ID `ulSousDomaines10`.

Duplicate ID `ulSousDomaines14`.

Duplicate ID `separateurMenu`.

Duplicate ID `chiffre`. (3)

Duplicate ID `imgInfoRecherche`. (4)

Duplicate ID `bandeTitrePartage`.

Duplicate ID `preHaut`.

Duplicate ID `preBas`.

Duplicate ID `typoPetit`.

Duplicate ID `typoMoyen`.

Duplicate ID `typoGrand`.

❌**Non corrigé**

#### Image

Une image de décoration est dépourvue d'alternative :

```html
<img id="imgInfoRecherche" src="images/rechercheInfo@3x.png" width="30" height="30">
```

Correction proposée :

Un attribut alt vide permettra de ne pas vocaliser cette image de décoration

```html
<img alt="" id="imgInfoRecherche" src="images/rechercheInfo@3x.png" width="30" height="30">
```

❌**Non corrigé**

Les autres erreurs peuvent être corrigées et vérifiées avec le validateur W3C https://validator.w3.org



## Conclusion : avis de l'auditeur

Le module de recherche avancée n'est pas encore totalement conforme aux critères du RGAA.
La démarche de correction en fin de développement est toujours plus chronophage quand il s'agit de rendre un travail accessible.

Malgré ces correctifs en attente, on constate que les listes de domaines à 2 niveaux sont désormais utilisables par les personnes non-voyantes. 

De nombreuses interactions qui ne fonctionnaient qu'au pointeur sont désormais accessibles au clavier pour une accessibilité aux personnes non-voyantes et aux personnes à mobilité réduite qui n'utilisent pas la souris. 





