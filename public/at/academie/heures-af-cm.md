## Feuille de temps
Accompagnement pour mise en accessibilité du module de recherche avancée du site de l'Académie Française.

Auditeur : Carsten MEYER


| Date       | Temps | Type                         | Détail                                                       |
| ---------- | ----- | ---------------------------- | ------------------------------------------------------------ |
| 2022-06-27 | 2     | Analyse du module            | Analyse des points majeurs d'inaccessibilité. Tests et préparation du la réunion. |
| 2022-06-28 | 1     | Réunion initialisation       | Détermination des points prioritaires à corriger : démonstrations et détermination des mécanismes de navigation au clavier attendus |
| 2022-06-28 | 3     | Reco écrite en PDF           | Première reco écrite                                         |
| 2022-07-01 | 0.5   | Email                        | Première analyse des corrections effectuées par Diagonal avant point d'étape |
| 2022-07-01 | 0.75  | Réunion d'étape              | Retours sur les premières corrections, détermination des nouveaux correctifs et résolutions pour les régressions occasionnées |
| 2022-07-01 | 2     | Analyse et programmation     | Amélioration du code HTML, JavaScript et CSS, point de suivi Google Sheet |
| 2022-07-05 | 4     | Programmation                | Programmation d'exemple de code adapté aux composants liste et sous-listes, ouverture de la boîte principale et fermeture avec 2 cibles, liste des résultats. Code HTML, JavaScript et CSS |
| 2022-07-05 | 2     | Reco écrite en PDF           | Recommandation documentée avec le code proposé et les solutions accessibles. |
| 2022-07-06 | 1     | Correctif HTML et JavaScript | Correction du script JavaScript pour meilleure adaptation.   |
| 2022-07-11 | 1     | Visio                        | Point avant congés                                           |
| 2022-08-29 | 0.5   | Visio avec Daniel            | Debrief avec des Daniel des avancées et difficultés d'intégration |
| 2022-09-09 | 3     | Codage JS                    | Transposition du JavaScript de démo dans le JavaScript en JQuery du projet |
| 2022-09-13 | 0.5   | Visio avec Daniel et Wanda   | Suivi des corrections d'intégration JavaScript et CSS        |
| 2022-09-14 | 2     | Codage JavaScript et CSS     | Correctifs des regressions sur le menu de langue, le volet de résultat (nouveau ciblage suite au changement de l'HTML) et transposition du script écoutant la touche Echap en JQuery |
| 2022-09-16 | 1     | Recommandation écrite        | Point d'avancement                                           |
| 2022-11-25 | 0.75  | Recommandation écrite        | Point d'avancement final                                     |
| Total      | 22.50 |                              |                                                              |
