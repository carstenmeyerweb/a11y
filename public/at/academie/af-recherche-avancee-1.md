# Recommandations d'accessibilité

## Dictionnaire de l'Académie française Module de recherche avancée 

Itération : 1

Date d'inspection : 28/06/2022

Url : http://testv4.dictionnaire-academie.fr/

Auditeur : [Carsten Meyer](mailto:cm@mobyou.net)

Norme : Référentiel Général d'Amélioration de l'Accessibilité (RGAA) 4.1 - équivalent WCAG niveau AA


## Contexte

Il s'agit d'une première inspection pour définir les correctifs prioritaires.
Dans un premier temps nous nous concentrons sur le bon fonctionnement de la navigation au clavier dans le module de recherche avancée.
La qualité et l'aspect sémantique du code seront également abordés car ils vont redéfinir la structure de ce code.
Des correctifs de style CSS vont permettre ce changement de structure HTML sans trop d'impact visuel.

## Navigation au clavier

### Liste des domaines

L'ouverture de la liste de domaines nécessite un bouton HTML en remplacement de la div.

#### Code actuel

* La div avec l'id "listeDomaine" constitue l'élément interactif à la souris, étant inerte, elle ne réagit pas au clavier;
* La div parente .menuPerso n'est qu'une balise enveloppante (d'un point de vue accessibilité).

```html
<div class="menuPerso" tabindex="0" role="button" name="rechDomaines" id="rechDomaines">
	<div id="listeDomaine"	title="Utilisez cette liste déroulante et hiérarchique pour effectuer une recherche basée sur les marqueurs de domaines (mathématiques, philosophie…). Vous pouvez utiliser les «&nbsp;super-domaines&nbsp;» pour effectuer des recherches groupées." class="active">Liste des domaines<span class="fa fa-chevron-right"></span></div>
```
#### Code proposé

* La div avec l'id "listeDomaine" est transformée en button pour lui donner une consistance au clavier;
* La class "disclosure" active un script générique pour assurer la restitution l'état du bouton (développé ou réduit) et le ciblage de l'élément à déplier;
* La touche "Escape" permet de refermer la liste à tout moment et de tabuler entre les différents types de recherches;
* On conserve le title d'origine très informatif;
* On ajoute un id "ulDomaines" à la liste à déplier, id qu'on cible avec aria-controls;
* La parente est inactivée avec un role à none.

```html
<div class="menuPerso" role="none" name="rechDomaines" id="rechDomaines">
	<button type="button" id="listeDomaine" class="disclosure" aria-expanded="false" aria-controls="ulDomaines" title="Utilisez cette liste déroulante et hiérarchique pour effectuer une recherche basée sur les marqueurs de domaines (mathématiques, philosophie…). Vous pouvez utiliser les «&nbsp;super-domaines&nbsp;» pour effectuer des recherches groupées.">Liste des domaines<span class="fa fa-chevron-right"></span></button>	
```
Voir également le JavaScript et le CSS proposés pour cette démo :

* [JavaScript du bouton d'ouverture](https://carstenmeyerweb.gitlab.io/a11y/at/academie/code/recherche-avancee-v1/js/a11y.js)
* [CSS adapté](https://carstenmeyerweb.gitlab.io/a11y/at/academie/code/recherche-avancee-v1/css/a11y.css)

#### Démonstration

[Exemple d'implémentation d'un bouton de type plier/déplier ou disclosure dans la recherche par domaine](https://carstenmeyerweb.gitlab.io/a11y/at/academie/code/recherche-avancee-v1/recherche_avancee_v1.html)

Il faudra appliquer le même traitement aux flèches ouvrant les sous-domaines. Intégration plus complexe à réaliser mais possible.

Idem pour les recherches par catégorie grammaticale et étymologique par langue.


## Sémantique

### Liste des Domaines

Le RGAA exige que les champs de même nature soient regroupés avec la balise fieldset

#### Code actuel

```html
<div id="divRechParDomaines">
  <h3>Recherche par domaines :</h3>
  <span id="checked_domain" style="display: none;"></span>
  <div class="menuPerso" tabindex="0" role="button" name="rechDomaines" id="rechDomaines">
    <div id="listeDomaine"
						title="Utilisez cette liste déroulante et hiérarchique pour effectuer une recherche basée sur les marqueurs de domaines (mathématiques, philosophie…). Vous pouvez utiliser les «&nbsp;super-domaines&nbsp;» pour effectuer des recherches groupées."
						class="active">Liste des domaines<span class="fa fa-chevron-right"></span></div>
    <ul class="">
```
#### Code proposé

```html
<div id="divRechParDomaines">
  <h3>Recherche par domaines :</h3>
  <fieldset>
	<legend class="sr-only">Liste hierarchique des domaines</legend>
	<span id="checked_domain" style="display: none;"></span>
	<div class="menuPerso" role="none" name="rechDomaines" id="rechDomaines">
		<button type="button" id="listeDomaine"
			class="disclosure" aria-expanded="false" aria-controls="ulDomaines" title="Utilisez cette liste déroulante et hiérarchique pour effectuer une recherche basée sur les marqueurs de domaines (mathématiques, philosophie…). Vous pouvez utiliser les «&nbsp;super-domaines&nbsp;» pour effectuer des recherches groupées.">Liste des domaines<span class="fa fa-chevron-right"></span></button>
        <ul class="" id="ulDomaines">...</ul>
      </div>
    </fieldset>
```
Ces regroupements par "fieldset" peuvent s'appliquer à certains champs de même nature ou de même thématique suivant un titre de niveau 3. À définir au cas par cas.

Voir la démonstration précédente pour vérifier le fonctionnement.

## Qualité du code

Le passage au validateur du W3C nous indique plus de 1000 erreurs à corriger. L'outil ne détaille que les 1000 premières erreurs.

Pour y voir plus clair et mieux repérer les erreurs qui posent des problèmes d'accessibilité il est nécessaire de corriger d'abord les erreurs les plus redondantes :

* L'attribut "access" dans les champs de case à cocher : il devient valide si on le préfixe avec data comme ceci :
```html
  	<input name="chk_domains" data-access="false" value="266" type="checkbox" class="indeterminate-checkbox" style="visibility: hidden;">
```

* Les étiquettes doivent être reliées à leurs champs respectifs avec un paramètre "for" pointant l'id de ce champ.
```html
  	<li class="niveauUn">
	  <input id="champ266" name="chk_domains" data-access="false" value="266" type="checkbox" class="indeterminate-checkbox" style="visibility: hidden;">
	  <label class="elementsListe" for="champ266">Acoustique</label>
	</li>
```



## Conclusion

Nous avons abordé l'optimisation du parcours au clavier des listes hiérarchiques. La fermeture de ces listes est possible à l'aide de la touche "Escape" comme pour un composant de liste déroulante ou de menu de navigation.

Au regard de la richesse des fonctionnalités, on pourra envisager des solutions d'évitement de contenu après les tests de ces correctifs.

Le mécanisme d'ouverture et de fermeture de ce volet de recherche avancée devra également être optimisé avec ce même motif disclosure (déplier/plier). Idem pour la liste des résultats qui comporte un système d'escamotage.

### Références

* RGAA 4.1 :
  * [Critère 7.3. Contrôle au clavier de chaque script](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode-rgaa/criteres/#crit-7-3)
  * [Critère 11.15. Champs de même nature](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode-rgaa/criteres/#crit-11-15)
  * [Critère 8.2. Validité du code](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode-rgaa/criteres/#crit-8-2)

* [Disclosure pattern W3C](https://www.w3.org/WAI/ARIA/apg/patterns/disclosure/)
* [Ajout de l'option "Escape" dans un menu par Atalan](https://www.accede-web.com/notices/interface-riche/menu-hamburger/#ancre-03)





