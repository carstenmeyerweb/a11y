
function loadEntry(iUrl)
{
	var aJqxhr = $.get(iUrl).done(function(iData) {
		var $data = $(iData);

		$('#versions').empty().append($('#versions', $data).children());
		//$('#colCentre').replaceWith($('#colCentre', $data));
		$('#colCentre').empty().append($('#colCentre', $data).children());
		$('#colDroite').replaceWith($('#colDroite', $data));

		findNodeWithTexte();
		highlightDomaines()
		highlightLangues();
		
		// Notifie les autres javascript qu'une nouvelle entrée est chargée
		//
		$(document).trigger('entry.ready');
		
		DiagDicoNavigation.addLookUpWordHandler('.s_Article, .s_Notice');
		
		interceptNavigation();
	})
	.fail(function() {
		alert("error");
	});
} // loadEntry

//Intercepte les navigations vers une entrée pour garder le client sur la page des résultats.
//
function interceptNavigation()
{
	// Pour l'instant, les liens vers les autres éditions, le voisinage alphabétique, historique de consultation
	//
	$('#versions a, #voisinage a, #HistoireMot a, #Historique a').click(function ($event) {
		var urlEntry = $(this).attr('href');
		
		if (urlEntry) {
			$event.preventDefault();
			$event.stopPropagation();
			loadEntry(urlEntry);
		}
	});
}

// Coche les valeurs d'une liste déroulante personnalisée. Les valeurs à cocher dans la liste déroulante
// sont stockées dans le contenu d'un élément de la page.
// iListSelector : sélecteur de la liste déroulante (ou parent proche).
// iValuesSelector : sélecteur de l'élément qui stocke les valeurs à sélectionner.
//
function checkCustomList(iListSelector, iValuesSelector)
{
	var selectedValues = $(iValuesSelector).text().split(',');
	
	for (var i = 0; i < selectedValues.length; i++) {
		$(iListSelector +' input[value="'+ selectedValues[i] +'"]').prop('checked', true);
		
		// On déclenche le change pour que l'état indeterminate soit bien géré.
		//
		$(iListSelector +' input[value="'+ selectedValues[i] +'"]').trigger('change');
	}
} // checkCustomList


function findNodeWithTexte()
{
	var allForm      = document.querySelector('#chk_allform').checked; 
	var isOldG       = document.querySelector('#chk_oldgraphies').checked;

	// HighLight du mot en texte integral
	// [BUG#7215] : toutes les apostrophes dans le texte sont des courbes, du coup on fait le remplacement 
	var aText = $("#rechIntegral").attr("value").replace(/'/g, "’");
	
	if (aText != undefined && aText.length != 0) {
		var textToHL = [];

		var nbQuot = [...aText.matchAll(new RegExp('"', 'gi'))];
		if (nbQuot.length %2 == 0) {

			for(var i = 0; i < nbQuot.length; i+=2) {
				var sub = aText.slice(nbQuot[i].index+1, nbQuot[i+1].index);
				textToHL.push(sub)
			}

			for(var i = 0; i < textToHL.length; i++) {
				aText = aText.replace(textToHL[i], '');
			}
			// on vire tous les guillemets restant
			aText = aText.replace(/""/g, '');
		}

		var splitText = aText.replace(/\s+/g, " ").trim().split(" ");

		for (var i = 0; i< splitText.length; i++) {
			if (splitText[i].trim().length > 0) {
				textToHL.push(splitText[i]);
				
				if (allForm || isOldG) {
					// call PL
					//  http://testv4.dictionnaire-academie.fr/v1/flexion?word=%C3%A9tudier&type=GA
					var type = "";
					
					if (isOldG && allForm) {
						type = "&type=GAF";
					}
					else if (isOldG) {
						type = "&type=GA";
					}
					
					
					$.ajax({ 
						url: "../v1/flexion?word="+ splitText[i] + type, 
						async: false,
						dataType: 'json',
						success: function(iData) {
							textToHL = textToHL.concat(iData);
	            		}
			        }); 
				}
			}
		}
		// Suppression des mots vide ""
		textToHL = textToHL.filter(e => e !== '');
		// Suppression des doublons
		textToHL = [...new Set(textToHL)];
		// On ordonne par taille decroissante, évite certains pb avec les accents, (cf cas part plus loin) 
		textToHL.sort(function(a,b) {return b.length-a.length;});
		//
		//console.log(textToHL);

		// Recuperation des info du formulaire de la recherche avancée
		var searchedText = '.';
			// Texte Intégral
		var isSensitive  = document.querySelector('#chk_casesensitive').checked; //$("#chk_casesensitive").attr("value");
		var rechDans     = $("#rechDans option:selected").attr("value");
		// Recherche par domaines
		//domaines
		// Recherche par entrées
		//var ignoreAccent = $("#chk_noaccent2").attr("value");
		// Recherche par catégore gammaticale
		//var rechCatGram  = $("#rechCatGram option:selected").text();
		// recherche dans l'étymologie
		//var rechEtymologieDatation  = $("#rechEtymologieDatation option:selected").text();
		// langues
		// Filtre par éditions (meme si ça quasi sur de rien mettre en valeur)
		//var filtreEditions  = $("#filtreEditions option:selected").text();

		if (!isSensitive) {
			aText = aText.toLowerCase();
			searchedText = "translate(., 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz')";
		}

		// Creation des requetes xpath :
		var ignoreClass = "not(contains(@class, 's_ZoneLiens')) and not(contains(@class, 's_ZoneHom')) and not(contains(@class, 's_ZoneOrtho')) and not(contains(@class, 's_Entree'))";

		var searchClass = "";

		// s_Def, s_zEtym, s_Exemple, s_Meta ne fonctionne que pour la 9e édition
		// s_im pour exemple dans édition 1 a 8
		// pour définition c'est le reste
		// meta et etym, rien pour édtion 1 a 8
		switch(rechDans){
		case "def": {
			searchClass = "(contains(@class, 's_Def') or contains(@class, 's_Corps')) and ";
		} break;
		case "etym": {
			searchClass = "contains(@class, 's_zEtym') and ";		
		} break;
		case "ex": {
			searchClass = "(contains(@class, 's_Exemple') or contains(@class, 's_im')) and ";
		} break;
		case "meta": {
			searchClass = "contains(@class, 's_Meta') and ";
		} break;
		default: break;
		}


		for (var t=0; t<textToHL.length;t++) {
			var aTextToHL = textToHL[t].replace(/\*/gi, ".*?");

			var contains = " and contains("+searchedText+", '"+ aTextToHL +"')";
			if (textToHL[t].indexOf("*") != -1) {
				// la fonction document.evaluate(xpath) ne fonctionne pas avec des expressions regulieres "compliqué"
				// .*? lui est inconnu
				contains = "";//" and contains("+searchedText+", /"+ aTextToHL +"/g)";
			}
			else if (textToHL[t].indexOf("?") != -1 && textToHL[t].length > 1) {
				var aT1 = aTextToHL.replace(/\?/g, "");
				var aT2 = aTextToHL.replace(/\?/g, ".");
				contains = " and (contains("+searchedText+", '"+ aT1 +"') or contains("+searchedText+",  /"+ aT2 +"/g) )";
				
				aTextToHL = aTextToHL.replace(/\?/g, ".?");
			}
			
			//test 1
			//var xpath = document.evaluate("//div[contains(@class, 's_Article')]/*["+ignoreClass +"]//*["+ searchClass + ignoreClass + contains +"]//text()",document, null, XPathResult.ANY_TYPE, null );
			// test 2 -> suppression de /*["+ignoreClass +"]
			var xpath = document.evaluate("//div[contains(@class, 's_Article')]//*["+ searchClass + ignoreClass + contains +"]//text()", document, null, XPathResult.ANY_TYPE, null );

			var nodes = [];
			while(request = xpath.iterateNext()) { nodes.push(request);}
			
			// si le contains ne donne rien, on prend plus large
			if (nodes.length == 0) {
				xpath = document.evaluate("//div[contains(@class, 's_Article')]//*["+ searchClass + ignoreClass +"]//text()",document, null, XPathResult.ANY_TYPE, null );
				while(request = xpath.iterateNext()) { nodes.push(request);}
			}
			
			//console.log(nodes.length);
			for (var i = 0; i < nodes.length; i++) {
				var flag = 'g';
				if(!isSensitive) { flag += 'i';	}
				
				// [BUG#7214] : Il se trouve que les mots commençant par des accents ne sont pas trouvé avec l'entité \b devant...
				// une spécificité du langage JS -> https://stackoverflow.com/questions/5436824/matching-accented-characters-with-javascript-regexes
				// il y a un lien vers le standard JS en ce qui concerne les RegExp qui explique pk...
				// Afin de trouvé les mots qui commence par un accent, on ne cherche plus avec \b
				// (évidemment, il en va de même pour les mots finissant par un accent...)
				//
				var startDecalAccent = "\\b";
				var endDecalAccent   = "\\b";
				if (/^[\u00C0-\u017F]/.test(aTextToHL)) startDecalAccent = "";
				if (/[\u00C0-\u017F]$/.test(aTextToHL)) endDecalAccent   = "";
				
				
				if (nodes[i].nodeType == Node.TEXT_NODE) {
					if (nodes[i].textContent.trim().length > 0) {		
						var span = document.createElement("span");
						var txt  = document.createTextNode(nodes[i].textContent);
						var txtNode = "";
						
						span.append(txt)
						
						// Dans le cas d'une recherche par grahies anciennes, on ignore :
						// les accents
						// les ç
						// les ligatures
						if (isOldG) {
							// Pour ne pas modifier le texte dans le node
							var txtNodeTmp = nodes[i].data;
							
							txtNode = txtNodeTmp.normalize("NFD").replace(/\p{Diacritic}/gu, "")
																 .replace(/ç/gu, "c")
							 									 .replace(/\u00E6/gu, "ae") // æ
							 									 .replace(/\u00C6/gu, "ae") // Æ
							 									 .replace(/\u0153/gu, "oe") // œ
							 									 .replace(/\u0152/gu, "OE") // Œ
							 									 
							 									 
							aTextToHL = aTextToHL.normalize("NFD").replace(/\p{Diacritic}/gu, "").replace(/ç/gu, "c");
						}
						else {
							txtNode = nodes[i].textContent;
						}
						//console.log(nodes[i].parentElement.className + " -> " + nodes[i].textContent);
						
						var occurrences = [...txtNode.matchAll(new RegExp(startDecalAccent+aTextToHL+endDecalAccent, flag))];
						
						
						//Boolean(nodes[i].closest(".s_Entree"))
						if (   !Boolean(nodes[i].parentElement.closest(".s_Entree")) 
							&& !Boolean(nodes[i].parentElement.closest(".s_ZoneLiens"))
							&& !Boolean(nodes[i].parentElement.closest(".s_ZoneHom"))
							&& !Boolean(nodes[i].parentElement.closest(".s_ZoneOrtho"))
							&& !Boolean(nodes[i].parentElement.closest(".advsearch_hightlight")) // pour ne pas boucler sur ce qui est deja fait
							&& occurrences.length > 0) {
							nodes[i].parentElement.replaceChild(span, nodes[i])
							highlightText(span, occurrences);
						}
					}
				}
				else {
					//console.log(nodes[i]);
					//console.log(nodes[i].outerHTML);
					var occurrences = [...nodes[i].textContent.matchAll(new RegExp("\\b"+startDecalAccent+aTextToHL+endDecalAccent+"\\b", flag))]
					
					highlightText(nodes[i], occurrences);
				}	
			}
		}
		
		/*
		if ($(".advsearch_hightlight").length > 0) {
			$([document.documentElement, document.body]).animate({
		        scrollTop: $(".advsearch_hightlight:first-child").offset().top - 200
		    }, 2000);
		}*/

	}
}

function highlightText(aNode, occurrences) {
	// find all occurence of aText in aNode
	var textNode = aNode.textContent;

	var newChildren = [];

	if (occurrences.length != 0) {
		var aParentNode = aNode.parentNode;
		var aDocument = aNode.ownerDocument;

		for(var i = 0; i < occurrences.length; i++) {
			var oc = occurrences[i];

			if (i == 0) {
				var aTextBefore = aDocument.createTextNode(textNode.substr(0, oc.index));
				if (aTextBefore.length != 0) {
					newChildren.push(aTextBefore);
				}
			}

			var aHighlightedText  = aDocument.createTextNode(textNode.substr(oc.index, oc[0].length));
			var aHighlightElement = aDocument.createElement("span");

			aHighlightElement.setAttribute("class", "advsearch_hightlight");
			aHighlightElement.appendChild(aHighlightedText);
			//
			newChildren.push(aHighlightElement);

			var aTextAfter;
			if (i == occurrences.length -1) {
				aTextAfter = aDocument.createTextNode(textNode.substr(oc.index+oc[0].length));
			}
			else {
				aTextAfter = aDocument.createTextNode(textNode.substr(oc.index+oc[0].length, occurrences[i+1].index - (oc.index + oc[0].length)));
			}

			if (aTextAfter.length != 0) {
				newChildren.push(aTextAfter);
			}
		}

		// Empty node 
		aNode.textContent = "";
		// Populate Node
		for (var c = 0; c < newChildren.length; c++) {
			aNode.appendChild(newChildren[c]);		
		}
	}
}

function highlightDomaines() {
	
	var domainMap = $("#hid_domainsMap").val();
	
	if (domainMap && domainMap.length > 0) {
		var values = JSON.parse(domainMap);
				
		for(var i = 0; i < values.length; i++) {
			$(".s_Marq[refid*='"+ values[i] +"'], .s_dom[refid*='"+ values[i] +"']").addClass("advsearch_hightlight");
			//$("span[refid='"+ values[i] +"']").parent(".s_Marq").addClass('advsearch_hightlight')
		}
	}
}

function highlightLangues(){

	var langMap = $("#hid_langsMap").val();
	
	if (langMap && langMap.length > 0) {
		var values = JSON.parse(langMap);
		
		for(var i = 0; i < values.length; i++) {
			$(".s_Lang[refid*='"+ values[i] +"']").addClass("advsearch_hightlight");
			//$("span[refid='"+ values[i] +"']").parent(".s_Lang").addClass('advsearch_hightlight')
		}
	}
	
}






$(document).ready(function() {
	$('#resultatRechAvancee a').on('click', function ($event) {
		var urlEntry = $(this).attr('href');

		if (urlEntry) {
			$event.preventDefault();
			$event.stopPropagation();

			$('#resultatRechAvancee .colGaucheMotActif').addClass('colGaucheMot').removeClass('colGaucheMotActif');
			$('.colGaucheMot', $(this)).addClass('colGaucheMotActif').removeClass('colGaucheMot');
			loadEntry(urlEntry);
		}
	});
	
	// Bandeau Col Gauche
	//
	$('.sectionToggleGauche').click(function($event) {
		$event.stopPropagation();
		$(this).next('.listColGauche').slideToggle(400);
		var urlImg = $('img', $(this)).attr('src');
		var $btn = $('button', $(this));
		
		$(this).toggleClass("activeRotGauche");
		$(this).toggleClass("activeRotDroite");
		
		// On change le sens de l'image de la fléche.
		//
		if (urlImg.search('sectionOuvert') != -1){
			urlImg = urlImg.replace('sectionOuvert', 'sectionFerme');
			$btn.attr('aria-expanded', 'false');
			$btn.attr('title', 'Déplier');
		}
		else {
			urlImg = urlImg.replace('sectionFerme', 'sectionOuvert');
			$btn.attr('aria-expanded', 'true');
			$btn.attr('title', 'Replier');
		}
		$('img', $(this)).first().attr('src', urlImg);
	});

	if ($('#resultatRechAvancee a').length > 0) {
		// Ouverture de la première section, sélection du premier résultat.
		//
		//$('#colGaucheResultat .sectionToggleGauche').first().click();
		$('#resultatRechAvancee a').first().click();
	}
	else {
		// Message pas de résultat
		//
		$('#sansResultat').addClass('divSansResultatsIconesOuvert').removeClass('divSansResultatsIconesFermer');
		$('#colCentreWrapper, #colGaucheRechAv, #colDroite').addClass('transparent');
	}
	
	checkCustomList('#divRechParDomaines', '#checked_domain');
	checkCustomList('#divRechCatGrammaticale', '#checked_grammcat');
	checkCustomList('.rechEtymologieLangue', '#checked_lang');
	
	$('#sansResultat #btFermerSansResultat').click(function () {
		$('#sansResultat').addClass('divSansResultatsIconesFermer').removeClass('divSansResultatsIconesOuvert');
		$('#colCentreWrapper, #colGaucheRechAv, #colDroite').removeClass('transparent');
	});
	
	// Désactivation du bouton partage.
	//
	$('#partage').prop('disabled', true);
	
	// Bouton de fermeture des résultats de recherche.
	// On navigue vers la page de l'entrée en cours de consultation.
	//
	$('#fermerColGauche').on('click', function() {
		var anEntryId = $('#txt_entry').val();
		
		if (anEntryId) {
			window.location.href = '../article/'+ encodeURIComponent(anEntryId);
		}
	});
	
	// Bandeau de resultat de recherche avancée
	//
	$('.blocBandeauResultat').click(function($event) {
		if ($(window).width() <= 600) {
			$event.stopPropagation();
			toggleOuvrirFermer(this);
		}
	});
	
	$(window).resize(function() {
		// La taille 600 est choisie arbitrairement (voir handle '.blocBandeauResultat')
		//
		if ($(window).width() > 600) {
			if($('#colGaucheResultat').is(':hidden')) {
				// On peut penser que les résultat ont été caché par l'utilisateur en affichage mobile.
				// On appelle la fonction qui affiche.
				//
				toggleOuvrirFermer($('.blocBandeauResultat'));
			}
		}
	});
});