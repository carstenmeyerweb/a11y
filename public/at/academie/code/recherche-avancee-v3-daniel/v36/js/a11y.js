;(function(){

    //
    // == CONSTANTES
    // --------------------------------------------------
    var d = document,
        w = window,
        a11y ={};


    //
    // == DOCUMENT READY
    // --------------------------------------------------
    // This is executed when HTML-Document is loaded and DOM is ready.
    document.addEventListener("DOMContentLoaded", function(event){

        a11y.toggle();
        a11y.close();

    });


	
	//
    // == ACCESSIBLE TOGGLE
    // --------------------------------------------------
    // This function adds a .is-active class on .toggle elements and their target when they are clicked.
    // It also handles accessibility issues by toggling aria-attributes and visibility (hidden/visible).
    // HTML element can be freezed if needed (example: modal windows). When so, links outside target are disabled.
    // This is partially based on: https://www.accede-web.com/notices/interface-riche/menu-hamburger/

    a11y.toggle = function(){

        var elements = d.querySelectorAll('.disclosure'),
            focusableElements = '' +
                'a[href]:not([tabindex="-1"]), ' +
                'area[href]:not([tabindex="-1"]), ' +
                'input:not([disabled]):not([tabindex="-1"]), ' +
                'select:not([disabled]):not([tabindex="-1"]), ' +
                'textarea:not([disabled]):not([tabindex="-1"]), ' +
                'button:not([disabled]):not([tabindex="-1"]), ' +
                'iframe:not([tabindex="-1"]), ' +
                '[tabindex]:not([tabindex="-1"]), ' +
                '[contentEditable=true]:not([tabindex="-1"])',
            isVisible = function(el){
                var style = w.getComputedStyle(el);

                return  style.width !== "0" &&
                    style.height !== "0" &&
                    style.opacity !== "0" &&
                    style.display !== 'none' &&
                    style.visibility !== 'hidden';
            };

        Array.prototype.forEach.call(elements, function(caller){

            // find target or take caller next sibling
            var target = d.querySelector('#'+caller.getAttribute('aria-controls')) || caller.nextElementSibling,
                // nodes arrays
                targetNodes = [],
                elsewhereNodes = [],

                // find target inner nodes and push them into targetNodes[]
                // but DON'T take the caller please
                // (don't use target container directly because it contains the caller which should remain accessible)
                fillTarget = function(){
                    Array.from(target.querySelectorAll(focusableElements)).forEach(function(node){
                        if ((!node.classList.contains('toggle-let-me-out')) && (caller !== node)){
                            targetNodes.push(node);
                        }
                    });
                },

                // if not <script>, add first siblings to elsewhereNodes[]...
                // but still NOT the caller please
                fillElsewhere = function(node){
                    Array.prototype.filter.call(node.parentNode.children, function(child){

                        if ((child !== node) && (child.nodeName !== 'SCRIPT') && (caller !== child)){
                            elsewhereNodes.push(child);
                        }
                    });

                    // ...until BODY node is hit
                    if (node.parentNode.nodeName !== 'BODY'){
                        //fillElsewhere(node.parentNode); //recursion!
                    }
                },

                // adds the '.toggle-let-me-out' items contained in 'target' to the elsewhereNodes list
                addElsewhereBonus = function(node){
                    Array.from(target.querySelectorAll('.toggle-let-me-out')).forEach(function(node){
                        elsewhereNodes.push(node);
                    });
                },

                // init
                init = function(){

                    // if caller is visible
                    if (isVisible(caller)){

                        // if not expanded
                        if (caller.getAttribute('aria-expanded') === 'false'){

                            // hide targetNodes
                            Array.prototype.forEach.call(targetNodes, function(node){
                                //node.style.display = 'none';
                            });

                        }

                        // if expanded
                        else{

                            // show target links
                            Array.prototype.forEach.call(targetNodes, function(node){
                                node.style.display = 'block';
                            });

                            // hide other links
                            if (caller.classList.contains('toggle-freeze')){
                                Array.prototype.forEach.call(elsewhereNodes, function(node){
                                    node.style.display = 'none';
                                });
                            }
                        }
                    }

                };

            // build elsewhereNodes[] and targetNodes[] arrays
            fillTarget(target);
            fillElsewhere(target);
            addElsewhereBonus(target);

            // on load: init
            init();

            // on click
            caller.addEventListener('click', function(){

                // toggle class on elements
                caller.classList.toggle('is-active');
                target.classList.toggle('is-active');

                // toggle aria-expanded attribute of element
                caller.setAttribute('aria-expanded', !(caller.getAttribute('aria-expanded') === 'true'));

                // if freeze html: kills overflow and elsewhereNodes
                if (caller.classList.contains('toggle-freeze')){
                    d.getElementsByTagName('html')[0].classList.toggle('is-freezed');

                    // toggle focus on every NON-target element
                    Array.prototype.forEach.call(elsewhereNodes, function(node){
                        node.style.display = (caller.getAttribute('aria-expanded') === 'true') ? 'none' : 'block';
                    });
                }

                if (caller.hasAttribute('data-parent-class')){
                    var parentClass = caller.getAttribute('data-parent-class');
                    if (caller.getAttribute('aria-expanded') === 'true'){
                        caller.parentNode.classList.add(parentClass);
                    }
                    else {
                        caller.parentNode.classList.remove(parentClass);
                    }
                }
                if (caller.hasAttribute('data-panel-class')){
                    var panelClass = caller.getAttribute('data-panel-class');
                    var disclosureControls = caller.getAttribute('aria-controls');
                    var panel = d.querySelector('#'+ disclosureControls);
                    if (caller.getAttribute('aria-expanded') === 'true'){
                        panel.classList.add(panelClass);
                    }
                    else {
                        panel.classList.remove(panelClass);
                    }
                }

                // toggle focus on every targetNodes element
                Array.prototype.forEach.call(targetNodes, function(node){
                    //node.style.display = (caller.getAttribute('aria-expanded') === 'true') ? 'block' : 'none';
                });

            }, false);

            // close and focus on element when hitting Echap (only if caller is visible)
            w.addEventListener('keydown',(e) => {
                if (e.key === 'Escape'){
                    if (isVisible(caller) && (caller.getAttribute('aria-expanded') === 'true') && (!caller.hasAttribute('data-escape'))){
                        if (caller.hasAttribute('data-parent-class')){
                            var parentClass = caller.getAttribute('data-parent-class');
                            caller.parentNode.classList.remove(parentClass);

                        }
                        if (caller.hasAttribute('data-panel-class')){
                            var panelClass = caller.getAttribute('data-panel-class');
                            var disclosureControls = caller.getAttribute('aria-controls');
                            var panel = d.querySelector('#'+ disclosureControls);
                            panel.classList.remove(panelClass);
                        }
                        caller.click();
                        caller.focus();
                    }
                }
            });
        });
    };

    a11y.close = function(){

        //const disclosurePanel = closingBtn.closest('.is-active');
        var closingBtn = d.querySelector('.close-disclosure');
        
        closeDisclosure = (event) => {
            var thisBtn = event.currentTarget;
            var disclosurePanel = thisBtn.closest('div.is-active');
            var disclosurePanelId = disclosurePanel.getAttribute('id');
            var disclosureBtn = d.querySelector('[aria-controls="'+ disclosurePanelId +'"]');
            // hide and change accessibility param
            disclosureBtn.classList.remove('is-active');
            disclosureBtn.setAttribute('aria-expanded', 'false');
            disclosurePanel.classList.remove('is-active');
            // set the focus on opener
            disclosureBtn.focus();
        }
        
        if(closingBtn !== null) closingBtn.addEventListener('click', closeDisclosure, false);
    };
})();