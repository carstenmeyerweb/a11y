function makeResultLabel(iResult) {
    var aLabel = iResult.label + (iResult.nbhomograph == "" ? "" : " [" + iResult.nbhomograph + "]");
    aLabel += iResult.nature == "" ? "" : ", " + iResult.nature;
    return aLabel
}

function handleSearchSuccess(data, textStatus, jqXHR) {
    var $resultList = $("#rechercheautocomplete-list");
    if ($.isArray(data.result) && $resultList.data("term") != $("#recherche").val()) {
        var tabResults = data.result;
        $(">a,.noresult", $resultList).remove();
        if (tabResults.length > 0)
            for (var i = 0; i < tabResults.length; i++) $resultList.append('<a href="' + tabResults[i].url + '" data-score="' + tabResults[i].score + '">' + makeResultLabel(tabResults[i]) + "</a>");
        else if (tabResults.length == 0 && data.term != "") $resultList.append('<div class="noresult">Il n\'y a pas de résultat pour cette recherche.</div>');
        $resultList.data("term", data.term);
        $resultList.show()
    }
}

function initToobarButton() {
    $("#partage").click(function () {
        if ($("#accessibiliteFen").hasClass("accessibiliteFenOuvert")) $("#btFermerAccessibilite").click();
        $("#divPartageIcones").addClass("divPartageIconesOuvert");
        $("#divPartageIcones").removeClass("divPartageIconesFermer")
    });
    $("#btFermerPartage").click(function () {
        $("#divPartageIcones").addClass("divPartageIconesFermer");
        $("#divPartageIcones").removeClass("divPartageIconesOuvert")
    });
    $(".boutonCopierPartage").click(function () {
        $("#urlcopie").select();
        document.execCommand("copy");
        $("#divPartageIcones").addClass("divPartageIconesFermer");
        $("#divPartageIcones").removeClass("divPartageIconesOuvert");
        if ($("#menuCentrale").hasClass("ouvert")) setTimeout(function () {
            $("#plus").trigger("click")
        }, 300)
    });
    $("#partageMobile").click(function () {
        $("#divPartageIcones").addClass("divPartageIconesOuvert");
        $("#divPartageIcones").removeClass("divPartageIconesFermer")
    });
    $("#imprime").click(function () {
        window.print()
    })
}

function overInfoRecherche(iIsAcceuil) {
    if (iIsAcceuil) {
        $("#recherche").addClass("rechercheFocusStyle");
        $("#explicationRecherche").addClass("afficher");
        $("#infoRechercheAccueil img").attr("src", $("#infoRechercheAccueil img").attr("src").replace("rechercheInfoAccueil@3x", "rechercheInfo2Accueil@3x"))
    } else {
        $("#recherche").addClass("rechercheFocusStyle");
        $("#explicationRecherche").addClass("afficher");
        $("#infoRecherche img").attr("src", $("#infoRecherche img").attr("src").replace("rechercheInfo@3x", "rechercheInfo2@3x"))
    }
}

function outInfoRecherche(iIsAcceuil) {
    if (iIsAcceuil) {
        $("#recherche").removeClass("rechercheFocusStyle");
        $("#explicationRecherche").removeClass("afficher");
        $("#infoRechercheAccueil img").attr("src", $("#infoRechercheAccueil img").attr("src").replace("rechercheInfo2Accueil@3x", "rechercheInfoAccueil@3x"))
    } else {
        $("#recherche").removeClass("rechercheFocusStyle");
        $("#explicationRecherche").removeClass("afficher");
        $("#infoRecherche img").attr("src", $("#infoRecherche img").attr("src").replace("rechercheInfo2@3x",
            "rechercheInfo@3x"))
    }
}

function createCookie(name, value, days) {
    var expires = "";
    if (days) {
        var date = new Date;
        date.setTime(date.getTime() + days * 24 * 60 * 60 * 1E3);
        expires = "; expires=" + date.toGMTString()
    }
    document.cookie = name + "=" + value + expires + "; path=/"
}

function eraseCookie(name) {
    createCookie(name, "", -1)
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(";");
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == " ") c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length)
    }
    return null
}

function acceptCookies(response) {
    var responseValue = "0";
    if (response == true) {
        responseValue = "1";
        if (typeof ga == "function" && window["trackerId"] != "undefined") {
            ga("create", window["trackerId"], "auto");
            ga("send", "pageview")
        }
        if (typeof _paq != "undefined") _paq.push(["setConsentGiven"])
    }
    createCookie("acceptCookies", responseValue, 360)
}

function saveFontSize(fontSize) {
    var acceptCookie = readCookie("acceptCookies");
    if (acceptCookie != null && acceptCookie == "1") createCookie("fontSize", fontSize, 30)
}

function initFontSize(currentSize) {
    var index = 2;
    var pourcentageStep = ["80%", "90%", "105%", "115%", "130%", "150%", "200%"];
    var pourcentageStepMobile = ["90%", "105%", "130%"];
    for (var i = 0; i < pourcentageStep.length; i++)
        if (pourcentageStep[i] == currentSize) {
            index = i;
            break
        } var previousIndex = function (index, length) {
        if (index <= 0) return length - 1;
        else return index - 1
    };
    var nextIndex = function (index, length) {
        return (index + 1) % length
    };
    var changeFontSize = function (fontSize) {
        $(".s_Article, .s_Notice, .a_Annexe").css("font-size", fontSize);
        $(".s_Entree_haut").css("font-size", "130%");
        $("#colDroite").css("font-size", fontSize);
        $("#colGaucheRechAv").css("font-size", fontSize);
        saveFontSize(fontSize)
    };
    $("#txPlus, #txPlusMenuCentrale").on("click", function () {
        if (pourcentageStep[index] != pourcentageStep[pourcentageStep.length - 1]) {
            index = nextIndex(index, pourcentageStep.length);
            changeFontSize(pourcentageStep[index])
        }
    });
    $("#txMoins, #txMoinsMenuCentrale").on("click", function () {
        if (pourcentageStep[index] != pourcentageStep[0]) {
            index = previousIndex(index,
                pourcentageStep.length);
            changeFontSize(pourcentageStep[index])
        }
    });
    var changeFontSizeMobile = function (fontSize) {
        $(".s_Article, .s_Notice, .a_Annexe").css("font-size", fontSize);
        if ($("#menuCentrale").hasClass("ouvert")) setTimeout(function () {
            $("#plus").trigger("click")
        }, 300);
        saveFontSize(fontSize)
    };
    $("#iconesOutilsMobile #typoPetit").on("click", function () {
        changeFontSizeMobile(pourcentageStepMobile[0])
    });
    $("#iconesOutilsMobile #typoMoyen").on("click", function () {
        changeFontSizeMobile(pourcentageStepMobile[1])
    });
    $("#iconesOutilsMobile #typoGrand").on("click", function () {
        changeFontSizeMobile(pourcentageStepMobile[2])
    });
    $("#taillesTypoAccessibilite #typoPetit").on("click", function () {
        index = 2;
        changeFontSize(pourcentageStep[index])
    });
    $("#taillesTypoAccessibilite #typoMoyen").on("click", function () {
        index = 4;
        changeFontSize(pourcentageStep[index])
    });
    $("#taillesTypoAccessibilite #typoGrand").on("click", function () {
        index = 6;
        changeFontSize(pourcentageStep[index])
    })
}
$(document).on("entry.ready", function () {
    $("#plus").on("click", function () {
        $("#vertical").toggleClass("ouvert");
        $("#menuCentrale").toggleClass("ouvert");
        $("#contenu").toggleClass("fermer");
        $("footer").toggleClass("cellulaire")
    });
    var cssAccessClass = "navy";
    $("." + cssAccessClass).each(function () {
        if ($(this).children().length > 0) {
            $(this).removeAttr("tabindex");
            $(this).removeClass(cssAccessClass)
        } else $(this).keypress(function (event) {
            var keycode = event.keyCode ? event.keyCode : event.which;
            if (keycode == "13") {
                var newRange =
                    document.createRange();
                newRange.setStart(this.firstChild, 0);
                newRange.setEnd(this.firstChild, 0);
                var selection = window.getSelection();
                selection.removeAllRanges();
                selection.addRange(newRange);
                $(this).click()
            }
        })
    });
    $("a ." + cssAccessClass).each(function () {
        $(this).removeAttr("tabindex");
        $(this).removeClass(cssAccessClass)
    })
});
jQuery(document).ready(function ($) {
    $("#infoRechercheAccueil").mouseover(function () {
        overInfoRecherche(true)
    }).on("focus", function () {
        overInfoRecherche(true)
    });
    $("#infoRechercheAccueil").mouseout(function () {
        outInfoRecherche(true)
    }).on("focusout", function () {
        outInfoRecherche(true)
    });
    $("#btViderRecherche, #btViderRechercheAccueil").on("focus", function () {
        $("img", $(this)).attr("src", $("img", $(this)).attr("src").replace("rechercheEffacer@3x", "rechercheEffacerBlanc@3x"))
    });
    $("#btViderRecherche, #btViderRechercheAccueil").on("focusout",
        function () {
            $("img", $(this)).attr("src", $("img", $(this)).attr("src").replace("rechercheEffacerBlanc@3x", "rechercheEffacer@3x"))
        });
    $("#btLoupeAv, #imgLoupeAvAccueil").on("focus", function () {
        $("img", $(this)).attr("src", $("img", $(this)).attr("src").replace("loupeAv.png", "loupeAvActif.png"))
    });
    $("#btLoupeAv, #imgLoupeAvAccueil").on("focusout", function () {
        $("img", $(this)).attr("src", $("img", $(this)).attr("src").replace("loupeAvActif.png", "loupeAv.png"))
    });
    $("#fermeture, #fermetureAccueil").on("focus", function () {
        $("img",
            $(this)).attr("src", $("img", $(this)).attr("src").replace("xFermeture.png", "xFermetureActif.png"))
    });
    $("#fermeture, #fermetureAccueil").on("focusout", function () {
        $("img", $(this)).attr("src", $("img", $(this)).attr("src").replace("xFermetureActif.png", "xFermeture.png"))
    });
    $("#informations, #informationsAccueil").on("focus", function () {
        $("img", $(this)).attr("src", $("img", $(this)).attr("src").replace("informations.png", "informationsActif.png"))
    });
    $("#informations, #informationsAccueil").on("focusout", function () {
        $("img",
            $(this)).attr("src", $("img", $(this)).attr("src").replace("informationsActif.png", "informations.png"))
    });
    $("#logoAF a").on("focus", function () {
        $("#logoAF a img").addClass("logoHighlight")
    });
    $("#logoAF a").on("focusout", function () {
        $("#logoAF a img").removeClass("logoHighlight")
    });
    $("#precedent a").on("focus", function () {
        $("#precedent").addClass("precedentFocus")
    });
    $("#precedent a").on("focusout", function () {
        $("#precedent").removeClass("precedentFocus")
    });
    $("#suivant a").on("focus", function () {
        $("#suivant").addClass("suivantFocus")
    });
    $("#suivant a").on("focusout", function () {
        $("#suivant").removeClass("suivantFocus")
    });
    $("#frm_search").submit(function ($event) {
        $event.preventDefault();
        if ($("#recherche").val() == "") {
            $("#rechercheautocomplete-list>a, #rechercheautocomplete-list .noresult").remove();
            $("#rechercheautocomplete-list").show()
        } else if ($("#recherche").val() != $("#rechercheautocomplete-list").data("term")) $.ajax({
            type: "POST",
            url: $("#frm_search").attr("action"),
            dataType: "json",
            data: $.param($("#frm_search input"))
        }).done(handleSearchSuccess);
        else if ($("#rechercheautocomplete-list > a").length > 0) {
            var url = $("#rechercheautocomplete-list > a.selected").attr("href");
            if (!url) url = $("#rechercheautocomplete-list > a").first().attr("href");
            if (url) document.location.href = url
        }
    });
    $("#recherche").keyup(function ($event) {
        if (($("#recherche").val().length > 2 || $("#rechercheautocomplete-list:visible").length > 0) && $("#recherche").val() != $("#rechercheautocomplete-list").data("term")) $("#frm_search").submit();
        else if ($("#rechercheautocomplete-list:visible").length >
            0)
            if ($event.which == 40 || $event.which == 38) {
                var $itemSelected = $("#rechercheautocomplete-list > a.selected");
                if ($event.which == 40)
                    if ($itemSelected.length > 0) {
                        $itemSelected.next().addClass("selected");
                        $itemSelected.removeClass("selected")
                    } else $("#rechercheautocomplete-list > a").first().addClass("selected");
                else if ($event.which == 38)
                    if ($itemSelected.length > 0) {
                        $itemSelected.prev().addClass("selected");
                        $itemSelected.removeClass("selected")
                    } else $("#rechercheautocomplete-list > a").last().addClass("selected");
                $itemSelected = $("#rechercheautocomplete-list > a.selected");
                if ($itemSelected.get(0).offsetTop < $("#rechercheautocomplete-list").scrollTop() || $itemSelected.get(0).offsetTop + $itemSelected.height() > $("#rechercheautocomplete-list").scrollTop() + $("#rechercheautocomplete-list").get(0).clientHeight) $("#rechercheautocomplete-list").scrollTop($itemSelected.get(0).offsetTop)
            }
    });
    $("#btViderRecherche, #btViderRechercheAccueil").click(function () {
        $("#recherche").val("");
        $("#rechercheautocomplete-list").hide();
        $("#recherche").focus();
    });
    $("#motCommencant, #motsProches").click(function ($event) {
        var $aTarget = $($event.currentTarget);
        $event.preventDefault();
        if (!$aTarget.is(".btRechercheActive")) {
            var aTitleCompl = " (filtre actif)";
            $("#search_options").val($aTarget.data("value"));
            $("#rechercheautocomplete-list .btRechercheActive").removeClass("btRechercheActive").addClass("btRechercheDisponible");
            $aTarget.addClass("btRechercheActive").removeClass("btRechercheDisponible");
            $("#rechercheautocomplete-list .btRechercheActive a").attr("title",
                $("#rechercheautocomplete-list .btRechercheActive a").attr("title") + aTitleCompl);
            $("#rechercheautocomplete-list .btRechercheDisponible a").attr("title", $("#rechercheautocomplete-list .btRechercheActive a").attr("title").replace(aTitleCompl, ""));
            $("#rechercheautocomplete-list").data("term", "");
            $("#recherche").focus();
            $("#frm_search").submit()
        }
    });
    $("#recherche").focus();
    $(".menuAnim").on("click", function () {
        $(".menuIcone").toggleClass("is-clicked");
        if ($(this).attr("aria-expanded") == "false") {
            $(this).attr("aria-expanded",
                "true");
            $(this).attr("title", "Fermer le menu")
        } else {
            $(this).attr("aria-expanded", "false");
            $(this).attr("title", "Ouvrir le menu")
        }
        $(".transform").toggleClass("transform-active");
        $(".menuTopCell").toggleClass("ouvertureMenuCell")
    });
    if ($("#blocCookies").length > 0) {
        $("#blocCookies .boutonCookies").first().focus();
        $("#blocCookies .boutonCookies").click(function ($event) {
            var $aTarget = $($event.currentTarget);
            var aResponse = $aTarget.data("response");
            $event.preventDefault();
            if (aResponse == "1") acceptCookies(true);
            else if (aResponse == "0") acceptCookies(false);
            $("#blocCookies").fadeOut()
        })
    }

    $("#txMoins, #txPlus, #imprime, #partage").mouseover(function () {
        $(this).addClass("picto-itemFocus")
    }).mouseout(function () {
        $(this).removeClass("picto-itemFocus")
    }).click(function () {
        $(this).removeClass("picto-itemFocus")
    }).focus(function () {
        $(this).addClass("picto-itemFocus")
    }).focusout(function () {
        $(this).removeClass("picto-itemFocus")
    });

    var aFontSize = readCookie("fontSize");
    if (aFontSize) $(".s_Article, .s_Notice, .colGaucheFond .a_Annexe").css("font-size", aFontSize);
    else aFontSize = "";
    initFontSize(aFontSize);
    initToobarButton();
    var tabPreloadImg = ["images/logoAFHighlight.png", "images/logoAFMobileHighlight.png", "images/rechercheInfo2Accueil@3x.png", "images/rechercheInfo2@3x.png", "images/loupeFocus.png", "images/logoAFMobileHighlight.png", "images/logoAFHighlight.png", "images/infoAccessibiliteSurvol.png", "images/accessibilite_survol.png",
"images/xFermerInfoBulleSurvol.png"];
    var basePath = $("#preload").data("basepath");
    if (typeof basePath != "undefined")
        for (var i = 0; i < tabPreloadImg.length; i++) $("<img />").attr("src", basePath + tabPreloadImg[i]);
    
    // Fermeture des niveaux des menus personnalisés
    function fermerNiveaux() {
        if ($("#rechDomaines ul li.niveauxUN > ul").hasClass("ouvert")) {
            $("#rechDomaines ul li.niveauxUN > ul").removeClass("ouvert");
            $("#rechDomaines ul li.niveauxUN").removeClass("fondBleu");
            $("#rechDomaines ul li.niveauxUN > h4").removeClass("sectionActive");
            $("#rechDomaines ul li.niveauxUN > h4").removeClass("active")
        }
        if ($("#rechDomaines ul li.niveauxUN > div").hasClass("rotation")) $("#rechDomaines ul li.niveauxUN > div").removeClass("rotation");
        if ($("#rechDomaines ul li.niveauxDEUX > h4").hasClass("active")) {
            $("#rechDomaines ul li.niveauxDEUX > h4").removeClass("active");
            $("#rechDomaines ul li.niveauxDEUX").removeClass("fondBleu");
            $("#rechDomaines ul li.niveauxDEUX > h4").removeClass("sectionActive");
            $("#rechDomaines ul li.niveauxDEUX > h4").removeClass("active")
        }
        if ($("#rechDomaines ul li.niveauxDEUX > ul").hasClass("ouvert")) $("#rechDomaines ul li.niveauxDEUX > ul").removeClass("ouvert");
        if ($("#rechDomaines ul li.niveauxDEUX > div").hasClass("rotation")) $("#rechDomaines ul li.niveauxDEUX > div").removeClass("rotation");
        if ($("#rechDomaines ul li.niveauxDEUX").hasClass("active")) $("#rechDomaines ul li.niveauxDEUX").removeClass("active");
        if ($("#rechDomaines ul li.niveauxTROIS > label").hasClass("active")) {
            $("#rechDomaines ul li.niveauxTROIS > label").removeClass("active");
            $("#rechDomaines ul li.niveauxTROIS").removeClass("fondBleu");
            $("#rechDomaines ul li.niveauxTROIS > label").removeClass("sectionActive");
            $("#rechDomaines ul li.niveauxTROIS > label").removeClass("active")
        }
        if ($("#rechDomaines ul li.niveauxTROIS > ul").hasClass("ouvert")) $("#rechDomaines ul li.niveauxTROIS > ul").removeClass("ouvert");
        if ($("#rechDomaines ul li.niveauxTROIS > div").hasClass("rotation")) $("#rechDomaines ul li.niveauxTROIS > div").removeClass("rotation");
        if ($("#rechDomaines ul li.niveauxTROIS").hasClass("active")) $("#rechDomaines ul li.niveauxTROIS").removeClass("active");
        if ($("#rechCatGram ul li.niveauxUN > ul").hasClass("ouvert")) {
            $("#rechCatGram ul li.niveauxUN > ul").removeClass("ouvert");
            $("#rechCatGram ul li.niveauxUN").removeClass("fondBleu");
            $("#rechCatGram ul li.niveauxUN > h4").removeClass("sectionActive");
            $("#rechCatGram ul li.niveauxUN > h4").removeClass("active")
        }
        if ($("#rechCatGram ul li.niveauxUN > div").hasClass("rotation")) $("#rechCatGram ul li.niveauxUN > div").removeClass("rotation");
        if ($("#rechCatGram ul li.niveauxDEUX > h4").hasClass("active")) {
            $("#rechCatGram ul li.niveauxDEUX > h4").removeClass("active");
            $("#rechCatGram ul li.niveauxDEUX").removeClass("fondBleu");
            $("#rechCatGram ul li.niveauxDEUX > h4").removeClass("sectionActive");
            $("#rechCatGram ul li.niveauxDEUX > h4").removeClass("active")
        }
        if ($("#rechCatGram ul li.niveauxDEUX > ul").hasClass("ouvert")) $("#rechCatGram ul li.niveauxDEUX > ul").removeClass("ouvert");
        if ($("#rechCatGram ul li.niveauxDEUX > div").hasClass("rotation")) $("#rechCatGram ul li.niveauxDEUX > div").removeClass("rotation");
        if ($("#rechCatGram ul li.niveauxDEUX").hasClass("active")) $("#rechCatGram ul li.niveauxDEUX").removeClass("active");
        if ($("#rechCatGram ul li.niveauxTROIS > label").hasClass("active")) {
            $("#rechCatGram ul li.niveauxTROIS > label").removeClass("active");
            $("#rechCatGram ul li.niveauxTROIS").removeClass("fondBleu");
            $("#rechCatGram ul li.niveauxTROIS > label").removeClass("sectionActive");
            $("#rechCatGram ul li.niveauxTROIS > label").removeClass("active")
        }
        if ($("#rechCatGram ul li.niveauxTROIS > ul").hasClass("ouvert")) $("#rechCatGram ul li.niveauxTROIS > ul").removeClass("ouvert");
        if ($("#rechCatGram ul li.niveauxTROIS > div").hasClass("rotation")) $("#rechCatGram ul li.niveauxTROIS > div").removeClass("rotation");
        if ($("#rechCatGram ul li.niveauxTROIS").hasClass("active")) $("#rechCatGram ul li.niveauxTROIS").removeClass("active");
        if ($("#rechEtymoParLangue ul li.niveauxUN > ul").hasClass("ouvert")) {
            $("#rechEtymoParLangue ul li.niveauxUN > ul").removeClass("ouvert");
            $("#rechEtymoParLangue ul li.niveauxUN").removeClass("fondBleu");
            $("#rechEtymoParLangue ul li.niveauxUN > h4").removeClass("sectionActive");
            $("#rechEtymoParLangue ul li.niveauxUN > h4").removeClass("active")
        }
        if ($("#rechEtymoParLangue ul li.niveauxUN > div").hasClass("rotation")) $("#rechEtymoParLangue ul li.niveauxUN > div").removeClass("rotation");
        if ($("#rechEtymoParLangue ul li.niveauxDEUX > h4").hasClass("active")) {
            $("#rechEtymoParLangue ul li.niveauxDEUX > h4").removeClass("active");
            $("#rechEtymoParLangue ul li.niveauxDEUX").removeClass("fondBleu");
            $("#rechEtymoParLangue ul li.niveauxDEUX > h4").removeClass("sectionActive");
            $("#rechEtymoParLangue ul li.niveauxDEUX > h4").removeClass("active")
        }
        if ($("#rechEtymoParLangue ul li.niveauxDEUX > ul").hasClass("ouvert")) $("#rechEtymoParLangue ul li.niveauxDEUX > ul").removeClass("ouvert");
        if ($("#rechEtymoParLangue ul li.niveauxDEUX > div").hasClass("rotation")) $("#rechEtymoParLangue ul li.niveauxDEUX > div").removeClass("rotation");
        if ($("#rechEtymoParLangue ul li.niveauxDEUX").hasClass("active")) $("#rechEtymoParLangue ul li.niveauxDEUX").removeClass("active");
        if ($("#rechEtymoParLangue ul li.niveauxTROIS > label").hasClass("active")) {
            $("#rechEtymoParLangue ul li.niveauxTROIS > label").removeClass("active");
            $("#rechEtymoParLangue ul li.niveauxTROIS").removeClass("fondBleu");
            $("#rechEtymoParLangue ul li.niveauxTROIS > label").removeClass("sectionActive");
            $("#rechEtymoParLangue ul li.niveauxTROIS > label").removeClass("active")
        }
        if ($("#rechEtymoParLangue ul li.niveauxTROIS > ul").hasClass("ouvert")) $("#rechEtymoParLangue ul li.niveauxTROIS > ul").removeClass("ouvert");
        if ($("#rechEtymoParLangue ul li.niveauxTROIS > div").hasClass("rotation")) $("#rechEtymoParLangue ul li.niveauxTROIS > div").removeClass("rotation");
        if ($("#rechEtymoParLangue ul li.niveauxTROIS").hasClass("active")) $("#rechEtymoParLangue ul li.niveauxTROIS").removeClass("active");
        var rDomaines = document.querySelector("#rechDomaines");
        rDomaines.scrollTop = 0;
        var rCatGram = document.querySelector("#rechCatGram");
        rCatGram.scrollTop = 0;
        var rLangue = document.querySelector("#rechEtymoParLangue");
        rLangue.scrollTop = 0;
    }


   // Clique extérieur ferme les menus personnalisés
    window.addEventListener("click", function (e) {
        if (document.getElementById("rechDomaines").contains(e.target));
        else if ($("#listeDomaine").next("ul").hasClass("ouvert")) {
            fermerMenuRechDomaine();
            fermerNiveaux();
            var rDomaines = document.querySelector("#rechDomaines");
            rDomaines.scrollTop = 0
        }
        if (document.getElementById("rechCatGram").contains(e.target));
        else if ($("#listeCatGram").next("ul").hasClass("ouvert")) {
            fermerMenuRechCatGram();
            fermerNiveaux();
            var rCatGram = document.querySelector("#rechCatGram");
            rCatGram.scrollTop = 0
        }
        if (document.getElementById("rechEtymoParLangue").contains(e.target));
        else if ($("#listeLangue").next("ul").hasClass("ouvert")) {
            fermerMenuRechListeLangue();
            fermerNiveaux();
            var rLangue =
                document.querySelector("#rechEtymoParLangue");
            rLangue.scrollTop = 0
        }
    });

    $("#rechercheAvance #frm_search_Av #fermeture").on("click", function () {
        $("#rechercheAvance").removeClass("active");
        $("#btLoupeAv").removeClass("active");
        fermerNiveaux();
    });
    $("#rechercheFixed #contenantRecherche #btLoupeAv").on("click", function () {
        $("#btLoupeAv").toggleClass("active");
        $("#rechercheAvance").toggleClass("active");
        if ($("#rechercheAvance").hasClass("active")) {
            fermerNiveaux();
            $("#rechIntegral").focus()
        }
        if ($("#sansResultat").hasClass("divSansResultatsIconesOuvert")) {
            $("#sansResultat").removeClass("divSansResultatsIconesOuvert");
            $("#sansResultat").addClass("divSansResultatsIconesFermer")
        }
    });

    function checkFrmAdvSearch() {
        if ($("#filtreEditions").data("forced") || $("#rechIntegral").val() != "" || $("#rechEntrees").val() != "" || $('input[name="chk_domains"]:checked').length > 0 || $('input[name="chk_gramcat"]:checked').length > 0) {
            $("#btRechercher").prop("disabled", false).addClass("criteresActifs");
            return true
        }
        $("#btRechercher").prop("disabled", true).removeClass("criteresActifs");
        return false
    }
    $('#rechDans, #rechEtymologieDatation, input[name="chk_langs"]').on("change",
        function () {
            if ($("#rechDans").val() != "" || $("#rechEtymologieDatation").val() != "" || $('input[name="chk_langs"]:checked').length > 0) {
                $("#filtreEditions").val("9").prop("disabled", true).data("forced", true);
                $("#chk_appear, #chk_disappear").prop("disabled", true)
            } else {
                $("#filtreEditions").prop("disabled", false).removeData("forced");
                $("#chk_appear, #chk_disappear").prop("disabled", false)
            }
            $("#filtreEditions").trigger("change")
        });
    $("#filtreEditions").on("change", function () {
        switch ($(this).val()) {
            case "0":
                $("#chk_appear, #chk_disappear").prop("disabled",
                    true);
                break;
            case "1":
                $("#chk_appear").prop("disabled", true);
                $("#chk_disappear").prop("disabled", false);
                break;
            case "9":
                if (!$(this).data("forced")) {
                    $("#chk_appear").prop("disabled", false);
                    $("#chk_disappear").prop("disabled", true)
                }
                break;
            default:
                $("#chk_appear").prop("disabled", false);
                $("#chk_disappear").prop("disabled", false)
        }
        checkFrmAdvSearch()
    });
    $("#rechIntegral, #rechEntrees").on("keyup", function () {
        checkFrmAdvSearch()
    });
    $("#rechIntegral, #rechEntrees").on("change", function () {
        checkFrmAdvSearch()
    });
    $("#frm_search_Av").on("submit", function ($event) {
        if (!checkFrmAdvSearch()) {
            $event.preventDefault();
            $event.stopPropagation()
        }
    });
    
    function Buttontoggle() {
        var t = document.getElementById("myButton");
        if (t.value == "YES") {
            t.value = "NO";
        } else if (t.value == "NO") {
            t.value = "YES";
        }
    }
    
    
    
    
    ////////// RECHERCHER AVANCÉE //////////
    
    
    
    //$("#btViderRecherche, #btViderRechercheAccueil").click(function () {
        //fermerNiveaux();
    //});
    
    // À l’intérieur de la fenêtre de recherche avancé, sur clique du mot Réinitialiser
    $("#btReinitialiser").on("click", function ($event) {
        $event.preventDefault();
        $("#rechIntegral, #rechEntrees").val("");
        $('#frm_search_Av input[type="checkbox"]').prop("checked", false);
        $("#frm_search_Av select option:selected").prop("selected", false);
        $("#chk_allform").prop("checked", true);
        $("#filtreEditions").val("9");
        $("#rechDans").trigger("change");
        $("#rechIntegral").focus()
    });
    
    $("#rechDans").trigger("change");

    $(".elementUnMenu").on("click", function () {
        $(this).next("ul").toggleClass("ouvert");
        $(this).children("span").toggleClass("rotation");
        $("li").removeClass("fondBleu");
        $("label").removeClass("sectionActive");
        $(this).parent().toggleClass("options");
        $(this).children("div").toggleClass("rotation")
    });
    
    ////// Recherche par domaine
    
    // Ouvrir menu Recherche par Domaine
    function ouvrirMenuRechDomaine() {
        $("#rechDomaines").addClass("options");
        $("#listeDomaine").next("ul").addClass("ouvert");
        $("#listeDomaine span").addClass("rotation");
        $("#listeDomaine").addClass("active");
    }
    
    // Fermer menu Recherche par Domaine
    function fermerMenuRechDomaine() {
        $("#rechDomaines").removeClass("options");
        $("#listeDomaine").next("ul").removeClass("ouvert");
        $("#listeDomaine span").removeClass("rotation");
        $("#listeDomaine").removeClass("active");
        var rDomaines = document.querySelector("#rechDomaines");
        rDomaines.scrollTop = 0
    }
    
    ////// Recherche par Catégories Grammaticales
    
    // Ouvrir menu Recherche par Catégories Grammaticales
    function ouvrirMenuRechCatGram() {
        $("#rechCatGram").addClass("options");
        $("#listeCatGram").next("ul").addClass("ouvert");
        $("#listeCatGram span").addClass("rotation");
        $("#listeCatGram").addClass("active");
    }

    // Fermer menu Recherche par Catégories Grammaticales
    function fermerMenuRechCatGram() {
        $("#rechCatGram").removeClass("options");
        $("#listeCatGram").next("ul").removeClass("ouvert");
        $("#listeCatGram span").removeClass("rotation");
        $("#listeCatGram").removeClass("active");
        var rCatGram = document.querySelector("#rechCatGram");
        rCatGram.scrollTop = 0
    }

    ////// Recherche par Liste de langue
    
    // Ouvrir menu Recherche par Liste de langue
    function ouvrirMenuRechListeLangue() {
        $("#rechEtymoParLangue").addClass("options");
        $("#listeLangue").next("ul").addClass("ouvert");
        $("#listeLangue span").addClass("rotation");
        $("#listeLangue").addClass("active");
    }
    
    // Fermer menu Recherche par Liste de langue
    function fermerMenuRechListeLangue() {
        $("#rechEtymoParLangue").removeClass("options");
        $("#listeLangue").next("ul").removeClass("ouvert");
        $("#listeLangue span").removeClass("rotation");
        $("#listeLangue").removeClass("active");
        var rLangue = document.querySelector("#rechEtymoParLangue");
        rLangue.scrollTop = 0
    }
    
    ////// Ouverture de menus personnalisés
    
    $("#listeDomaine").on("click", function () {
        if ($(this).hasClass("is-active")) {
            //console.log("ouvert");
            ouvrirMenuRechDomaine();
            if ($("#listeCatGram").hasClass("is-active")) {
                fermerMenuRechCatGram();
            }
            if ($("#listeLangue").hasClass("is-active")) {
                fermerMenuRechListeLangue();
            }
            $(this).children("span").addClass("rotation");
        } else {
            //console.log("fermé");
            fermerMenuRechDomaine();
            $(this).children("span").removeClass("rotation");
        }
    });
    
    $("#listeCatGram").on("click", function () {
        if ($(this).hasClass("is-active")) {
            //console.log("ouvert");
            ouvrirMenuRechCatGram();
            if ($("#listeDomaine").hasClass("is-active")) {
                fermerMenuRechDomaine();
            }
            if ($("#listeLangue").hasClass("is-active")) {
                fermerMenuRechListeLangue();
            }
            $(this).children("span").addClass("rotation");
        } else {
            //console.log("fermé");
            fermerMenuRechCatGram();
            $(this).children("span").removeClass("rotation");
        }
    });
    
    $("#listeLangue").on("click", function () {
        if ($(this).hasClass("is-active")) {
            //console.log("ouvert");
            ouvrirMenuRechListeLangue();
            if ($("#listeDomaine").hasClass("is-active")) {
                fermerMenuRechDomaine();
            }
            if ($("#listeCatGram").hasClass("is-active")) {
                fermerMenuRechCatGram();
            }
            $(this).children("span").addClass("rotation");
        } else {
            //console.log("fermé");
            fermerMenuRechListeLangue();
            $(this).children("span").removeClass("rotation");
        }
    });
    
    
    ///// Menus personnalisés - Recherche par domaines et Recherche par catégorie grammaticale
    ///// niveauxUN et niveauxDEUX

    // On click - Titre H4
    $("#rechDomaines ul li.niveauxUN > h4, #rechCatGram ul li.niveauxUN > h4, #rechDomaines ul li.niveauxDEUX > h4, #rechCatGram ul li.niveauxDEUX > h4").on("click", function () {
        if ($(this).hasClass("sectionActive")) {
            $(this).removeClass("sectionActive"); // titre (h4) section non active
            $(this).next("ul").removeClass("ouvert"); // fermeture du sous-menu (ul)
            $(this).prev().removeClass("rotation"); // rotation du chevron - potition initiale
            $(this).parent("li").removeClass("fondBleu"); // fond blanc rangée (li)
        } else {
            $(this).addClass("sectionActive"); // titre (h4) section active
            $(this).next("ul").addClass("ouvert"); // ouverture du sous-menu (ul)
            $(this).prev().addClass("rotation"); // rotation du chevron - potition vers le bas
            $(this).parent("li").addClass("fondBleu"); // fond bleu rangée (li)
        }
    });
    
    // On click - Chevron
    $("#rechDomaines ul li.niveauxUN > button.fa, #rechCatGram ul li.niveauxUN > button.fa, #rechDomaines ul li.niveauxDEUX > button.fa, #rechCatGram ul li.niveauxDEUX > button.fa").on("click", function () {
        if ($(this).prev().hasClass("sectionActive")) {
            $(this).prev().removeClass("sectionActive"); // titre (h4) section non active
            $(this).next("ul").removeClass("ouvert"); // fermeture du sous-menu (ul)
            $(this).removeClass("rotation"); // rotation du chevron - potition initiale
            $(this).parent("li").removeClass("fondBleu"); // fond blanc rangée (li)
        } else {
            $(this).prev().addClass("sectionActive"); // titre (h4) section active
            $(this).next("ul").addClass("ouvert"); // ouverture du sous-menu (ul)
            $(this).addClass("rotation"); // rotation du chevron - potition vers le bas
            $(this).parent("li").addClass("fondBleu"); // fond bleu rangée (li)
        }
    });
    
    ///// Menus personnalisés - Recherche etymologie par langue
    ///// niveauxUN et niveauxDEUX
    
    // On click Titre H4
    $("#rechEtymoParLangue ul li.niveauxUN > h4, #rechEtymoParLangue ul li.niveauxDEUX > h4").on("click", function () {
        if ($(this).hasClass("sectionActive")) {
            $(this).removeClass("sectionActive"); // titre (h4) section non active
            $(this).next("ul").removeClass("is-active"); // fermeture du sous-menu (ul)
            $(this).prev().removeClass("rotation"); // rotation du chevron - potition initiale
            $(this).parent("li").removeClass("fondBleu"); // fond blanc rangée (li)
        } else {
            $(this).addClass("sectionActive"); // titre (h4) section active
            $(this).next("ul").addClass("is-active"); // ouverture du sous-menu (ul)
            $(this).prev().addClass("rotation"); // rotation du chevron - potition vers le bas
            $(this).parent("li").addClass("fondBleu"); // fond bleu rangée (li)
        }
    });
    
    // On click - Chevron
    $("#rechEtymoParLangue ul li.niveauxUN > button.fa, #rechEtymoParLangue ul li.niveauxDEUX > button.fa").on("click", function () {
        if ($(this).prev().hasClass("sectionActive")) {
            $(this).prev().removeClass("sectionActive"); // titre (h4) section non active
            $(this).next().next("ul").removeClass("is-active"); // fermeture du sous-menu (ul)
            $(this).removeClass("rotation"); // rotation du chevron - potition initiale
            $(this).parent("li").removeClass("fondBleu"); // fond blanc rangée (li)
        } else {
            $(this).prev().addClass("sectionActive"); // titre (h4) section active
            $(this).next().next("ul").addClass("is-active"); // ouverture du sous-menu (ul)
            $(this).addClass("rotation"); // rotation du chevron - potition vers le bas
            $(this).parent("li").addClass("fondBleu"); // fond bleu rangée (li)
        }
    });


    $('.menuPerso input[type="checkbox"], .menuPersoEtroit input[type="checkbox"]').change(function (e) {
        var checked = $(this).prop("checked"),
            container = $(this).parent(),
            siblings = container.siblings();
        container.find('input[type="checkbox"]').prop({
            indeterminate: false,
            checked: checked
        });
        checkFrmAdvSearch();

        function checkSiblings(el) {
            var parent = el.parent().parent(),
                all = true;
            el.siblings().each(function () {
                var returnValue = all = $(this).children('input[type="checkbox"]').prop("checked") === checked;
                return returnValue
            });
            if (all && checked) {
                parent.children('input[type="checkbox"]').prop({
                    indeterminate: false,
                    checked: checked
                });
                checkSiblings(parent)
            } else if (all && !checked) {
                parent.children('input[type="checkbox"]').prop("checked", checked);
                parent.children('input[type="checkbox"]').prop("indeterminate",
                    parent.find('input[type="checkbox"]:checked').length > 0);
                checkSiblings(parent)
            } else el.parents("li").children('input[type="checkbox"]').prop({
                indeterminate: true,
                checked: false
            })
        }
        checkSiblings(container)
    })
});
