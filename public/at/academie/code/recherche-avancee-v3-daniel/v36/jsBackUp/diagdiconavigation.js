﻿/// Gère la sélection d'un mot et la demande de navigation vers prolexis ou autre élément supervisant une WebView
///
/// Gère deux types de navigation : sur un mot simple et mot dans son contexte.
/// - Navigation sur un mot simple : on renvoie une url de la forme prolexis:navigate?word=mot
/// - Navigation mot dans son contexte : si on parvient à retrouver la paragraphe où se trouve le mot on renvoie une url prolexis:navigate
///   Le manager de la Webview rappelle alors les fonctions DiagDicoNavigation.getParagraphText, DiagDicoNavigation.getWordStartIndex
///    et DiagDicoNavigation.getWordLength afin de récupérer toutes les informations nécessaires.
///
/// Par défaut, le handler de navigation sur les mots est positionné sur les élements ayant une classe "pl-navigable".
/// En cas de double clique, on ajoute un paramètre action=dblclick à l'url renvoyée.
///
///
var DiagDicoNavigation = ( function() {
        /****** Méthodes publiques *****/

        /// Ajoute un handler d'évenement pour effectuer une consulation du mot cliqué.
        /// Par défaut, on branche sur l'événement simple clic.
        ///
        /// @param selectorTarget  Chaîne représentant un sélecteur JQuery (CSS)
        /// ciblant le ou les éléments sur lesquel brancher le handler.
        ///
        var addLookUpWordHandler;

        /// Consulte le mot cliqué.
        ///
        /// @param $event  L'évènement JQuery qui a déclenché la fonction
        var lookupword;

        /// Renvoie le texte du paragraphe sur lequel on a cliqué.
        /// Vide si le paragraphe n'a pas été localisé.
        ///
        var getParagraphText;

        /// Donne la position du mot cliqué dans le paragraphe (cf. getParagraphText)
        ///
        var getWordStartIndex;

        /// Donne la longeur du mot cliqué (cf. getParagraphText).
        ///
        var getWordLength;

        /// Annule un éventuel surlignage déjà existant.
        ///
        var cancelHighlight;


        /****** Membres privées *****/

		var _R_ACC = "navy";

        /// Le timer du clic/double cli
        ///
        var _timerNavigate;

        var _plNavigationOptions;

        /// Le mot couramment surligné.
        ///
        var _currentHighlight = null;

        /// Le paragraphe, contexte du mot couramment surligné [String].
        ///
        var _paragraphText = "";

        /// L'offset de début du mot couramment surligné dans le paragraphe [int].
        ///
        var _startIndex = 0;

        /// Longueur du mot couramment surligné [int].
        ///
        var _length = 0;

        /// Objet Jquery contenant un élément DOM stockant les valeurs de retour
        /// des script pour qu'elles soient récupérées par le code natif.
        ///
        var _$scriptResult;

        /// Trouve le mot (si besoin) et le met en relief dans le html
        ///
        /// @param aNode        Noeud contenant le mot a mettre en relief
        /// @param aSelection   Noeud ayant reçu l'evenement click
        /// @param aWord        Mot a mettre en relief (si null on le calcul grace au 2 param precedent)
        /// @return             retourne le Noeud mis en relief
        var _reliefWord;
        
        /// met a jour les variables que le natif doit recuperer
        ///
        /// @param selectedNode  Noeud a partir du quel toutes les variables sont calculé
        var _setParamForNatif;

        /// A partir d'une sélection tente de déterminer si une selection contient du texte.
        /// C'est-à-dire si il y a eut une sélection réalisée par l'utilisateur.
        ///
        /// @param iSelection Sélection retournée par getSelection.
        var _isTextSelected;
        
        /// Recrée un objet sélection à partir d'un objet event. 
        ///
        /// @param event Un objet event généré par un évenement utilisateur (click).
        var _recreateSelection;

        /****** Implémentations des méthodes privées *****/

        /// Test si JQuery est chargée
        ///
        function isJQueryLoaded() {
            return ( typeof jQuery != 'undefined');
        };

        /// Teste si un caractère est une lettre (au sens d'Unicode).
        ///
        /// @param c  Code Unicode du caractère.
        ///
        /// @return true si c'est une lettre, false sinon.
        ///
        function isLetter(c) {
            // NOTE: Il se trouve que JavaScript, bien qu'utilisant Unicode pour
            // représenter les caractères, est grossièrement ignorant de ce genre de
            // choses. Il faut donc l'implémenter nous-mêmes.

            // Traiter en priorité les cas fréquents
            // -------------------------------------
            // Lettres ASCII majuscules.
            //
            if (c >= 0x0041 && c <= 0x005A) {
                return true;
            }
            // Lettres ASCII minuscules.
            //
            else if (c >= 0x0061 && c <= 0x007A) {
                return true;
            }
            // Reste du bloc ASCII.
            //
            else if (c <= 0x007F) {
                return false;
            }
            // On peut ensuite finasser
            // ------------------------
            // Latin-1 Supplement.
            //
            else if (c >= 0x00C0 && c <= 0x00FF && c != 0x00F7) {
                return true;
            }
            // Latin Extended-A, Latin Extended-B.
            //
            else if (c >= 0x0100 && c <= 0x0241) {
                return true;
            }
            // IPA Extensions.
            //
            else if (c >= 0x0250 && c <= 0x02AF) {
                return true;
            }

            return false;
        };

        /// Teste si un caractère est de type "mot".
        ///
        /// @param c  Code Unicode du caractère.
        ///
        /// @return true si le caractère peut faire partie d'un mot, false sinon.
        ///
        function isWordChar(c) {
            return c == 0x002D// tiret
            || c == 0xfeff    // MANTIS 4695 : ZERO WIDTH NO-BREAK SPACE
            || c == 0x0027 || c == 0x2019// apostrophes droite et courbe
            || isLetter(c);
        }

        function changeTextForNatif(aText) {
            var textForNatif = aText;

            // Mantis 4695 :
            // On supprime les Zero Width No-Break Space (&#8209;) qui aurait été ajouté dans le texte => car introduise un decalage dans le texte
            textForNatif = textForNatif.replace(/[\uFEFF]/g, "");

            // On remplace les &,< et > par des # pour évité la transformation html (eg:  & en &amp;)
            // et ainsi créée un decalage dans pour la récuperation du mot
            textForNatif = textForNatif.replace(/[>&<]/g, "#");

            return textForNatif;
        }

        /// Transforme une paire de noeuds, dont l'un doit être l'ancêtre de l'autre,
        /// en un texte et un range, qui représente la position du noeud descendant
        /// dans le texte de l'ancêtre.
        ///
        /// NOTE: Les données en sortie sont stockées dans des variables globales.
        /// Pas beau, mais il n'y a pas moyen de faire autrement car on doit y accéder
        /// ultérieurement depuis le code natif (en l'absence de WebScript sur iPhone).
        ///
        function nodePairToTextAndRange(iAncestorNode, iDescendantNode) {
            if (iAncestorNode == null || iDescendantNode == null)
                return null;

            // Si l'ancêtre et le descendant sont confondus, mettre à jour les
            // indices de début et de fin.
            //
            if (iAncestorNode == iDescendantNode) {
                _startIndex = _paragraphText.length;
                var aText = (iDescendantNode.innerText) ? iDescendantNode.innerText : iDescendantNode.textContent;
                
                aText = changeTextForNatif(aText);
                
                _paragraphText += aText;
                
                _length = aText.length;
            }
            // Sinon, récursion traditionnelle.
            //
            else {
                for (var aNode = iAncestorNode.firstChild; aNode != null; aNode = aNode.nextSibling) {
                    // Texte : ajouter au paragraphe.
                    //
                    if (aNode.nodeType == Node.TEXT_NODE) {
                        var aText = normalizeSpace(aNode.textContent);

                        aText = changeTextForNatif(aText);

                        _paragraphText += aText;
                    }
                    // Élément : récursion.
                    //
                    else if (aNode.nodeType == Node.ELEMENT_NODE) {
                        nodePairToTextAndRange(aNode, iDescendantNode);
                    }
                }
            }
        }

        /// Normalise les espaces dans une chaîne de caractère.
        /// Toute séquence de blancs consécutifs est remplacée par un seul espace.
        /// Par "blanc", on entend : espace, tabulation, retours à la ligne ('\n' et
        /// '\r').
        ///
        function normalizeSpace(iText) {
            if (iText == null)
                return null;

            return iText.replace(/\s+/g, " ");
        }

        /// Déclenche une navigation vers le manager de WebView en utilisant le tableau _plNavigationOptions.
        /// Il est sérialisé et passé en paramètre GET.
        ///
        function plNavigate() {
            // Annulation du timer.
            //
            if (_timerNavigate) {
                clearTimeout(_timerNavigate);
                _timerNavigate = null;
            }

            // Sérialise les options en GET.
            //
            //window.location.replace("prolexis:navigate?" + jQuery.param(_plNavigationOptions));
            mainWordNavigate(_plNavigationOptions);
        }


        function _reliefWord(aNode, aSelection, aWord) {

            if (aWord == undefined) {
                // À partir du texte du noeud et de la position de la sélection, déterminer
                // le mot le plus probable.
                //
                var aText = aNode.textContent;
                var p = aSelection.anchorOffset;
                var a = p;
                var b = p;

                while (a > 0 && isWordChar(aText.charCodeAt(a - 1)))--a;
                while (b < aText.length && isWordChar(aText.charCodeAt(b)))++b;

                // Si l'on n'a pas pu détecter de mot, abandonner.
                //
                if (a == b) {
                    return;
                }
                aWord = aText.substr(a, b - a);
            }

            // Mettre en relief le mot.
            //
            var aParentNode = aNode.parentNode;
            var aDocument = aNode.ownerDocument;
            var aTextBefore = aDocument.createTextNode(aText.substr(0, a));
            var aHighlightedText = aDocument.createTextNode(aWord);
            var aHighlightElement;

            if (aParentNode.namespaceURI == 'http://www.w3.org/2000/svg') {// Si le parent est un objet SVG
                aHighlightElement = aDocument.createElementNS('http://www.w3.org/2000/svg', "tspan");
            } else {// sinon
                aHighlightElement = aDocument.createElement("span");
            }
            aHighlightElement.setAttribute("class", "link-highlight");

            aHighlightElement.appendChild(aHighlightedText);
            var aTextAfter = aDocument.createTextNode(aText.substr(b, aText.length - b));

            if (aText.substr(0, a).length == 0 && aText.substr(b, aText.length - b).length == 0) {
                aParentNode.replaceChild(aHighlightElement, aNode);
            } else {
                aParentNode.replaceChild(aTextAfter, aNode);
                aParentNode.insertBefore(aHighlightElement, aTextAfter);
                aParentNode.insertBefore(aTextBefore, aHighlightElement);
            }
            // Mémoriser le mot surligné pour une éventuelle annulation ultérieure.
            //
            _currentHighlight = aHighlightElement;
            
            return aHighlightElement;
        };
        
        function _setParamForNatif(selectedNode){
            // Récupérer le paragraphe contenant le mot.
            //
            _paragraphText = "";
            _startIndex = -1;
            _length = -1;

            // Afin d'imposer le moins de contraintes possibles sur la structure du
            // HTML, je me fonde sur la propriété CSS "display" pour déterminer ce
            // qui est un paragraphe.
            //
            if (selectedNode != null) {
                var aNode = selectedNode;
                do {
                    // NOTE: getComputedStyle() est une extension non-standard, disponible sur
                    // WebKit et Gecko, pas IE.
                    //
                    var aStyle = window.getComputedStyle(aNode, null);
                    if (aStyle.display == "block")
                        break;
                    aNode = aNode.parentNode;
                } while (aNode != null);
    
                // À ce stade, soit on est tombé sur un élément de type paragraphe (le cas
                // nominal), soit on n'a rien du tout (échec, censé ne pas arriver).
                //
                if (aNode != null) {
                    nodePairToTextAndRange(aNode, selectedNode);
                }
            }
        };
        
        _isTextSelected = function(iSelection)
        {
        	if (typeof iSelection.type != 'undefined') {
        		return (iSelection.type == "Range");
        	}
        	// Méthode alternative, on suppose qu'un sélection avec une seule Range avec 
        	// un début et une fin identique n'a pas de texte sélectionné. 
        	//
        	else if (iSelection.rangeCount == 1) {
        		var aRange = iSelection.getRangeAt(0);
        		return (aRange.startOffset != aRange.endOffset);
        	}
        	return true;
        }; // _isTextSelected
        
        
        _recreateSelection = function(event) {
        	// On récupére la position du caret manuellement.
        	var range = document.caretRangeFromPoint(event.clientX, event.clientY);
        	
            return {
            	type         : 'Custom',
             	anchorNode   : range.startContainer,
				baseNode     : range.startContainer,
				focusNode    : range.startContainer,
                anchorOffset : range.startOffset,
				baseOffset   : range.startOffset,
				focusOffset  : range.startOffset
        	}
        }; // _recreateSelection


        /****** Implémentations méthodes publiques *****/

        addLookUpWordHandler = function(selectorTarget) {
            if (window.getSelection) {
                // Navigateurs récents: WebKit et IE>8
                //
                //$(selectorTarget).click(lookupWord);
                $(selectorTarget).click(function($event) {
                    DiagDicoNavigation.lookupWord($event);
                });
                $(selectorTarget).dblclick(function($event) {
                    // Un timer est en cours, on annule et passe en appel direct en
                    // en ajoutant le paramètre du double-clique.
                    //
                    if (_timerNavigate) {
                        clearTimeout(_timerNavigate);
                        _timerNavigate = null;
                        _plNavigationOptions.action = "dblclick";
                        plNavigate();
                    } else {
                        // Ne devrait pas se produire...
                        //
                        DiagDicoNavigation.lookupWord($event);
                    }
                });
            } else {
                // IE<9, on branche sur le double clique.
                //
                $(selectorTarget).dblclick(lookupWord);
            }
        };

        lookupWord = function($event) {
            // Récupérer la sélection.
            //
            var aSelection;

            // Réinitialisation des paramètres
            //
            _plNavigationOptions = {};

			// En cas de clic sur les liens renvois, on annule la navigation.
			//
			if (typeof $event != 'undefined') {
				var target = $event.target;

                // [Mantis 4772] : ajout de "typeof target != 'undefined'" pour supprimé une alert javascript sur IE8
                //
				// On liste ici les balises ou on ne veux pas que le lien hypertexte s'execute
				//
				if ( typeof target != 'undefined' && ((target.className.indexOf("s_Entree") > -1)         // <Entree> et <Fem>
                                                   || (target.className.indexOf("s_NumHom") > -1)         // <NumHom>
											       || (target.className.indexOf("s_Marq") > -1)           // <Marq>
												   || (target.className.indexOf("s_Phon") > -1)           // <Phon>
												   || (target.className.indexOf("s_Renv") > -1)           // <Renv>
												   || (target.className.indexOf("s_Date") > -1)           // <Date>
												   || (target.className.indexOf("s_NoDivRomToggle") > -1) // élément toggle
												   || (target.className.indexOf("blocSectionLiens") > -1) // blocSectionLiens
												   || (target.className.indexOf("s_SsEntree") > -1))      // <SsEntree> et <xnt> 
				){
					return;
				}
				// On ajoute un blindage pour vérifier qu'on clique pas sur un lien.
				// Une autre vérification est faite plus loin, je préfére garder les deux.
				//
				else if(($(target).is('a') || ($(target).parents('a').length > 0)) && $(target).attr("class").indexOf(_R_ACC) == -1){
					return;
				}
			}
			

            if (window.getSelection) {
                // WebKit, Gecko
                var aSelection = window.getSelection();
                
                // Cas part sur iPhone, le window.getSelection ne fonctionne plus...
                //
                if (aSelection.type == "None") {
                	// On créé une sélection manuelle
                	//
                	aSelection = _recreateSelection($event);
                }
                
                // TODO modif ici pour éviter de lancer une 2eme fois l'event de prolexis:navigate
                if ($(".link-highlight").length > 0) {
                    return;
                }
            } else {
                // Internet Explorer inférieur à 9
                //
                var aText = document.selection.createRange().text;

                if (aText == "") {
                    // Aucune sélection récupérée, surement déclenché par un simple click.
                    // On passe.
                    //
                    return;
                }

                // On ne peut pas mieux faire, déclenchement d'une navigation sur mot simple.
                //
                //window.location.replace("prolexis:navigate?word=" + aText);
                _plNavigationOptions.word = aText;
                plNavigate();

                return;
            }

            if ((aSelection == null) || _isTextSelected(aSelection)) {
                return;
            }
            
            // Récupérer le noeud "ancre" de la sélection.
            // Normalement, pour une simple pression, la sélection est d'épaisseur nulle.
            //
            var aNode = aSelection.anchorNode;
            if (aNode == null) {
                return;
            }

            // [BUG#2186] Si le noeud n'est pas un noeud texte, abandonner. En effet,
            // si on est sur un élément et qu'on demande le contenu texte, on va
            // récupérer la concaténation de tous les noeuds textes descendants de
            // l'élément, ce qui peut conduire à des cas tordus.
            //
            if (aNode.nodeType != Node.TEXT_NODE) {
                return;
            }

            // Chercher s'il y a un lien parmi les ancêtres du noeud. S'il y en a un,
            // on abandonne pour ne pas interférer avec le traitement du lien.
            //
            for (var aTempNode = aNode; aTempNode != null; aTempNode = aTempNode.parentNode) {
                if (aTempNode.nodeType == Node.ELEMENT_NODE && aTempNode.localName == "a")
					// a l'exception des renvoi de classe _R_ACC
					if(aTempNode.className.indexOf(_R_ACC) != -1) {
						return;
					}
            }

            // Si on a cliqué sur une entrée caché <lct> on ne continue pas la navigation hypertexe
            // les entrées cachées renvoient vers la même def que l'entrée principal (courante)
            // (cas part si parent est <svg>)
            //
            var _isLctEntry = false;
            
            if (aNode.parentNode.namespaceURI == 'http://www.w3.org/2000/svg') {// Si le parent est un objet SVG
                _isLctEntry = (aNode.parentNode.className.baseVal.indexOf("s_lct") != -1);
            } else {// sinon
                _isLctEntry = (aNode.parentNode.className.indexOf("s_lct") != -1);
            }
            
            if (!_isLctEntry) {

                var aHighlightElement = _reliefWord(aNode, aSelection);
                _setParamForNatif(aHighlightElement);
    
    			// Cas spécial, le paragraphe ne contient qu'un seul mot, une navigation mot dans son contexte 
    			// ne fonctionnera pas correctement, du coup envoi le mot pour un id constitué du mot + la gramLetter de la vedette
    			// commme pour une navigation par click droit dans les synos ou les combinaisons
    			// 
				if ($.trim(_paragraphText).split(" ").length == 1) {
    			
    				var word = $.trim(_paragraphText);
    				var index =  $(".btn_entry").index($("#btn_entry_selected"));
    				var gramLetter = $("#gramLetter_" + index).text();

					// On temporise la navigation pour qu'elle puisse être annulé (double-clique).
					// Si un timer court on le laisse finir.
					//
					if (_timerNavigate) {
		                clearTimeout(_timerNavigate);
                		_timerNavigate = null;
            		}
					
					if (!_timerNavigate && word.length > 0) {    				
	    				_timerNavigate = setTimeout(function() {
	    					_plNavigationOptions.id = word + "," + gramLetter;
	    					_plNavigationOptions.word = word;
	    					plNavigate();
    						//window.location.href = "prolexis:navigate?word=" + word + "&id=" + word + "," + gramLetter;
						}, 150);
					}
    			}
    			else {
					// Si on a réussi à localiser le paragraphe, je déclenche une navigation mot dans son contexte
					//
					if (_paragraphText.length > 0 && _startIndex >= 0 && _length >= 0) {
						// On ne passe aucune options
					}
					// Sinon, je déclenche une navigation sur un mot simple.
					//
					else if (aHighlightElement) {
						_plNavigationOptions.word = aHighlightElement.textContent;
					}
	
					// On temporise la navigation pour qu'elle puisse être annulé (double-clique).
					// Si un timer court on le laisse finir.
					//
					if (_timerNavigate) {
						clearTimeout(_timerNavigate);
						_timerNavigate = null;
					}
					
					if (!_timerNavigate) {
						_timerNavigate = setTimeout(function() {
							plNavigate();
						}, 150);
					}
                }
            }
        };


        getParagraphText = function() {
            _$scriptResult.text(_paragraphText);
            return _paragraphText;
        };

        getWordStartIndex = function() {
            _$scriptResult.text(_startIndex);
            return _startIndex;
        };

        getWordLength = function() {
            _$scriptResult.text(_length);
            return _length;
        };

        cancelHighlight = function() {
            // NOTE: On se contente d'annuler la classe CSS de l'élément surligné. On
            // ne prend pas la peine de le recoller avec ses voisins. En effet, 
            // l'annulation de l'highlight n'est censée intervenir que très rarement,
            // dans le cas où la navigation a échoué.
            //
            if (_currentHighlight != null) {
                _currentHighlight.removeAttribute("class");
            }

            // TODO modif ici pour évité qu'un link-highlight reste actif
            $(".link-highlight").removeClass("link-highlight");
        };

        /****** Attachement au chargement du DOM des handlers par défaut *****/
        if (isJQueryLoaded()) {
            $(document).ready(function() {
                // Création de l'élément stockant les valeurs de retour des scripts
                //
                if ($('#scriptResult').length == 0) {
                    $(document.body).append($("<div id='scriptResult' style='display:none;'></div>"));
                }
                _$scriptResult = $('#scriptResult');

                // Branchement des handlers par défaut.
                //
                addLookUpWordHandler('.s_Article, .s_Notice');
            });
        }

        /****** Publications méthodes publiques *****/
        return {
            addLookUpWordHandler : addLookUpWordHandler,
            lookupWord : lookupWord,
            getParagraphText : getParagraphText,
            getWordStartIndex : getWordStartIndex,
            getWordLength : getWordLength,
            cancelHighlight : cancelHighlight
        };
    }());
