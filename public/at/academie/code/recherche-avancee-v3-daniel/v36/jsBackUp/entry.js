

/**
 * Fonction appelée par DiagDicoNavigation lors d'un navigation sur un mot.
 */
function mainWordNavigate(iNavigationOptions)
{
	var aPara = DiagDicoNavigation.getParagraphText();
	var aStart = DiagDicoNavigation.getWordStartIndex();
	var aLength = DiagDicoNavigation.getWordLength();
	
	$.ajax({
		type: "POST",
		url: "../navigate",
		dataType: "json",
		data: {
			paragraph: aPara,
			start: aStart,
			length: aLength
		},
		success: handleNavigationSuccess,
		error: function() {
			DiagDicoNavigation.cancelHighlight();
		}
	});
} // mainWordNavigate

//Traitement du résultat de la navigation sur les mots.
//
//
function handleNavigationSuccess(data, jqXHR, textStatus)
{
	if ($.isArray(data.result)) {
		var tabResults = data.result;
		var $linkContainer = $('#acsenceurInfoBulle ul');
		var $title = $('#contenantHyperlienInfoBulle h3');
		
		if (tabResults.length > 0) {
			if (tabResults.length == 1) {
				DiagDicoNavigation.cancelHighlight();
				if (typeof tabResults[0].url !== "undefined") {
					document.location.href = tabResults[0].url;
					return 0;
				}
			}
			else {
				var aWord = DiagDicoNavigation.getParagraphText();
				var aTitle;
				$linkContainer.empty();
				
				aWord = aWord.substr(DiagDicoNavigation.getWordStartIndex(), DiagDicoNavigation.getWordLength());
				$title.html("<span class=\"world-highlight\">"+ aWord +"</span>&nbsp;: ce mot correspond à plusieurs entrées&nbsp;:");
				
				for(var i=0; i < tabResults.length; i++){
					$linkContainer.append('<li><a href="'+ tabResults[i].url +'" data-score="'+ tabResults[i].score +'">'+ makeResultLabel(tabResults[i]) +'</a></li>');
				}
				
				$('#acsenceurInfoBulle').show();
			}
		}
		else{
			$('#acsenceurInfoBulle').hide();
			$title.html("Ce mot n’a pas été trouvé dans le Dictionnaire de l’Académie.");
		}
		
		$('#contenantHyperlienInfoBulle').show();
		$("#contenantHyperlienInfoBulle").position({
	        of: $('.link-highlight'),
	        my: "left bottom",
	        at: "left top"
	    });
		
		$('.s_Article, .s_Notice').on("click", clickWithDialog);
	}
	else{
		DiagDicoNavigation.cancelHighlight();
	}
} // handleNavigationSuccess

// Click dans la définition quand le dialogue de navigation sur les mots est ouvert.
// Ferme le dialogue et retire la sélection pour que DiagDicoNavigation soit prêt.
// Ensuite appel direct à DiagDicoNavigation.lookupWord en utilisant le même événement.
//
function clickWithDialog($event)
{
	$('.s_Article, .s_Notice').off("click", clickWithDialog);
	$('#contenantHyperlienInfoBulle').hide();
	DiagDicoNavigation.cancelHighlight();
	DiagDicoNavigation.lookupWord($event);
}

// Demande de voisinage alphabétique.
//
function requestNeighborhood(iDirection, iId)
{
	if (typeof iId !== "undefined") {
		$.ajax({
			type: "POST",
			url: "",
			dataType: "json",
			data: {
				direction: iDirection,
				entry: iId
			},
			success: handleNeighborhood
		});
	}
} // requestNeighborhood

// Traitement du résultat d'une demande de voisinage alphabétique.
//
function handleNeighborhood(data, textStatus, jqXHR)
{
	if ($.isArray(data.result)) {
		var tabResults = data.result;
		var entryId = $('#txt_entry').val(); // On récupère l'id de l'entrée consultée.
		
		if (tabResults.length > 0) {
			var $linkContainer = $('#voisinage .colDroiteContenuDeuxBoutons ul');
			$linkContainer.empty();
			
			for(var i=0; i < tabResults.length; i++){
				$linkContainer.append('<li'+ ((entryId == tabResults[i].url) ? ' class="motActif"' : '') +'><a href="'+ tabResults[i].url +'">'+ tabResults[i].label +'</a></li>');
			}
		}
	}
} // handleNeighborhood

// Affiche la zone de rappel othographique à droite.
//
function showOrthoArea()
{
	var $orthoReminder = $(".s_ZoneOrtho .s_ZoneOrtho_contenu .s_OrthoNew").clone();
	if ($orthoReminder.length > 0) {
		var $linkContainer = $('#liensOrtho ul.listColDroite');
		$linkContainer.empty();
		
		$orthoReminder.each(function() {
			$linkContainer.append('<li><a href="#zoneOrtho">'+ $(this).html() +'</a></li>');
		});
		
		$('#liensOrtho').show();
	}
} //showOrthoArea


// Initialise le comportement des infos bulles, désactivé sur les écrans trop petits.
//
function initToolTip()
{
	// Gestion des infos bulles.
	//
	if ($(window).width() > 600) {
		$(".s_Article .s_Marq span, .s_Article .s_Meta span, .s_Article .s_ortho").each(function() {
			$(this).data('title', $(this).attr('title'));
			$(this).removeAttr('title');
			
			$(this).mouseenter(function($event){
				var $aTarget = $($event.currentTarget);
				var aContent = "";
				$event.preventDefault();
				$event.stopImmediatePropagation();
				$event.stopPropagation();
				
				if($aTarget.data('title')) {
					$('#contenantInfoBulle .infoBulle ul').empty();
					$('#contenantInfoBulle .infoBulle ul').append("<li><a href=\"#\">"+ $aTarget.data('title') +"</a></li>");
					
					$("#contenantInfoBulle").show();
					$("#contenantInfoBulle").position({
				        of: $aTarget,
				        my: "left top-7",
				        at: "right+10 top",
				        collision: "none"
				    });
				}
			});
			
			$(this).mouseleave(function() {
				$("#contenantInfoBulle").hide();
			});
		});
	}
} // initToolTip

// Fonction lancée au redimensionnement de la fenêtre.
//
function onWindowResize()
{
	if ($(window).width() > 768) {
		if ($('#colGauche #voisinage:visible, #colGauche #Historique:visible').length > 0) {
			// On affiche/masque les éléments qui ont pu être positionnés pour 
			// l'affichage en petit écran (showAreaInMobile et clickMenuLiensCentrale).
			//
			$('#colGauche .s_Article, #colGauche .s_Notice').show();
			$('#colGauche #voisinage, #colGauche #Historique').hide();
			
			// On rénitialise le menu mobile.
			//
			$('#menuLiensCentrale a').removeClass('active');
			$('#menuLiensCentrale a').first().addClass('active');
		} else {
            
        }
	}
	// On ferme la navigation sur les mots et l'infos bulle custom
	//
	$('#contenantHyperlienInfoBulle .hyperlienInfoBullefermeture').click();
	$("#contenantInfoBulle").hide();
} // onWindowResize

// Affiche une zone annexe dans la partie principale (#colGauche) lorsque l'on est en affichage mobile.
// iEltSelector : selecteur JQuery de l'élément à afficher (#voisinage, #Historique ou .s_Article).
//
function showAreaInMobile(iEltSelector)
{
	var $aMainArea = $('#colGauche');
	
	// On cache tous les éléments présent dans la zone principale
	$("#voisinage, #Historique, #content_gallica, #HistoireMot, .s_Article, .s_Notice", $aMainArea).hide();
	
	// Si besoin, on déplace l'élément dans la zone principale.
	//
	if ($(iEltSelector, $aMainArea).length == 0) {
		var $neighboorhood = $(iEltSelector).clone();
		$neighboorhood.appendTo($aMainArea);
	}
	// On l'affiche simplement.
	//
	else {
		$(iEltSelector).show();
	}
} // showAreaInMobile

// Gère les clics sur voisinage et historique dans le menu Mobile.
// $event : l'évenement JQuery original.
// $link : le lien cliqué.
// iEltSelector : selecteur JQuery de l'élément à afficher.
//
function clickMenuLiensCentrale($event, $link, iEltSelector)
{
	showAreaInMobile(iEltSelector);
	$('#menuLiensCentrale a').removeClass('active');
	$link.addClass('active');
	
	// Fermeture du menu.
	//
	$("#plus").trigger("click");
} // clickMenuLiensCentrale

// Initialise les différentes zones liées à Gallica.
// La vignette en colonne de droite et la zone de visualisation en zone centrale. 
//
function initGallica()
{
	var $gallicaThumb = $('#thumb_gallica');
	
	if ($gallicaThumb.length > 0) {
		var aWidth = Math.floor($('#colGauche .s_Article').width());
		var anUrl = $gallicaThumb.data('urlbase') +'/full/'+ aWidth*2 +',/0/native.jpg';
		var $gallicaContent = $("<div id=\"content_gallica\" style=\"display: none;\" data-urlbase=\""+ $gallicaThumb.data('urlbase') +"\"><a target=\"_blank\" title=\"Accéder au site Gallica pour consulter le document numérisé (nouvelle fenêtre)\" href=\""+ $gallicaThumb.data('externurl') +"\" tabindex=\"1\"><img src=\""+ anUrl +"\" width=\""+ aWidth +"\" alt=\"\"></a></div>");
		var $closeButton = $("<div id=\"precedent2\" tabindex=\"1\"><div id=\"preHaut\"></div><div id=\"preBas\"></div></div>");
		var image = new Image();
		
		// On charge la vignette
		//
        image.src = $gallicaThumb.data('urlthumb');
        image.onload = function () {
        	// Le succès du chargement conditionne l'affiche de la zone dans la colonne droite.
        	//
        	$gallicaThumb.prepend(image);
        	
        	// On met la vignette dans la colonne droite
    		//
    		$gallicaThumb.detach();
    		$gallicaThumb.appendTo('#colGallica .colDroiteContenu');
    		$gallicaThumb.show();
    		$('#colGallica').show();
        };
//        image.onerror = function () {
//            console.log('That image is not available.');
//        };
		
        $gallicaThumb.attr('role', 'button');
        $gallicaThumb.attr('tabindex', '0');
        
		$gallicaThumb.click(function($event) {
			$event.preventDefault();
			$event.stopPropagation();
			showGallica();
		});

		$gallicaThumb.keydown(function($event) {
			// Touche entrée
			//
			if ($event.which == 13) {
				showGallica();
			}
		});
		
		$('#lienCentralGallica').click(function($event){
			clickMenuLiensCentrale($event, $(this), '#content_gallica');
		});
		
		// On affiche le lien du menu mobile.
		//
		$('#lienCentralGallica').parent('li').show();
		
		// On copie la légende de la vignette
		//
		$gallicaContent.append($('#thumb_gallica .legend_gallica').clone());
		
		// On place le bloc contenu au bon endroit du dom.
		//
		$gallicaContent.appendTo('#colGauche');
		
		// Le bouton de fermeture
		//
		$closeButton.hide();
		$('#plus').before($closeButton);
		$closeButton.click(function($event) {
			$('#content_gallica').hide();
			$('#colGauche .s_Article').show();
			$('#precedent, #precedentInactif, #suivant, #suivantInactif').show();
			$('#precedent2').hide();
		});
	}
} // initGallica

// Affiche l'iframe de Gallica.
//
function showGallica()
{
	var $aArticleArea = $('#colGauche .s_Article');
	var $gallica = $('#content_gallica');
	var aWidth = $aArticleArea.width();
	var anUrl = $gallica.data('urlbase') +'/full/'+ Math.floor(aWidth) +',/0/native.jpg';
	
	$aArticleArea.hide();
	$gallica.show();
	
	// Bouton
	//
	$('#precedent, #precedentInactif, #suivant, #suivantInactif').hide();
	$('#precedent2').show();
	
	// On s'assure que l'image est visible
	//
	$(window).scrollTop(0);
	
	// On met le focus sur le bouton de fermeture de l'iframe.
	//
	$('#precedent2').focus();
} // showGallica

// Gére l'ouverture et la fermeture d'un zone toggle
// element : élément DOM réprésentant la zone toggle.
//
function toggleOuvrirFermer(element)
{
	$(element).nextAll('.sectionToggle').slideToggle(400);
	var urlImg = $('img', $(element)).attr('src');
	var $btn = $('button', $(element));

	$(element).children().children(".colGToggleGauche").toggleClass("activeRotGauche");
	$(element).children().children(".colGToggleDroite").toggleClass("activeRotDroite");

	// On change le sens de l'image de la fléche.
	//
	if (urlImg.search('sectionOuvert') != -1) {
		urlImg = urlImg.replace('sectionOuvert', 'sectionFerme');
		$btn.attr('aria-expanded', 'false');
		$btn.attr('title', 'Déplier');
	}
	else {
		urlImg = urlImg.replace('sectionFerme', 'sectionOuvert');
		$btn.attr('aria-expanded', 'true');
		$btn.attr('title', 'Replier');
	}
	$('img', $(element)).first().attr('src', urlImg);
} // toggleOuvrirFermer

$(document).ready(function() {
	// Fermeture de l'info bulle.
	//
	$('#contenantHyperlienInfoBulle .hyperlienInfoBullefermeture').click(function(){
		$('#contenantHyperlienInfoBulle').hide();
		if (typeof DiagDicoNavigation !== "undefined") {
			DiagDicoNavigation.cancelHighlight();
		}
		$('.s_Article, .s_Notice').off("click", clickWithDialog);
	});
	
	// Initialisation infos bulles customs.
	// 
	initToolTip();
	
	// Gestion du voisinage alphabétique
	//
	$("#voisinage .colDPrecedent").click(function() {
		var aFirstElt = $("#voisinage .listColDroite li a").first();
		requestNeighborhood("up", aFirstElt.attr('href'));
	});
	
	$("#voisinage .colDSuivant").click(function() {
		var aFirstElt = $("#voisinage .listColDroite li a").last();
		requestNeighborhood("down", aFirstElt.attr('href'));
	});
	
	// Gestion du Voir aussi du bloc de droite
	//
	$("#liensExterne a").click(function(){
		var linkContainer = $("#zoneliens .s_ZoneLiens_contenu:visible");
		if (linkContainer.length == 0) {
			$("#zoneliens .blocSectionLiens").click();
		}
	});
	
	// Scroll pour que l'entrée dans le voisinage soit visible et on la centre.
	//
	if ($(".listColDroiteVoisinage .motActif").length > 0) {
		var $activeWord = $(".listColDroiteVoisinage .motActif");
		var $wordContainer = $(".listColDroiteVoisinage");
		if (($activeWord.get(0).offsetTop <  $wordContainer.scrollTop()) || 
			($activeWord.get(0).offsetTop + $activeWord.height() > $wordContainer.scrollTop() + $wordContainer.get(0).clientHeight)) {
			$wordContainer.scrollTop($activeWord.get(0).offsetTop + $activeWord.outerHeight(true)/2 - $wordContainer.get(0).clientHeight/2);
		}
	}
	
	// Scroll pour que l'entrée dans l'historique soit visible.
	//
	if ($(".listColDroiteHistorique .motActif").length > 0) {
		if (($(".listColDroiteHistorique .motActif").get(0).offsetTop != 0) && 
				($(".listColDroiteHistorique .motActif").get(0).offsetTop + $(".listColDroiteHistorique .motActif").height() > $(".listColDroiteHistorique").scrollTop() + $(".listColDroiteHistorique").get(0).clientHeight)) {
			$('.listColDroiteHistorique').scrollTop($(".listColDroiteHistorique .motActif").get(0).offsetTop);
		}
	}
	
	$(document).keydown(function($event) {
		// Dès qu'on tape sur escape, on ferme tout.
		//
		if ($event.which == 27) {
			// Navigation sur les mots.
			//
			$('#contenantHyperlienInfoBulle .hyperlienInfoBullefermeture').click();
			
			// Info bulle.
			//
			$("#contenantInfoBulle").hide();
			
			//	Zone de recherche
			// 
			$("#btViderRecherche").click();
			
			// Fermeture du dialogue de partage
			//
			$("#btFermerPartage").click();
		}
	});
	
	// Gestion de l'apparition du bloc Orthographe à droite
	//
	if ($('.s_ZoneOrtho .s_ZoneOrtho_contenu').length > 0) {
		showOrthoArea();
	}
	
	// Le menu mobile
	//
	$('#lienCentralVoisinage').click(function($event){
		clickMenuLiensCentrale($event, $(this),'#voisinage');
	});
	$('#lienCentralHisto').click(function($event){
		clickMenuLiensCentrale($event, $(this), '#Historique');
	});
	$('#lienCentralZoneLiens').click(function($event){
		clickMenuLiensCentrale($event, $(this), '.s_Article');
	});
	$('#lienCentralHistoireMot').click(function($event){
		clickMenuLiensCentrale($event, $(this), '#HistoireMot');
	});
	
	// Bandeau
	//
	$('.blocBandeau, .blocSectionLiens').click(function($event) {
		$event.stopPropagation();
		toggleOuvrirFermer(this);
	});
	
	$('.colGToggle').mouseenter(function () {
		$(this).css("background-color", "#B7207A");
		$(this).css("border-left-color", "white");
	}).focusin(function () {
		$(this).css("background-color", "#B7207A");
		$(this).css("border-left-color", "white");
	});
	
	$('.colGToggle').mouseleave(function () {
		$(this).css("background-color", "transparent");
		$(this).css("border-left-color", "transparent");
	}).focusout(function () {
		$(this).css("background-color", "transparent");
		$(this).css("border-left-color", "transparent");
	});
	
    // Bandeau Col Gauche
	//
	$('.sectionToggleGauche').click(function($event) {
		$event.stopPropagation();
		$(this).next('.listColGauche').slideToggle(400);
		var urlImg = $('img', $(this)).attr('src');
		var $btn = $('button', $(this));
		
		$(this).toggleClass("activeRotGauche");
		$(this).toggleClass("activeRotDroite");
		
		// On change le sens de l'image de la fléche.
		//
		if (urlImg.search('sectionOuvert') != -1){
			urlImg = urlImg.replace('sectionOuvert', 'sectionFerme');
			$btn.attr('aria-expanded', 'false');
			$btn.attr('title', 'Déplier');
		}
		else {
			urlImg = urlImg.replace('sectionFerme', 'sectionOuvert');
			$btn.attr('aria-expanded', 'true');
			$btn.attr('title', 'Replier');
		}
		$('img', $(this)).first().attr('src', urlImg);
	});
	
	initGallica();
	
	$(window).resize(onWindowResize);
});

