/*jslint browser: true*/
/*global $, jQuery*/

// Génére un label correspondant à un résultat de recherche ou de navigation sur un mot.
// iResult : Objet résultat.
//
function makeResultLabel(iResult) {
	var aLabel = iResult.label + (iResult.nbhomograph == "" ? "" : " ["+ iResult.nbhomograph +"]");
	aLabel += (iResult.nature == "" ? '' : ', '+ iResult.nature);
	
	return aLabel;
}

//Traitement du résultat de la recherche.
//
//
function handleSearchSuccess(data, textStatus, jqXHR)
{
	var $resultList = $('#rechercheautocomplete-list');

	if ($.isArray(data.result) && ($resultList.data('term') != $('#recherche').val())) {
		var tabResults = data.result;
		
		$('>a,.noresult', $resultList).remove();
		
		if (tabResults.length > 0) {
			for(var i=0; i < tabResults.length; i++){
				$resultList.append('<a href="'+ tabResults[i].url +'" data-score="'+ tabResults[i].score +'">'+ makeResultLabel(tabResults[i]) +'</a>');
			}
		}	
		else if ((tabResults.length == 0) && (data.term != "")) {
			$resultList.append("<div class=\"noresult\">Il n'y a pas de résultat pour cette recherche.</div>");
		}
		
		// Sauvegarde le terme correspondant à la recherche.
		//
		$resultList.data('term', data.term);
		$resultList.show();
	}
} // handleSearchSuccess

// Gestion des icônes des outils
//
function initToobarButton()
{
	$("#partage").click(function() {
		// Si une autre "modale" (paramètre d'accessibilité) est ouverte, on la ferme.
		//
		if ($('#accessibiliteFen').hasClass('accessibiliteFenOuvert')) {
			$("#btFermerAccessibilite").click();
		}
		$("#divPartageIcones").addClass("divPartageIconesOuvert");
		$("#divPartageIcones").removeClass("divPartageIconesFermer");
	});
	$("#btFermerPartage").click(function() {
		$("#divPartageIcones").addClass("divPartageIconesFermer");
		$("#divPartageIcones").removeClass("divPartageIconesOuvert");
	});

	// Partage URL de la page
	//
	$(".boutonCopierPartage").click(function() {
		// Assignation de l'action de la fonction du copie vers le presse-papier au bouton
		//
		$('#urlcopie').select();
		document.execCommand('copy');
		
		$("#divPartageIcones").addClass("divPartageIconesFermer");
		$("#divPartageIcones").removeClass("divPartageIconesOuvert");
		if ($("#menuCentrale").hasClass("ouvert")) {
			setTimeout(function() {
				$("#plus").trigger("click");
			}, 300);
		}
	});

	// Gestion Outil Partage version appareils mobiles
	//
	$("#partageMobile").click(function() {
		$("#divPartageIcones").addClass("divPartageIconesOuvert");
		$("#divPartageIcones").removeClass("divPartageIconesFermer");
	});
	
	// Le bouton d'impression
	//
	$("#imprime").click(function(){
		window.print();
	});
} // initToobarButton

// Gère les modifications graphiques lors du survol de la zone infosRecherche
// iIsAcceuil : true si nous sommes sur la page d'accueil.
//
function overInfoRecherche(iIsAcceuil) {
	if (iIsAcceuil) {
		$('#recherche').addClass('rechercheFocusStyle');
		$('#explicationRecherche').addClass('afficher');
		$('#infoRechercheAccueil img').attr('src', $('#infoRechercheAccueil img').attr('src').replace('rechercheInfoAccueil@3x', 'rechercheInfo2Accueil@3x'));		
	}
	else {
		$('#recherche').addClass('rechercheFocusStyle');
		$('#explicationRecherche').addClass('afficher');
		$('#infoRecherche img').attr('src', $('#infoRecherche img').attr('src').replace('rechercheInfo@3x', 'rechercheInfo2@3x'));
	}
} // overInfoRecherche

// Gère les modifications graphiques lorsque l'on quitte la zone infosRecherche
// iIsAcceuil : true si nous sommes sur la page d'accueil.
//
function outInfoRecherche(iIsAcceuil) {
	if (iIsAcceuil) {
		$('#recherche').removeClass('rechercheFocusStyle');
		$('#explicationRecherche').removeClass('afficher');
		$('#infoRechercheAccueil img').attr('src', $('#infoRechercheAccueil img').attr('src').replace('rechercheInfo2Accueil@3x', 'rechercheInfoAccueil@3x'));		
	}
	else {
		$('#recherche').removeClass('rechercheFocusStyle');
		$('#explicationRecherche').removeClass('afficher');
		$('#infoRecherche img').attr('src', $('#infoRecherche img').attr('src').replace('rechercheInfo2@3x', 'rechercheInfo@3x'));		
	}
} // overInfoRecherche

/** Partie cookie **/

function createCookie(name, value, days)
{
	var expires = "";
	if (days) {
		var date = new Date();
		date.setTime(date.getTime()+(days*24*60*60*1000));
		expires = "; expires="+date.toGMTString();
	}
	document.cookie = name+"="+value+expires+"; path=/";
}

function eraseCookie(name)
{
	createCookie(name, "", -1);
}

function readCookie(name)
{
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	}
	return null;
}

// Permet de préciser si l'utilisateur accepte les cookies.
// Si l'utilisateur accepte, on génére une page vue pour les statistiques.
// response : true si on accepte.
//
function acceptCookies(response)
{
	var responseValue = "0";
	if (response == true) {
		responseValue = "1";
		// Génération d'une page vue.
		//
		if ((typeof ga == 'function') && (window['trackerId'] != 'undefined')) {
			ga('create', window['trackerId'], 'auto');
			ga('send', 'pageview');
		}
		if (typeof _paq != 'undefined') {
			_paq.push(['setConsentGiven']);
		} 
	}
	
	createCookie("acceptCookies", responseValue, 360);
}


function saveFontSize(fontSize)
{
	var acceptCookie = readCookie("acceptCookies");
	
	// Seulement si le client accepte les cookies. 
	//
	if ((acceptCookie != null) && (acceptCookie == "1")) {
		createCookie("fontSize", fontSize, 30);
	}
} // saveFontSize


// Initialise les fonctionnalités de changement de taille de la font.
// Ajoute les handlers sur les boutons etc.
//
function initFontSize(currentSize)
{
	var index = 2;
	var pourcentageStep = [ "80%", "90%", "105%", "115%", "130%", "150%", "200%" ]; // Les différentes tailles possibles sur desktop.
	var pourcentageStepMobile = [ "90%", "105%", "130%" ]; // Les différentes tailles possibles sur mobile.
	
	// On cherche la taille courante de la page Web dans notre tableau
	//
	for (var i = 0; i < pourcentageStep.length; i++) {
		if (pourcentageStep[i] == currentSize) {
			index = i;
			break;
		}
	}

	var previousIndex = function(index, length) {
		if (index <= 0) {
			return length - 1;
		} else {
			return index - 1;
		}
	};

	var nextIndex = function(index, length) {
		return ((index + 1) % length)
	};
	
	var changeFontSize = function(fontSize) {
		$(".s_Article, .s_Notice, .a_Annexe").css("font-size", fontSize);
		$(".s_Entree_haut").css("font-size", "130%");
		$("#colDroite").css("font-size", fontSize);
		$("#colGaucheRechAv").css("font-size", fontSize);
		
		saveFontSize(fontSize);
	};

	// Augmentation taille de la typo
	//
	$("#txPlus, #txPlusMenuCentrale").on("click", function() {
		if (pourcentageStep[index] != pourcentageStep[pourcentageStep.length - 1]) {
			index = nextIndex(index, pourcentageStep.length);
			changeFontSize(pourcentageStep[index]);
		}
	});

	// Réduction taille de la typo
	//
	$("#txMoins, #txMoinsMenuCentrale").on("click", function() {
		if (pourcentageStep[index] != pourcentageStep[0]) {
			index = previousIndex(index, pourcentageStep.length);
			changeFontSize(pourcentageStep[index]);
		}
	});
	
    // Appareils mobiles
	//
	var changeFontSizeMobile = function(fontSize) {
		$(".s_Article, .s_Notice, .a_Annexe").css("font-size", fontSize);
		if ($("#menuCentrale").hasClass("ouvert")) {
			setTimeout(function() {
				$("#plus").trigger("click");
			}, 300);
		}
		saveFontSize(fontSize);
	};
	
    // Bouton taille petite
	//
	$('#iconesOutilsMobile #typoPetit').on('click',function(){
		changeFontSizeMobile(pourcentageStepMobile[0]);
	});
    
    // Bouton taille moyenne
	//
    $('#iconesOutilsMobile #typoMoyen').on('click',function(){
		changeFontSizeMobile(pourcentageStepMobile[1]);
	});
    
    // Bouton taille grande
    //
    $('#iconesOutilsMobile #typoGrand').on('click',function(){
		changeFontSizeMobile(pourcentageStepMobile[2]);
	});
    
    // Changement de taille partie accessibilité
    //
	$('#taillesTypoAccessibilite #typoPetit').on('click',function(){
		index = 2;
		changeFontSize(pourcentageStep[index]);
	});
    
    $('#taillesTypoAccessibilite #typoMoyen').on('click',function(){
    	index = 4;
    	changeFontSize(pourcentageStep[index]);
	});
    
    $('#taillesTypoAccessibilite #typoGrand').on('click',function(){
    	index = 6;
    	changeFontSize(pourcentageStep[index]);
	});
} // InitFontSize

// Event custom quand une entrée du dictionnaire est chargée. Utile si l'on charge seulement
// une partie de la page.
//
$(document).on('entry.ready', function () {
	// MENU CENTRALE pour appareils mobiles
	// Ouverture / Fermeture du menu principal
	//
	$('#plus').on('click', function() {
		$('#vertical').toggleClass('ouvert');

		// Animation Menu
		$('#menuCentrale').toggleClass('ouvert');

		//Fixer en place le menu centrale et contenu stop le scroll
		$('#contenu').toggleClass('fermer');
		$("footer").toggleClass("cellulaire");
	});
	
	var cssAccessClass = "navy";
	$("."+ cssAccessClass).each(function() {
		if ($(this).children().length > 0) {
			$(this).removeAttr("tabindex");
			$(this).removeClass(cssAccessClass);
		}
		else {
			$(this).keypress(function(event) {
				var keycode = (event.keyCode ? event.keyCode : event.which);
				if (keycode == '13') {
					var newRange = document.createRange();
					newRange.setStart(this.firstChild, 0);
					newRange.setEnd(this.firstChild, 0);

					var selection = window.getSelection();
					selection.removeAllRanges();
					selection.addRange(newRange);

					$(this).click();
				}
			});
		}
	});

	$("a ." + cssAccessClass).each(function() {
		$(this).removeAttr("tabindex");
		$(this).removeClass(cssAccessClass);
	});
});

jQuery(document).ready(function ($) {
    // Activer la fenêtre info Recherche
    //
    /*
	$("#contenantRecherche #recherche").on('focus', function () {
		$('#infoRecherche img').attr('src', $('#infoRecherche img').attr('src').replace('rechercheInfo@3x', 'rechercheInfo2@3x'));
	}).focusout(function () {
		$('#infoRecherche img').attr('src', $('#infoRecherche img').attr('src').replace('rechercheInfo2@3x', 'rechercheInfo@3x'));
	});
	
	$("#contenantRechercheAccueil #recherche").on('focus', function () {
		$('#infoRechercheAccueil img').attr('src', $('#infoRechercheAccueil img').attr('src').replace('rechercheInfoAccueil@3x', 'rechercheInfo2Accueil@3x'));
	}).focusout(function () {
		$('#infoRechercheAccueil img').attr('src', $('#infoRechercheAccueil img').attr('src').replace('rechercheInfo2Accueil@3x', 'rechercheInfoAccueil@3x'));
	});
	*/
	// Survol info Recherche - Accueil
	//
	$("#infoRechercheAccueil").mouseover(function () {
		overInfoRecherche(true);
	}).on('focus', function () {
		overInfoRecherche(true);
	});
	$("#infoRechercheAccueil").mouseout(function () {
		outInfoRecherche(true);
	}).on('focusout', function () {
		outInfoRecherche(true);
	});
	
	// FOCUS > Vider champ recherche
	//
	$('#btViderRecherche, #btViderRechercheAccueil').on('focus', function () {
		$('img', $(this)).attr('src', $('img', $(this)).attr('src').replace('rechercheEffacer@3x', 'rechercheEffacerBlanc@3x'));
	});
	$("#btViderRecherche, #btViderRechercheAccueil").on('focusout', function () {
		$('img', $(this)).attr('src', $('img', $(this)).attr('src').replace('rechercheEffacerBlanc@3x', 'rechercheEffacer@3x'));
	});
	
	// FOCUS > Recherche avancée
    //
    $('#btLoupeAv, #imgLoupeAvAccueil').on('focus', function () {
        $('img', $(this)).attr('src', $('img', $(this)).attr('src').replace('loupeAv.png', 'loupeAvActif.png'));
    });
    $("#btLoupeAv, #imgLoupeAvAccueil").on('focusout', function () {
        $('img', $(this)).attr('src', $('img', $(this)).attr('src').replace('loupeAvActif.png', 'loupeAv.png'));
    });

    // FOCUS > Fermeture X Recherche avancée
    //
    $('#fermeture, #fermetureAccueil').on('focus', function () {
        $('img', $(this)).attr('src', $('img', $(this)).attr('src').replace('xFermeture.png', 'xFermetureActif.png'));
    });
    $("#fermeture, #fermetureAccueil").on('focusout', function () {
        $('img', $(this)).attr('src', $('img', $(this)).attr('src').replace('xFermetureActif.png', 'xFermeture.png'));
    });
    
    // FOCUS > Informations
    //
    $('#informations, #informationsAccueil').on('focus', function () {
        $('img', $(this)).attr('src', $('img', $(this)).attr('src').replace('informations.png', 'informationsActif.png'));
    });
    $("#informations, #informationsAccueil").on('focusout', function () {
        $('img', $(this)).attr('src', $('img', $(this)).attr('src').replace('informationsActif.png', 'informations.png'));
    });
	
	// FOCUS > Logo Académie
	//
	$("#logoAF a").on('focus', function() {
		$("#logoAF a img").addClass("logoHighlight");
	});
	$("#logoAF a").on('focusout', function() {
		$("#logoAF a img").removeClass("logoHighlight");
	});
	
	// FOCUS > Boutons retour/suivant
	//
	$("#precedent a").on('focus', function () {
		$("#precedent").addClass("precedentFocus");
	});
	$("#precedent a").on('focusout', function () {
		$("#precedent").removeClass("precedentFocus");
	});

	$("#suivant a").on('focus', function () {
		$("#suivant").addClass("suivantFocus");
	});
	$("#suivant a").on('focusout', function () {
		$("#suivant").removeClass("suivantFocus");
	});
    
	// Validation du formulaire de recherche.
	//
	$("#frm_search").submit(function($event) {
		$event.preventDefault();
		
		if ($('#recherche').val() == "") {
			// On vide la liste mais on laisse apparaître les boutons de options de recherche.
			//
			$('#rechercheautocomplete-list>a, #rechercheautocomplete-list .noresult').remove();
			$('#rechercheautocomplete-list').show();
		}
		else if ($('#recherche').val() != $('#rechercheautocomplete-list').data('term')) {
			// On fait la recherche.
			//
			$.ajax({
				type: "POST",
				url: $("#frm_search").attr('action'),
				dataType: "json",
				data: $.param($("#frm_search input"))
			}).done(handleSearchSuccess);
		}
		else if ($('#rechercheautocomplete-list > a').length > 0) {
			var url = $('#rechercheautocomplete-list > a.selected').attr('href');
			
			if (! url) {
				url = $('#rechercheautocomplete-list > a').first().attr('href');
			}
			
			if (url) {
				document.location.href = url;
			}
		}
	});
	
	$('#recherche').keyup(function ($event) {
		if ((($('#recherche').val().length > 2) || ($('#rechercheautocomplete-list:visible').length > 0)) && ($('#recherche').val() != $('#rechercheautocomplete-list').data('term'))) {
			$("#frm_search").submit();
		}
		else if ($('#rechercheautocomplete-list:visible').length > 0) {
			// Flèches
			// 
			if (($event.which == 40) || ($event.which == 38)) {
				var $itemSelected = $('#rechercheautocomplete-list > a.selected');
				
				// Flèche du bas.
				//
				if ($event.which == 40) {
					if ($itemSelected.length > 0) {
						$itemSelected.next().addClass('selected');
						$itemSelected.removeClass('selected');
					}
					else {
						$('#rechercheautocomplete-list > a').first().addClass('selected');
					}
				}
				// Flèche du haut
				//
				else if ($event.which == 38) {				
					if ($itemSelected.length > 0) {
						$itemSelected.prev().addClass('selected');
						$itemSelected.removeClass('selected');
					}
					else {
						$('#rechercheautocomplete-list > a').last().addClass('selected');
					}
				}
				
				// On scrolle si besoin.
				//
				$itemSelected = $('#rechercheautocomplete-list > a.selected');
				if (($itemSelected.get(0).offsetTop <  $('#rechercheautocomplete-list').scrollTop()) ||
						($itemSelected.get(0).offsetTop + $itemSelected.height() > $('#rechercheautocomplete-list').scrollTop() + $('#rechercheautocomplete-list').get(0).clientHeight)) {
					$('#rechercheautocomplete-list').scrollTop($itemSelected.get(0).offsetTop);
				}
			}
		}
	});
	
	$("#btViderRecherche, #btViderRechercheAccueil").click(function() {
		$("#recherche").val("");
		$('#rechercheautocomplete-list').hide();
		$("#recherche").focus();
	});
	
	// Clic permettant de choisir le type de recherche.
	//
	$('#motCommencant, #motsProches').click(function($event) {
		var $aTarget = $($event.currentTarget);
		$event.preventDefault();
		
		if (! $aTarget.is(".btRechercheActive")) {
			var aTitleCompl = ' (filtre actif)';
			$('#search_options').val($aTarget.data('value'));
			
			$('#rechercheautocomplete-list .btRechercheActive').removeClass("btRechercheActive").addClass("btRechercheDisponible");
			$aTarget.addClass("btRechercheActive").removeClass("btRechercheDisponible");
			
			// On met filtre actif sur le filtre nouvellement sélectionné.
			//
			$('#rechercheautocomplete-list .btRechercheActive a').attr('title', $('#rechercheautocomplete-list .btRechercheActive a').attr('title') + aTitleCompl);
			$('#rechercheautocomplete-list .btRechercheDisponible a').attr('title' , $('#rechercheautocomplete-list .btRechercheActive a').attr('title').replace(aTitleCompl, ''));

			
			// Modification du title.
			//
			
			$('#rechercheautocomplete-list').data('term', ''); // Pour forcer une nouvelle recherche.
			$("#recherche").focus();
			$("#frm_search").submit();
		}
	});
	
	$("#recherche").focus();

    // MENU PRINCIPALE


    // Ouverture / Fermeture du menu principal
    $('.menuAnim').on('click', function(){
        $('.menuIcone').toggleClass('is-clicked');
        
        if ($(this).attr('aria-expanded') == 'false') {
        	$(this).attr('aria-expanded', 'true');
        	$(this).attr('title', 'Fermer le menu');
        }
        else {
        	$(this).attr('aria-expanded', 'false');
        	$(this).attr('title', 'Ouvrir le menu');
        }

        // Animation Menu
        //
        $('.transform').toggleClass('transform-active');
        $('.menuTopCell').toggleClass('ouvertureMenuCell');
    });
    
    // Les Cookies
    //
    if ($('#blocCookies').length > 0) {
    	$("#blocCookies .boutonCookies").first().focus();
    	
    	// Zone de demande d'autorisation des cookies.
    	//
    	$('#blocCookies .boutonCookies').click(function($event) {
    		var $aTarget = $($event.currentTarget);
    		var aResponse = $aTarget.data('response');
    		$event.preventDefault();
    		
    		if (aResponse == "1") {
    			acceptCookies(true);
    		}
    		else if (aResponse == "0") {
    			acceptCookies(false);
    		}
    		$('#blocCookies').fadeOut();
    	});
    }
    
	// Gestion infoBulle - txMoins
    //
	$("#txMoins").mouseover(function () {
		$(this).addClass("picto-itemFocus");
	}).mouseout(function () {
		$(this).removeClass("picto-itemFocus");
	}).click(function () {
		$(this).removeClass("picto-itemFocus");
	}).focus(function () {
		$(this).addClass("picto-itemFocus");
	}).focusout(function () {
		$(this).removeClass("picto-itemFocus");
	});
	
	// Gestion infoBulle - txPlus
	//
	$("#txPlus").mouseover(function () {
		$(this).addClass("picto-itemFocus");
	}).mouseout(function () {
		$(this).removeClass("picto-itemFocus");
	}).click(function () {
		$(this).removeClass("picto-itemFocus");
	}).focus(function () {
		$(this).addClass("picto-itemFocus");
	}).focusout(function () {
		$(this).removeClass("picto-itemFocus");
	});
	
	// Gestion infoBulle - imprime
	//
	$("#imprime").mouseover(function () {
		$(this).addClass("picto-itemFocus");
	}).mouseout(function () {
		$(this).removeClass("picto-itemFocus");
	}).click(function () {
		$(this).removeClass("picto-itemFocus");
	}).focus(function () {
		$(this).addClass("picto-itemFocus");
	}).focusout(function () {
		$(this).removeClass("picto-itemFocus");
	});
	
	// Gestion infoBulle - partage
	//
	$("#partage").mouseover(function () {
		$(this).addClass("picto-itemFocus");
	}).mouseout(function () {
		$(this).removeClass("picto-itemFocus");
	}).click(function () {
		$(this).removeClass("picto-itemFocus");
	}).focus(function () {
		$(this).addClass("picto-itemFocus");
	}).focusout(function () {
		$(this).removeClass("picto-itemFocus");
	});

    // BOUTONS > TAILLE TYPO
    //
    // Provisoire ça devrait être côté serveur.
    //
    var aFontSize = readCookie("fontSize");
    if (aFontSize) {
    	$(".s_Article, .s_Notice, .colGaucheFond .a_Annexe").css("font-size", aFontSize);
    }
    else {
    	aFontSize = "";
    }
    initFontSize(aFontSize);
//    initFontSize($('.s_Article').css('font-size'));
    
    initToobarButton();
	
	// Préchargement d'image.
	//
	var tabPreloadImg = ['images/logoAFHighlight.png', 'images/logoAFMobileHighlight.png', 'images/rechercheInfo2Accueil@3x.png', 'images/rechercheInfo2@3x.png', 'images/loupeFocus.png', 'images/logoAFMobileHighlight.png', 'images/logoAFHighlight.png', 'images/infoAccessibiliteSurvol.png', 'images/accessibilite_survol.png', 'images/xFermerInfoBulleSurvol.png'];
	var basePath = $('#preload').data('basepath');
	
	if (typeof basePath != 'undefined') {
		for (var i = 0; i < tabPreloadImg.length; i++) {
			$("<img />").attr("src", basePath + tabPreloadImg[i]);
		}
	}
	
	// RECHERCHE AVANCÉE
	//
	
	function fermerNiveaux() {
		// Fermer toutes les fenêtres - rech Domaine
	
		// niveau un
		//
		if ($('#rechDomaines ul li.niveauxUN > ul').hasClass('ouvert')) {
			$('#rechDomaines ul li.niveauxUN > ul').removeClass('ouvert');
			$('#rechDomaines ul li.niveauxUN').removeClass('fondBleu');
			$('#rechDomaines ul li.niveauxUN > h4').removeClass('sectionActive');
			$('#rechDomaines ul li.niveauxUN > h4').removeClass('active');
		}
		if ($('#rechDomaines ul li.niveauxUN > div').hasClass('rotation')) {
			$('#rechDomaines ul li.niveauxUN > div').removeClass('rotation');
		}
	
		// niveau deux
		//
		if ($('#rechDomaines ul li.niveauxDEUX > h4').hasClass('active')) {
			$('#rechDomaines ul li.niveauxDEUX > h4').removeClass('active');
			$('#rechDomaines ul li.niveauxDEUX').removeClass('fondBleu');
			$('#rechDomaines ul li.niveauxDEUX > h4').removeClass('sectionActive');
			$('#rechDomaines ul li.niveauxDEUX > h4').removeClass('active');
		}
		if ($('#rechDomaines ul li.niveauxDEUX > ul').hasClass('ouvert')) {
			$('#rechDomaines ul li.niveauxDEUX > ul').removeClass('ouvert');
		}
		if ($('#rechDomaines ul li.niveauxDEUX > div').hasClass('rotation')) {
			$('#rechDomaines ul li.niveauxDEUX > div').removeClass('rotation');
		}
		if ($('#rechDomaines ul li.niveauxDEUX').hasClass('active')) {
			$('#rechDomaines ul li.niveauxDEUX').removeClass('active');
		}
	

	
		// Fermer toutes les fenêtres - rech Catégories Grammaticales
	
		// niveau un
		//
		if ($('#rechCatGram ul li.niveauxUN > ul').hasClass('ouvert')) {
			$('#rechCatGram ul li.niveauxUN > ul').removeClass('ouvert');
			$('#rechCatGram ul li.niveauxUN').removeClass('fondBleu');
			$('#rechCatGram ul li.niveauxUN > h4').removeClass('sectionActive');
			$('#rechCatGram ul li.niveauxUN > h4').removeClass('active');
		}
		if ($('#rechCatGram ul li.niveauxUN > div').hasClass('rotation')) {
			$('#rechCatGram ul li.niveauxUN > div').removeClass('rotation');
		}
	
		// niveau deux
		//
		if ($('#rechCatGram ul li.niveauxDEUX > h4').hasClass('active')) {
			$('#rechCatGram ul li.niveauxDEUX > h4').removeClass('active');
			$('#rechCatGram ul li.niveauxDEUX').removeClass('fondBleu');
			$('#rechCatGram ul li.niveauxDEUX > h4').removeClass('sectionActive');
			$('#rechCatGram ul li.niveauxDEUX > h4').removeClass('active');
		}
		if ($('#rechCatGram ul li.niveauxDEUX > ul').hasClass('ouvert')) {
			$('#rechCatGram ul li.niveauxDEUX > ul').removeClass('ouvert');
		}
		if ($('#rechCatGram ul li.niveauxDEUX > div').hasClass('rotation')) {
			$('#rechCatGram ul li.niveauxDEUX > div').removeClass('rotation');
		}
		if ($('#rechCatGram ul li.niveauxDEUX').hasClass('active')) {
			$('#rechCatGram ul li.niveauxDEUX').removeClass('active');
		}
	

	
		// Fermer toutes les fenêtres - rech Étymologie par langue
	
		// niveau un
		//
		if ($('#rechEtymoParLangue ul li.niveauxUN > ul').hasClass('ouvert')) {
			$('#rechEtymoParLangue ul li.niveauxUN > ul').removeClass('ouvert');
			$('#rechEtymoParLangue ul li.niveauxUN').removeClass('fondBleu');
			$('#rechEtymoParLangue ul li.niveauxUN > h4').removeClass('sectionActive');
			$('#rechEtymoParLangue ul li.niveauxUN > h4').removeClass('active');
		}
		if ($('#rechEtymoParLangue ul li.niveauxUN > div').hasClass('rotation')) {
			$('#rechEtymoParLangue ul li.niveauxUN > div').removeClass('rotation');
		}
	
		// niveau deux
		//
		if ($('#rechEtymoParLangue ul li.niveauxDEUX > h4').hasClass('active')) {
			$('#rechEtymoParLangue ul li.niveauxDEUX > h4').removeClass('active');
			$('#rechEtymoParLangue ul li.niveauxDEUX').removeClass('fondBleu');
			$('#rechEtymoParLangue ul li.niveauxDEUX > h4').removeClass('sectionActive');
			$('#rechEtymoParLangue ul li.niveauxDEUX > h4').removeClass('active');
		}
		if ($('#rechEtymoParLangue ul li.niveauxDEUX > ul').hasClass('ouvert')) {
			$('#rechEtymoParLangue ul li.niveauxDEUX > ul').removeClass('ouvert');
		}
		if ($('#rechEtymoParLangue ul li.niveauxDEUX > div').hasClass('rotation')) {
			$('#rechEtymoParLangue ul li.niveauxDEUX > div').removeClass('rotation');
		}
		if ($('#rechEtymoParLangue ul li.niveauxDEUX').hasClass('active')) {
			$('#rechEtymoParLangue ul li.niveauxDEUX').removeClass('active');
		}
	


		var rDomaines = document.querySelector('#rechDomaines');
		rDomaines.scrollTop = 0;
		var rCatGram = document.querySelector('#rechCatGram');
		rCatGram.scrollTop = 0;
		var rLangue = document.querySelector('#rechEtymoParLangue');
		rLangue.scrollTop = 0;
	} // fermerNiveaux
	
	
	// Fermeture menu rechDomaines
	//
	function fermerMenuRechDomaine() {
		$('#rechDomaines').removeClass('options');
		$('#listeDomaine span').removeClass('rotationNivUn');
		$('#listeDomaine').removeClass('is-active');
        // Replace le menu au premier énoncé (haut du ul)
        var rDomaines = document.querySelector('#rechDomaines');
		rDomaines.scrollTop = 0;
	}
	
	// Fermeture menu rechCatGram
	//
	function fermerMenuRechCatGram() {
		$('#rechCatGram').removeClass('options');
		$('#listeCatGram span').removeClass('rotationNivUn');
		$('#listeCatGram').removeClass('is-active');
        // Replace le menu au premier énoncé (haut du ul)
        var rCatGram = document.querySelector('#rechCatGram');
		rCatGram.scrollTop = 0;
	}
	
	// Fermeture menu rechDomaines
	//
	function fermerMenuRechListeLangue() {
		$('#rechEtymoParLangue').removeClass('options');
		$('#listeLangue span').removeClass('rotationNivUn');
		$('#listeLangue').removeClass('is-active');
        // Replace le menu au premier énoncé (haut du ul)
        var rLangue = document.querySelector('#rechEtymoParLangue');
		rLangue.scrollTop = 0;
	}
	
	// Fermer les liste déroulantes hiérarchiques lorsque l’on clique à l’extérieur
	//
	window.addEventListener('click', function(e) {
		if (document.getElementById('rechDomaines').contains(e.target)) { } else {
			if ($('#rechDomaines').hasClass('options')) {
				fermerMenuRechDomaine();
				fermerNiveaux();
				var rDomaines = document.querySelector('#rechDomaines');
				rDomaines.scrollTop = 0;
			}
		}
	
		if (document.getElementById('rechCatGram').contains(e.target)) { } else {
			if ($('#rechCatGram').hasClass('options')) {
				fermerMenuRechCatGram();
				fermerNiveaux();
				var rCatGram = document.querySelector('#rechCatGram');
				rCatGram.scrollTop = 0;
			}
		}
	
		if (document.getElementById('rechEtymoParLangue').contains(e.target)) { } else {
			if ($('#rechEtymoParLangue').hasClass('options')) {
				fermerMenuRechListeLangue();
				fermerNiveaux();
				var rLangue = document.querySelector('#rechEtymoParLangue');
				rLangue.scrollTop = 0;
			}
		}
	});
	
	
	// Fermeture fenêtre recherche avancée
	//
	$('#rechercheAvance #frm_search_Av #fermeture').on('click', function() {
		$('#rechercheAvance').removeClass('active');
		$('#btLoupeAv').removeClass('active');
	
		fermerNiveaux();
	});
	
	// Faire apparaître/disparaître la fenêtre recherche avancée
	//
	$('#rechercheFixed #contenantRecherche #btLoupeAv').on('click', function() {
		$('#btLoupeAv').toggleClass('active');
		$('#rechercheAvance').toggleClass('active');
		if ($('#rechercheAvance').hasClass('active')) {
			fermerNiveaux();
			// Focus dans le formulaire de recherche
			//
			$('#rechIntegral').focus();
		}
		
		if ($('#sansResultat').hasClass('divSansResultatsIconesOuvert')) {
			$('#sansResultat').removeClass('divSansResultatsIconesOuvert');
			$('#sansResultat').addClass('divSansResultatsIconesFermer');
		}
	});
	
	// Vérifie que le minimun des champs requis pour la recherche avancée soit remplis.
	//
	function checkFrmAdvSearch()
	{
		if (($('#filtreEditions').data('forced')) || ($('#rechIntegral').val() != '') || ($('#rechEntrees').val() != '') 
			|| ($('input[name="chk_domains"]:checked').length > 0) || ($('input[name="chk_gramcat"]:checked').length > 0)) {
			// Au moins un critére est remplie, la recherche est valide
			//
			$('#btRechercher').prop('disabled', false).addClass('criteresActifs');
			return true;
		}
		// Recherche sans critère majeur, on désactive le bouton de recherche.
		//
		$('#btRechercher').prop('disabled', true).removeClass('criteresActifs');
		return false;
	} // checkFrmAdvSearch
	
	// La sélection d'une valeur dans les listes déroulantes domaines, datation et langues force (désactive aussi apparu/disparu)
	// la sélection de l'édition 9.
	//
	$('#rechDans, #rechEtymologieDatation, input[name="chk_langs"]').on('change', function() {
		if (($('#rechDans').val() != '') || ($('#rechEtymologieDatation').val() != '') || ($('input[name="chk_langs"]:checked').length > 0)) {
			// On choisit la 9e édition et on la grise.
			//
			$('#filtreEditions').val('9').prop('disabled', true).data('forced', true);
			$('#chk_appear, #chk_disappear').prop('disabled', true);
		}
		else {
			$('#filtreEditions').prop('disabled', false).removeData('forced');
			$('#chk_appear, #chk_disappear').prop('disabled', false);
		}
		$('#filtreEditions').trigger('change');
	});
	
	// Réalise les actions nécessaire au changement d'édition et active ou non le bouton de recherche.
	//
	$('#filtreEditions').on('change', function () {
		switch($(this).val()) {
			case '0':
				$('#chk_appear, #chk_disappear').prop('disabled', true);
				break;
				
			case '1':
				$('#chk_appear').prop('disabled', true);
				$('#chk_disappear').prop('disabled', false);
				break;
				
			case '9':
				if (! $(this).data('forced')) {
					$('#chk_appear').prop('disabled', false);
					$('#chk_disappear').prop('disabled', true);
				}
				break;
				
			default:
				$('#chk_appear').prop('disabled', false);
				$('#chk_disappear').prop('disabled', false);
		}
		
		checkFrmAdvSearch();
	});
	
	// Handler sur les modifications des champs de la recherche avancée pour activer/désactiver
	// le bouton de recherche.
	//
	$('#rechIntegral, #rechEntrees').on('keyup', function() {
		checkFrmAdvSearch();	
	});
	$('#rechIntegral, #rechEntrees').on('change', function() {
		checkFrmAdvSearch();	
	});
	
	$('#frm_search_Av').on('submit', function($event) {
		if (! checkFrmAdvSearch()) {
			$event.preventDefault();
			$event.stopPropagation();
		}
	});
	
	// Bouton réinitialiser
	//
	$('#btReinitialiser').on('click', function($event) {
		$event.preventDefault();
		$('#rechIntegral, #rechEntrees').val('');
		$('#frm_search_Av input[type="checkbox"]').prop('checked', false);
		$('#frm_search_Av select option:selected').prop('selected', false);
		
		// Lemmatisation
		//
		$('#chk_allform').prop('checked', true);
		
		// Edition
		//
		$('#filtreEditions').val('9');
		
		// Déclenche la vérification de certaines contraintes.
		//
		$('#rechDans').trigger('change');
		
		// Focus dans le premier champs de recherche
		//
		$('#rechIntegral').focus();
	});
	
	// Déclencle la vérification de certains critères pour initialiser le formulaire.
	//
	$('#rechDans').trigger('change');
	
	
	// Rotation du chevron - Rech. par domaines
    $('#rechDomaines').on('click', function() {
        if ($(this).hasClass('options')) {
            $(this).children('button').children('span').addClass('rotationNivUn');
        } else {
            $(this).children('button').children('span').removeClass('rotationNivUn');
        }
    });
    
    // Rotation du chevron - Rech. par catégories grammaticales
    $('#rechCatGram').on('click', function() {
        if ($(this).hasClass('options')) {
            $(this).children('button').children('span').addClass('rotationNivUn');
        } else {
            $(this).children('button').children('span').removeClass('rotationNivUn');
        }
    });
    
    // Rotation du chevron - Rech. par langue
    $('#rechEtymoParLangue').on('click', function() {
        if ($(this).hasClass('options')) {
            $(this).children('button').children('span').addClass('rotationNivUn');
        } else {
            $(this).children('button').children('span').removeClass('rotationNivUn');
        }
    });
    
    //// Menu caché - Recherche avancée - RECHERCHE PAR DOMAINES
	//
	// Menu niveaux un
	//
	$('#rechDomaines ul li.niveauxUN').on('click', function() {
		if ($(this).children('h4').hasClass('sectionActive')) {
            $(this).children('h4').removeClass('sectionActive');
            $(this).children('button').removeClass('is-active');
            $(this).children('ul').removeClass('is-active');
            $(this).removeClass('fondBleu');
        } else {
            $(this).children('h4').addClass('sectionActive');
            $(this).children('button').addClass('is-active');
            $(this).children('ul').addClass('is-active');
            $(this).addClass('fondBleu');
        }
	});

	// Menu niveau Deux
	//
    $('#rechDomaines ul li.niveauxDEUX').on('click', function() {
		if ($(this).children('h4').hasClass('sectionActive')) {
            $(this).children('h4').removeClass('sectionActive');
            $(this).children('button').removeClass('is-active');
            $(this).children('ul').removeClass('is-active');
            $(this).removeClass('fondBleu');
        } else {
            $(this).children('h4').addClass('sectionActive');
            $(this).children('button').addClass('is-active');
            $(this).children('ul').addClass('is-active');
            $(this).addClass('fondBleu');
        }
	});
	
	//// Menu caché - Recherche avancée - PAR CATÉGORIES GRAMMATICALE
	//
	// Menu niveaux un
	//
	$('#rechCatGram ul li.niveauxUN').on('click', function() {
		if ($(this).children('h4').hasClass('sectionActive')) {
            $(this).children('h4').removeClass('sectionActive');
            $(this).children('button').removeClass('is-active');
            $(this).children('ul').removeClass('is-active');
            $(this).removeClass('fondBleu');
        } else {
            $(this).children('h4').addClass('sectionActive');
            $(this).children('button').addClass('is-active');
            $(this).children('ul').addClass('is-active');
            $(this).addClass('fondBleu');
        }
	});

	// Menu niveau Deux
	//
    $('#rechCatGram ul li.niveauxDEUX').on('click', function() {
		if ($(this).children('h4').hasClass('sectionActive')) {
            $(this).children('h4').removeClass('sectionActive');
            $(this).children('button').removeClass('is-active');
            $(this).children('ul').removeClass('is-active');
            $(this).removeClass('fondBleu');
        } else {
            $(this).children('h4').addClass('sectionActive');
            $(this).children('button').addClass('is-active');
            $(this).children('ul').addClass('is-active');
            $(this).addClass('fondBleu');
        }
	});
	
	// Menu caché - Recherche avancée - ÉTYMOLOGIE - PAR LANGUE
	//
	// Menu niveaux un
	//
	$('#rechEtymoParLangue ul li.niveauxUN').on('click', function() {
		if ($(this).children('h4').hasClass('sectionActive')) {
            $(this).children('h4').removeClass('sectionActive');
            $(this).children('button').removeClass('is-active');
            $(this).children('ul').removeClass('is-active');
            $(this).removeClass('fondBleu');
        } else {
            $(this).children('h4').addClass('sectionActive');
            $(this).children('button').addClass('is-active');
            $(this).children('ul').addClass('is-active');
            $(this).addClass('fondBleu');
        }
	});

	// Menu niveau Deux
	//
    $('#rechEtymoParLangue ul li.niveauxDEUX').on('click', function() {
		if ($(this).children('h4').hasClass('sectionActive')) {
            $(this).children('h4').removeClass('sectionActive');
            $(this).children('button').removeClass('is-active');
            $(this).children('ul').removeClass('is-active');
            $(this).removeClass('fondBleu');
        } else {
            $(this).children('h4').addClass('sectionActive');
            $(this).children('button').addClass('is-active');
            $(this).children('ul').addClass('is-active');
            $(this).addClass('fondBleu');
        }
	});
	
	// Checkbox famille
	//
	$('.menuPerso input[type="checkbox"], .menuPersoEtroit input[type="checkbox"]').change(function(e) {
		var checked = $(this).prop("checked"),
			container = $(this).parent(),
			siblings = container.siblings();
	
		container.find('input[type="checkbox"]').prop({
			indeterminate: false,
			checked: checked
		});
	
		checkFrmAdvSearch();

		function checkSiblings(el) {
			var parent = el.parent().parent(),
				all = true;
	
			el.siblings().each(function() {
				//let returnValue = all = ($(this).children('input[type="checkbox"]').prop("checked") === checked);
				var returnValue = all = ($(this).children('input[type="checkbox"]').prop("checked") === checked);
				return returnValue;
			});
	
			if (all && checked) {
				parent.children('input[type="checkbox"]').prop({
					indeterminate: false,
					checked: checked
				});
	
				checkSiblings(parent);
			}
			else if (all && !checked) {
				parent.children('input[type="checkbox"]').prop("checked", checked);
				parent.children('input[type="checkbox"]').prop("indeterminate", (parent.find('input[type="checkbox"]:checked').length > 0));
				checkSiblings(parent);
			}
			else {
				el.parents("li").children('input[type="checkbox"]').prop({
					indeterminate: true,
					checked: false
				});
			}
		} // checkSiblings
	
		checkSiblings(container);
	});
});
