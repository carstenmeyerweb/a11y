/**
 * Regoupe un maximum les éléments liés à l'accessibilité.
 */

// Objet global stockant les préférences d'accessibilité.
//
var accessibilitySettings = {
	wordNavigationLink: false,
	openDyslexic: false
};

// Charge les paramètres depuis un cookie et modifie la variable globale accessibilitySettings en conséquence.
//
function loadAccessibilitySettings()
{
	var settingsStr = readCookie("accessibilitySettings");
	if (settingsStr) {
		var splitStr = settingsStr.split('&');
		for (var i=0; i<splitStr.length; i++) {
			var splitStrKeyVal = splitStr[i].split('=');
			if (splitStrKeyVal.length == 2) {
				if ((splitStrKeyVal[0] == 'wordNavigationLink') && (splitStrKeyVal[1] == 'true')) {
					accessibilitySettings.wordNavigationLink = true;
				}
			}
		}
	}
	else {
		createCookie('accessibilitySettings', $.param(accessibilitySettings), 30);
	}
} // loadAccessibilitySettings

// Sauvegarde les modifications si besoin dans la variable globale accessibilitySettings.
// Recharge la page si nécessaire.
//
function saveAccessibilitySettings()
{
	if ($('#accessibiliteHypertexte').prop('checked') != accessibilitySettings.wordNavigationLink) {
		accessibilitySettings.wordNavigationLink = $('#accessibiliteHypertexte').prop('checked');
		
		createCookie('accessibilitySettings', $.param(accessibilitySettings), 30);
		
		window.location.reload(true);
	}
} // saveAccessibilitySettings

function initAccessibility()
{
	// Icône Accessibilité
	//
	$('#accessibilite').mouseover(function() {
		$('#accessibilite[activated="0"] a img').attr('src', $('#accessibilite a img').attr('src').replace('accessibilite.png', 'accessibilite_survol.png'));
	});
	$('#accessibilite a').focus(function() {
		$('#accessibilite[activated="0"] a img').attr('src', $('#accessibilite a img').attr('src').replace('accessibilite.png', 'accessibilite_survol.png'));
	});

	$("#accessibilite").mouseout(function() {
		$('#accessibilite[activated="0"] a img').attr('src', $('#accessibilite a img').attr('src').replace('_survol', ''));
	});
	$('#accessibilite a').focusout(function() {
		$('#accessibilite[activated="0"] a img').attr('src', $('#accessibilite a img').attr('src').replace('_survol', ''));
	});
	
	// Si ce réglage est activé, on met l'icône d'accessibilité en rendu activé (symbolisé par l'attribut activated à 1).
	//
	if (accessibilitySettings.wordNavigationLink) {
		$('#accessibilite a img').attr('src', $('#accessibilite a img').attr('src').replace('accessibilite.png', 'accessibilite_survol.png'));
		//$('#accessibilite').addClass('activated');
		$('#accessibilite').attr('activated', '1');
	}
	else {
		$('#accessibilite').attr('activated', '0');
	}
	
	// Fenêtre options accessibilité
	//
	$("#accessibilite").click(function() {
		// Si une autre "modale" (partage d'un article) est ouverte, on la ferme.
		//
		if ($('#divPartageIcones').hasClass('divPartageIconesOuvert')) {
			$("#btFermerPartage").click();
		}
		$("#accessibiliteFen").addClass("accessibiliteFenOuvert");
		$("#accessibiliteFen").removeClass("accessibiliteFenFermer");
		$('#accessibiliteHypertexte').focus();
	});
	$('#btFermerAccessibilite, #accessibiliteFen .boutonAccessibilite').click(function() {
		$("#accessibiliteFen").addClass("accessibiliteFenFermer");
		$("#accessibiliteFen").removeClass("accessibiliteFenOuvert");
		saveAccessibilitySettings();
		$('#accessibilite a').focus();
	});
	
	$('#infoAccessibilite a').focus(function() {
		$('#infoAccessibilite a img').attr('src', $("#infoAccessibilite a img").attr('src').replace('infoAccessibilite', 'infoAccessibiliteSurvol'));
	});
	$('#infoAccessibilite a').focusout(function() {
		$('#infoAccessibilite a img').attr('src', $("#infoAccessibilite a img").attr('src').replace('Survol', ''));
	});
	
	$('#accessibiliteHypertexte').prop('checked', accessibilitySettings.wordNavigationLink);
	
	// On peut cocher ou décocher en appuyant sur entrée.
	//
	$('#accessibiliteHypertexte').keydown(function ($event) {
		if ($event.which == 13) {
			$(this).click();
		}
	});
} // initAccessibility

jQuery(document).ready(function ($) {
	loadAccessibilitySettings();
	
    initAccessibility();
    
    $(document).keydown(function($event) {
		// Dès qu'on tape sur escape, on ferme la fenêtre.
		//
		if ($event.which == 27) {
			if ($('#accessibiliteFen').hasClass('accessibiliteFenOuvert')) {
				$("#btFermerAccessibilite").click();
			}
		}
	});
});