
function loadEntry(iUrl)
{
	var aJqxhr = $.get(iUrl).done(function(iData) {
		var $data = $(iData);

		$('#versions').empty().append($('#versions', $data).children());
		//$('#colCentre').replaceWith($('#colCentre', $data));
		$('#colCentre').empty().append($('#colCentre', $data).children());
		$('#colDroite').replaceWith($('#colDroite', $data));

		findNodeWithTexte();
		highlightDomaines()
		highlightLangues();
		
		// Notifie les autres javascript qu'une nouvelle entrée est chargée
		//
		$(document).trigger('entry.ready');
		
		DiagDicoNavigation.addLookUpWordHandler('.s_Article, .s_Notice');
		
		interceptNavigation();
	})
	.fail(function() {
		alert("error");
	});
} // loadEntry

//Intercepte les navigations vers une entrée pour garder le client sur la page des résultats.
//
function interceptNavigation()
{
	// Pour l'instant, les liens vers les autres éditions, le voisinage alphabétique, historique de consultation
	//
	$('#versions a, #voisinage a, #HistoireMot a, #Historique a').click(function ($event) {
		var urlEntry = $(this).attr('href');
		
		if (urlEntry) {
			$event.preventDefault();
			$event.stopPropagation();
			loadEntry(urlEntry);
		}
	});
}

// Coche les valeurs d'une liste déroulante personnalisée. Les valeurs à cocher dans la liste déroulante
// sont stockées dans le contenu d'un élément de la page.
// iListSelector : sélecteur de la liste déroulante (ou parent proche).
// iValuesSelector : sélecteur de l'élément qui stocke les valeurs à sélectionner.
//
function checkCustomList(iListSelector, iValuesSelector)
{
	var selectedValues = $(iValuesSelector).text().split(',');
	
	for (var i = 0; i < selectedValues.length; i++) {
		$(iListSelector +' input[value="'+ selectedValues[i] +'"]').prop('checked', true);
		
		// On déclenche le change pour que l'état indeterminate soit bien géré.
		//
		$(iListSelector +' input[value="'+ selectedValues[i] +'"]').trigger('change');
	}
} // checkCustomList


function findNodeWithTexte()
{
	var allForm      = document.querySelector('#chk_allform').checked; // $("#chk_allform").attr("value");

	// HighLight du mot en texte integral
	// [BUG#7215] : toutes les apostrophes dans le texte sont des courbes, du coup on fait le remplacement 
	var aText = $("#rechIntegral").attr("value").replace(/'/g, "’");
	
	if (aText != undefined && aText.length != 0) {
		var textToHL = [];

		var nbQuot = [...aText.matchAll(new RegExp('"', 'gi'))];
		if (nbQuot.length %2 == 0) {

			for(var i = 0; i < nbQuot.length; i+=2) {
				var sub = aText.slice(nbQuot[i].index+1, nbQuot[i+1].index);
				textToHL.push(sub)
			}

			for(var i = 0; i < textToHL.length; i++) {
				aText = aText.replace(textToHL[i], '');
			}
			// on vire tous les guillemets restant
			aText = aText.replace(/""/g, '');
		}

		var splitText = aText.replace(/\s+/g, " ").trim().split(" ");

		for (var i = 0; i< splitText.length; i++) {
			if (splitText[i].trim().length > 0) {
				textToHL.push(splitText[i]);
				
				if (allForm) {
					// call PL
					$.ajax({ 
						url: "../v1/flexion?word="+ splitText[i], 
						async: false,
						dataType: 'json',
						success: function(iData) {
							textToHL = textToHL.concat(iData);
	            		}
			        }); 
				}
			}
		}
		// Suppression des mots vide ""
		textToHL = textToHL.filter(e => e !== '');
		// Suppression des doublons
		textToHL = [...new Set(textToHL)];
		// On ordonne par taille decroissante, évite certains pb avec les accents, (cf cas part plus loin) 
		textToHL.sort(function(a,b) {return b.length-a.length;});
		//
		//console.log(textToHL);

		// Recuperation des info du formulaire de la recherche avancée
		var searchedText = '.';
			// Texte Intégral
		var isSensitive  = document.querySelector('#chk_casesensitive').checked; //$("#chk_casesensitive").attr("value");
		var rechDans     = $("#rechDans option:selected").attr("value");
		// Recherche par domaines
		//domaines
		// Recherche par entrées
		//var ignoreAccent = $("#chk_noaccent2").attr("value");
		// Recherche par catégore gammaticale
		//var rechCatGram  = $("#rechCatGram option:selected").text();
		// recherche dans l'étymologie
		//var rechEtymologieDatation  = $("#rechEtymologieDatation option:selected").text();
		// langues
		// Filtre par éditions (meme si ça quasi sur de rien mettre en valeur)
		//var filtreEditions  = $("#filtreEditions option:selected").text();

		if (!isSensitive) {
			aText = aText.toLowerCase();
			searchedText = "translate(., 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz')";
		}

		// Creation des requetes xpath :
		var ignoreClass = "not(contains(@class, 's_ZoneLiens')) and not(contains(@class, 's_ZoneHom')) and not(contains(@class, 's_ZoneOrtho')) and not(contains(@class, 's_Entree'))";

		var searchClass = "";

		// s_Def, s_zEtym, s_Exemple, s_Meta ne fonctionne que pour la 9e édition
		// s_im pour exemple dans édition 1 a 8
		// pour définition c'est le reste
		// meta et etym, rien pour édtion 1 a 8
		switch(rechDans){
		case "def": {
			searchClass = "(contains(@class, 's_Def') or contains(@class, 's_Corps')) and ";
		} break;
		case "etym": {
			searchClass = "contains(@class, 's_zEtym') and ";		
		} break;
		case "ex": {
			searchClass = "(contains(@class, 's_Exemple') or contains(@class, 's_im')) and ";
		} break;
		case "meta": {
			searchClass = "contains(@class, 's_Meta') and ";
		} break;
		default: break;
		}


		for (var t=0; t<textToHL.length;t++) {
			var aTextToHL = textToHL[t].replace(/\*/gi, ".*?");

			var contains = " and contains("+searchedText+", '"+ aTextToHL +"')";
			if (textToHL[t].indexOf("*") != -1) {
				// la fonction document.evaluate(xpath) ne fonctionne pas avec des expressions regulieres "compliqué"
				// .*? lui est inconnu
				contains = "";//" and contains("+searchedText+", /"+ aTextToHL +"/g)";
			}
			else if (textToHL[t].indexOf("?") != -1 && textToHL[t].length > 1) {
				var aT1 = aTextToHL.replace(/\?/g, "");
				var aT2 = aTextToHL.replace(/\?/g, ".");
				contains = " and (contains("+searchedText+", '"+ aT1 +"') or contains("+searchedText+",  /"+ aT2 +"/g) )";
				
				aTextToHL = aTextToHL.replace(/\?/g, ".?");
			}
			
			//test 1
			//var xpath = document.evaluate("//div[contains(@class, 's_Article')]/*["+ignoreClass +"]//*["+ searchClass + ignoreClass + contains +"]//text()",document, null, XPathResult.ANY_TYPE, null );
			// test 2 -> suppression de /*["+ignoreClass +"]
			var xpath = document.evaluate("//div[contains(@class, 's_Article')]//*["+ searchClass + ignoreClass + contains +"]//text()", document, null, XPathResult.ANY_TYPE, null );

			var nodes = [];
			while(request = xpath.iterateNext()) { nodes.push(request);}
			
			// si le contains ne donne rien, on prend plus large
			if (nodes.length == 0) {
				xpath = document.evaluate("//div[contains(@class, 's_Article')]//*["+ searchClass + ignoreClass +"]//text()",document, null, XPathResult.ANY_TYPE, null );
				while(request = xpath.iterateNext()) { nodes.push(request);}
			}
			
			//console.log(nodes.length);
			for (var i = 0; i < nodes.length; i++) {
				var flag = 'g';
				if(!isSensitive) { flag += 'i';	}
				
				// [BUG#7214] : Il se trouve que les mots commençant par des accents ne sont pas trouvé avec l'entité \b devant...
				// une spécificité du langage JS -> https://stackoverflow.com/questions/5436824/matching-accented-characters-with-javascript-regexes
				// il y a un lien vers le standard JS en ce qui concerne les RegExp qui explique pk...
				// Afin de trouvé les mots qui commence par un accent, on ne cherche plus avec \b
				// (évidemment, il en va de même pour les mots finissant par un accent...)
				//
				var startDecalAccent = "\\b";
				var endDecalAccent   = "\\b";
				if (/^[\u00C0-\u017F]/.test(aTextToHL)) startDecalAccent = "";
				if (/[\u00C0-\u017F]$/.test(aTextToHL)) endDecalAccent   = "";
				
				
				if (nodes[i].nodeType == Node.TEXT_NODE) {
					if (nodes[i].textContent.trim().length > 0) {		
						var span = document.createElement("span");
						var txt  = document.createTextNode(nodes[i].textContent);
						
						span.append(txt)
	
						//console.log(nodes[i].parentElement.className + " -> " + nodes[i].textContent);
						
						var occurrences = [...nodes[i].textContent.matchAll(new RegExp(startDecalAccent+aTextToHL+endDecalAccent, flag))]
						
						//Boolean(nodes[i].closest(".s_Entree"))
						if (   !Boolean(nodes[i].parentElement.closest(".s_Entree")) 
							&& !Boolean(nodes[i].parentElement.closest(".s_ZoneLiens"))
							&& !Boolean(nodes[i].parentElement.closest(".s_ZoneHom"))
							&& !Boolean(nodes[i].parentElement.closest(".s_ZoneOrtho"))
							&& !Boolean(nodes[i].parentElement.closest(".advsearch_hightlight")) // pour ne pas boucler sur ce qui est deja fait
							&& occurrences.length > 0) {
							nodes[i].parentElement.replaceChild(span, nodes[i])
							highlightText(aTextToHL, span, occurrences);
						}
					}
				}
				else {
					//console.log(nodes[i]);
					//console.log(nodes[i].outerHTML);
					var occurrences = [...nodes[i].textContent.matchAll(new RegExp("\\b"+startDecalAccent+aTextToHL+endDecalAccent+"\\b", flag))]
					
					highlightText(aTextToHL, nodes[i], occurrences);
				}	
			}
		}
		
		/*
		if ($(".advsearch_hightlight").length > 0) {
			$([document.documentElement, document.body]).animate({
		        scrollTop: $(".advsearch_hightlight:first-child").offset().top - 200
		    }, 2000);
		}*/

	}
}

function highlightText(aText, aNode, occurrences) {
	// find all occurence of aText in aNode
	var textNode = aNode.textContent;

	var newChildren = [];

	if (occurrences.length != 0) {
		var aParentNode = aNode.parentNode;
		var aDocument = aNode.ownerDocument;

		for(var i = 0; i < occurrences.length; i++) {
			var oc = occurrences[i];

			if (i == 0) {
				var aTextBefore = aDocument.createTextNode(textNode.substr(0, oc.index));
				if (aTextBefore.length != 0) {
					newChildren.push(aTextBefore);
				}
			}

			var aHighlightedText  = aDocument.createTextNode(oc[0]);
			var aHighlightElement = aDocument.createElement("span");

			aHighlightElement.setAttribute("class", "advsearch_hightlight");
			aHighlightElement.appendChild(aHighlightedText);
			//
			newChildren.push(aHighlightElement);

			var aTextAfter;
			if (i == occurrences.length -1) {
				aTextAfter = aDocument.createTextNode(textNode.substr(oc.index+oc[0].length));
			}
			else {
				aTextAfter = aDocument.createTextNode(textNode.substr(oc.index+oc[0].length, occurrences[i+1].index - (oc.index + oc[0].length)));
			}

			if (aTextAfter.length != 0) {
				newChildren.push(aTextAfter);
			}
		}

		// Empty node 
		aNode.textContent = "";
		// Populate Node
		for (var c = 0; c < newChildren.length; c++) {
			aNode.appendChild(newChildren[c]);		
		}
	}
}

function highlightDomaines() {
	
	var domainMap = $("#hid_domainsMap").val();
	
	if (domainMap && domainMap.length > 0) {
		var values = JSON.parse(domainMap);
				
		for(var i = 0; i < values.length; i++) {
			$(".s_Marq[refid*='"+ values[i] +"'], .s_dom[refid*='"+ values[i] +"']").addClass("advsearch_hightlight");
			//$("span[refid='"+ values[i] +"']").parent(".s_Marq").addClass('advsearch_hightlight')
		}
	}
}

function highlightLangues(){

	var langMap = $("#hid_langsMap").val();
	
	if (langMap && langMap.length > 0) {
		var values = JSON.parse(langMap);
		
		for(var i = 0; i < values.length; i++) {
			$(".s_Lang[refid*='"+ values[i] +"']").addClass("advsearch_hightlight");
			//$("span[refid='"+ values[i] +"']").parent(".s_Lang").addClass('advsearch_hightlight')
		}
	}
	
}






$(document).ready(function() {
	$('#resultatRechAvancee a').on('click', function ($event) {
		var urlEntry = $(this).attr('href');

		if (urlEntry) {
			$event.preventDefault();
			$event.stopPropagation();

			$('#resultatRechAvancee .colGaucheMotActif').addClass('colGaucheMot').removeClass('colGaucheMotActif');
			$('.colGaucheMot', $(this)).addClass('colGaucheMotActif').removeClass('colGaucheMot');
			loadEntry(urlEntry);
		}
	});
	
	// Bandeau Col Gauche
	//
	$('.sectionToggleGauche').click(function($event) {
		$event.stopPropagation();
		$(this).next('.listColGauche').slideToggle(400);
		var urlImg = $('img', $(this)).attr('src');
		var $btn = $('button', $(this));
		
		$(this).toggleClass("activeRotGauche");
		$(this).toggleClass("activeRotDroite");
		
		// On change le sens de l'image de la fléche.
		//
		if (urlImg.search('sectionOuvert') != -1){
			urlImg = urlImg.replace('sectionOuvert', 'sectionFerme');
			$btn.attr('aria-expanded', 'false');
			$btn.attr('title', 'Déplier');
		}
		else {
			urlImg = urlImg.replace('sectionFerme', 'sectionOuvert');
			$btn.attr('aria-expanded', 'true');
			$btn.attr('title', 'Replier');
		}
		$('img', $(this)).first().attr('src', urlImg);
	});

	if ($('#resultatRechAvancee a').length > 0) {
		// Ouverture de la première section, sélection du premier résultat.
		//
		$('#colGaucheResultat .sectionToggleGauche').first().click();
		$('#resultatRechAvancee a').first().click();
	}
	else {
		// Message pas de résultat
		//
		$('#sansResultat').addClass('divSansResultatsIconesOuvert').removeClass('divSansResultatsIconesFermer');
		$('#colCentreWrapper, #colGaucheRechAv, #colDroite').addClass('transparent');
	}
	
	checkCustomList('#divRechParDomaines', '#checked_domain');
	checkCustomList('#divRechCatGrammaticale', '#checked_grammcat');
	checkCustomList('.rechEtymologieLangue', '#checked_lang');
	
	$('#sansResultat #btFermerSansResultat').click(function () {
		$('#sansResultat').addClass('divSansResultatsIconesFermer').removeClass('divSansResultatsIconesOuvert');
		$('#colCentreWrapper, #colGaucheRechAv, #colDroite').removeClass('transparent');
	});
	
	// Désactivation du bouton partage.
	//
	$('#partage').prop('disabled', true);
	
	// Bouton de fermeture des résultats de recherche.
	// On navigue vers la page de l'entrée en cours de consultation.
	//
	$('#fermerColGauche').on('click', function() {
		var anEntryId = $('#txt_entry').val();
		
		if (anEntryId) {
			window.location.href = '../article/'+ encodeURIComponent(anEntryId);
		}
	});
	
	// Bandeau de resultat de recherche avancée
	//
	$('.blocBandeauResultat').click(function($event) {
		if ($(window).width() <= 600) {
			$event.stopPropagation();
			toggleOuvrirFermer(this);
		}
	});
	
	$(window).resize(function() {
		// La taille 600 est choisie arbitrairement (voir handle '.blocBandeauResultat')
		//
		if ($(window).width() > 600) {
			if($('#colGaucheResultat').is(':hidden')) {
				// On peut penser que les résultat ont été caché par l'utilisateur en affichage mobile.
				// On appelle la fonction qui affiche.
				//
				toggleOuvrirFermer($('.blocBandeauResultat'));
			}
		}
	});
});

function makeResultLabel(iResult) {
	var aLabel = iResult.label + (iResult.nbhomograph == "" ? "" : " [" + iResult.nbhomograph + "]");
	aLabel += iResult.nature == "" ? "" : ", " + iResult.nature;
	return aLabel
}

function handleSearchSuccess(data, textStatus, jqXHR) {
	var $resultList = $("#rechercheautocomplete-list");
	if ($.isArray(data.result) && $resultList.data("term") != $("#recherche").val()) {
		var tabResults = data.result;
		$(">a,.noresult", $resultList).remove();
		if (tabResults.length > 0)
			for (var i = 0; i < tabResults.length; i++) $resultList.append('<a href="' + tabResults[i].url + '" data-score="' + tabResults[i].score + '">' + makeResultLabel(tabResults[i]) + "</a>");
		else if (tabResults.length == 0 && data.term != "") $resultList.append('<div class="noresult">Il n\'y a pas de résultat pour cette recherche.</div>');
		$resultList.data("term", data.term);
		$resultList.show()
	}
}

function initToobarButton() {
	$("#partage").click(function() {
		if ($("#accessibiliteFen").hasClass("accessibiliteFenOuvert")) $("#btFermerAccessibilite").click();
		$("#divPartageIcones").addClass("divPartageIconesOuvert");
		$("#divPartageIcones").removeClass("divPartageIconesFermer")
	});
	$("#btFermerPartage").click(function() {
		$("#divPartageIcones").addClass("divPartageIconesFermer");
		$("#divPartageIcones").removeClass("divPartageIconesOuvert")
	});
	$(".boutonCopierPartage").click(function() {
		$("#urlcopie").select();
		document.execCommand("copy");
		$("#divPartageIcones").addClass("divPartageIconesFermer");
		$("#divPartageIcones").removeClass("divPartageIconesOuvert");
		if ($("#menuCentrale").hasClass("ouvert")) setTimeout(function() {
			$("#plus").trigger("click")
		}, 300)
	});
	$("#partageMobile").click(function() {
		$("#divPartageIcones").addClass("divPartageIconesOuvert");
		$("#divPartageIcones").removeClass("divPartageIconesFermer")
	});
	$("#imprime").click(function() {
		window.print()
	})
}

function overInfoRecherche(iIsAcceuil) {
	if (iIsAcceuil) {
		$("#recherche").addClass("rechercheFocusStyle");
		$("#explicationRecherche").addClass("afficher");
		$("#infoRechercheAccueil img").attr("src", $("#infoRechercheAccueil img").attr("src").replace("rechercheInfoAccueil@3x", "rechercheInfo2Accueil@3x"))
	} else {
		$("#recherche").addClass("rechercheFocusStyle");
		$("#explicationRecherche").addClass("afficher");
		$("#infoRecherche img").attr("src", $("#infoRecherche img").attr("src").replace("rechercheInfo@3x", "rechercheInfo2@3x"))
	}
}

function outInfoRecherche(iIsAcceuil) {
	if (iIsAcceuil) {
		$("#recherche").removeClass("rechercheFocusStyle");
		$("#explicationRecherche").removeClass("afficher");
		$("#infoRechercheAccueil img").attr("src", $("#infoRechercheAccueil img").attr("src").replace("rechercheInfo2Accueil@3x", "rechercheInfoAccueil@3x"))
	} else {
		$("#recherche").removeClass("rechercheFocusStyle");
		$("#explicationRecherche").removeClass("afficher");
		$("#infoRecherche img").attr("src", $("#infoRecherche img").attr("src").replace("rechercheInfo2@3x",
			"rechercheInfo@3x"))
	}
}

function createCookie(name, value, days) {
	var expires = "";
	if (days) {
		var date = new Date;
		date.setTime(date.getTime() + days * 24 * 60 * 60 * 1E3);
		expires = "; expires=" + date.toGMTString()
	}
	document.cookie = name + "=" + value + expires + "; path=/"
}

function eraseCookie(name) {
	createCookie(name, "", -1)
}

function readCookie(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(";");
	for (var i = 0; i < ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == " ") c = c.substring(1, c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length)
	}
	return null
}

function acceptCookies(response) {
	var responseValue = "0";
	if (response == true) {
		responseValue = "1";
		if (typeof ga == "function" && window["trackerId"] != "undefined") {
			ga("create", window["trackerId"], "auto");
			ga("send", "pageview")
		}
		if (typeof _paq != "undefined") _paq.push(["setConsentGiven"])
	}
	createCookie("acceptCookies", responseValue, 360)
}

function saveFontSize(fontSize) {
	var acceptCookie = readCookie("acceptCookies");
	if (acceptCookie != null && acceptCookie == "1") createCookie("fontSize", fontSize, 30)
}

function initFontSize(currentSize) {
	var index = 2;
	var pourcentageStep = ["80%", "90%", "105%", "115%", "130%", "150%", "200%"];
	var pourcentageStepMobile = ["90%", "105%", "130%"];
	for (var i = 0; i < pourcentageStep.length; i++)
		if (pourcentageStep[i] == currentSize) {
			index = i;
			break
		} var previousIndex = function(index, length) {
		if (index <= 0) return length - 1;
		else return index - 1
	};
	var nextIndex = function(index, length) {
		return (index + 1) % length
	};
	var changeFontSize = function(fontSize) {
		$(".s_Article, .s_Notice, .a_Annexe").css("font-size", fontSize);
		$(".s_Entree_haut").css("font-size", "130%");
		$("#colDroite").css("font-size", fontSize);
		$("#colGaucheRechAv").css("font-size", fontSize);
		saveFontSize(fontSize)
	};
	$("#txPlus, #txPlusMenuCentrale").on("click", function() {
		if (pourcentageStep[index] != pourcentageStep[pourcentageStep.length - 1]) {
			index = nextIndex(index, pourcentageStep.length);
			changeFontSize(pourcentageStep[index])
		}
	});
	$("#txMoins, #txMoinsMenuCentrale").on("click", function() {
		if (pourcentageStep[index] != pourcentageStep[0]) {
			index = previousIndex(index,
				pourcentageStep.length);
			changeFontSize(pourcentageStep[index])
		}
	});
	var changeFontSizeMobile = function(fontSize) {
		$(".s_Article, .s_Notice, .a_Annexe").css("font-size", fontSize);
		if ($("#menuCentrale").hasClass("ouvert")) setTimeout(function() {
			$("#plus").trigger("click")
		}, 300);
		saveFontSize(fontSize)
	};
	$("#iconesOutilsMobile #typoPetit").on("click", function() {
		changeFontSizeMobile(pourcentageStepMobile[0])
	});
	$("#iconesOutilsMobile #typoMoyen").on("click", function() {
		changeFontSizeMobile(pourcentageStepMobile[1])
	});
	$("#iconesOutilsMobile #typoGrand").on("click", function() {
		changeFontSizeMobile(pourcentageStepMobile[2])
	});
	$("#taillesTypoAccessibilite #typoPetit").on("click", function() {
		index = 2;
		changeFontSize(pourcentageStep[index])
	});
	$("#taillesTypoAccessibilite #typoMoyen").on("click", function() {
		index = 4;
		changeFontSize(pourcentageStep[index])
	});
	$("#taillesTypoAccessibilite #typoGrand").on("click", function() {
		index = 6;
		changeFontSize(pourcentageStep[index])
	})
}
$(document).on("entry.ready", function() {
	$("#plus").on("click", function() {
		$("#vertical").toggleClass("ouvert");
		$("#menuCentrale").toggleClass("ouvert");
		$("#contenu").toggleClass("fermer");
		$("footer").toggleClass("cellulaire")
	});
	var cssAccessClass = "navy";
	$("." + cssAccessClass).each(function() {
		if ($(this).children().length > 0) {
			$(this).removeAttr("tabindex");
			$(this).removeClass(cssAccessClass)
		} else $(this).keypress(function(event) {
			var keycode = event.keyCode ? event.keyCode : event.which;
			if (keycode == "13") {
				var newRange =
					document.createRange();
				newRange.setStart(this.firstChild, 0);
				newRange.setEnd(this.firstChild, 0);
				var selection = window.getSelection();
				selection.removeAllRanges();
				selection.addRange(newRange);
				$(this).click()
			}
		})
	});
	$("a ." + cssAccessClass).each(function() {
		$(this).removeAttr("tabindex");
		$(this).removeClass(cssAccessClass)
	})
});
jQuery(document).ready(function($) {
	$("#infoRechercheAccueil").mouseover(function() {
		overInfoRecherche(true)
	}).on("focus", function() {
		overInfoRecherche(true)
	});
	$("#infoRechercheAccueil").mouseout(function() {
		outInfoRecherche(true)
	}).on("focusout", function() {
		outInfoRecherche(true)
	});
	$("#btViderRecherche, #btViderRechercheAccueil").on("focus", function() {
		$("img", $(this)).attr("src", $("img", $(this)).attr("src").replace("rechercheEffacer@3x", "rechercheEffacerBlanc@3x"))
	});
	$("#btViderRecherche, #btViderRechercheAccueil").on("focusout",
		function() {
			$("img", $(this)).attr("src", $("img", $(this)).attr("src").replace("rechercheEffacerBlanc@3x", "rechercheEffacer@3x"))
		});
	$("#btLoupeAv, #imgLoupeAvAccueil").on("focus", function() {
		$("img", $(this)).attr("src", $("img", $(this)).attr("src").replace("loupeAv.png", "loupeAvActif.png"))
	});
	$("#btLoupeAv, #imgLoupeAvAccueil").on("focusout", function() {
		$("img", $(this)).attr("src", $("img", $(this)).attr("src").replace("loupeAvActif.png", "loupeAv.png"))
	});
	$("#fermeture, #fermetureAccueil").on("focus", function() {
		$("img",
			$(this)).attr("src", $("img", $(this)).attr("src").replace("xFermeture.png", "xFermetureActif.png"))
	});
	$("#fermeture, #fermetureAccueil").on("focusout", function() {
		$("img", $(this)).attr("src", $("img", $(this)).attr("src").replace("xFermetureActif.png", "xFermeture.png"))
	});
	$("#informations, #informationsAccueil").on("focus", function() {
		$("img", $(this)).attr("src", $("img", $(this)).attr("src").replace("informations.png", "informationsActif.png"))
	});
	$("#informations, #informationsAccueil").on("focusout", function() {
		$("img",
			$(this)).attr("src", $("img", $(this)).attr("src").replace("informationsActif.png", "informations.png"))
	});
	$("#logoAF a").on("focus", function() {
		$("#logoAF a img").addClass("logoHighlight")
	});
	$("#logoAF a").on("focusout", function() {
		$("#logoAF a img").removeClass("logoHighlight")
	});
	$("#precedent a").on("focus", function() {
		$("#precedent").addClass("precedentFocus")
	});
	$("#precedent a").on("focusout", function() {
		$("#precedent").removeClass("precedentFocus")
	});
	$("#suivant a").on("focus", function() {
		$("#suivant").addClass("suivantFocus")
	});
	$("#suivant a").on("focusout", function() {
		$("#suivant").removeClass("suivantFocus")
	});
	$("#frm_search").submit(function($event) {
		$event.preventDefault();
		if ($("#recherche").val() == "") {
			$("#rechercheautocomplete-list>a, #rechercheautocomplete-list .noresult").remove();
			$("#rechercheautocomplete-list").show()
		} else if ($("#recherche").val() != $("#rechercheautocomplete-list").data("term")) $.ajax({
			type: "POST",
			url: $("#frm_search").attr("action"),
			dataType: "json",
			data: $.param($("#frm_search input"))
		}).done(handleSearchSuccess);
		else if ($("#rechercheautocomplete-list > a").length > 0) {
			var url = $("#rechercheautocomplete-list > a.selected").attr("href");
			if (!url) url = $("#rechercheautocomplete-list > a").first().attr("href");
			if (url) document.location.href = url
		}
	});
	$("#recherche").keyup(function($event) {
		if (($("#recherche").val().length > 2 || $("#rechercheautocomplete-list:visible").length > 0) && $("#recherche").val() != $("#rechercheautocomplete-list").data("term")) $("#frm_search").submit();
		else if ($("#rechercheautocomplete-list:visible").length >
			0)
			if ($event.which == 40 || $event.which == 38) {
				var $itemSelected = $("#rechercheautocomplete-list > a.selected");
				if ($event.which == 40)
					if ($itemSelected.length > 0) {
						$itemSelected.next().addClass("selected");
						$itemSelected.removeClass("selected")
					} else $("#rechercheautocomplete-list > a").first().addClass("selected");
				else if ($event.which == 38)
					if ($itemSelected.length > 0) {
						$itemSelected.prev().addClass("selected");
						$itemSelected.removeClass("selected")
					} else $("#rechercheautocomplete-list > a").last().addClass("selected");
				$itemSelected = $("#rechercheautocomplete-list > a.selected");
				if ($itemSelected.get(0).offsetTop < $("#rechercheautocomplete-list").scrollTop() || $itemSelected.get(0).offsetTop + $itemSelected.height() > $("#rechercheautocomplete-list").scrollTop() + $("#rechercheautocomplete-list").get(0).clientHeight) $("#rechercheautocomplete-list").scrollTop($itemSelected.get(0).offsetTop)
			}
	});
	$("#btViderRecherche, #btViderRechercheAccueil").click(function() {
		$("#recherche").val("");
		$("#rechercheautocomplete-list").hide();
		$("#recherche").focus()
	});
	$("#motCommencant, #motsProches").click(function($event) {
		var $aTarget = $($event.currentTarget);
		$event.preventDefault();
		if (!$aTarget.is(".btRechercheActive")) {
			var aTitleCompl = " (filtre actif)";
			$("#search_options").val($aTarget.data("value"));
			$("#rechercheautocomplete-list .btRechercheActive").removeClass("btRechercheActive").addClass("btRechercheDisponible");
			$aTarget.addClass("btRechercheActive").removeClass("btRechercheDisponible");
			$("#rechercheautocomplete-list .btRechercheActive a").attr("title",
				$("#rechercheautocomplete-list .btRechercheActive a").attr("title") + aTitleCompl);
			$("#rechercheautocomplete-list .btRechercheDisponible a").attr("title", $("#rechercheautocomplete-list .btRechercheActive a").attr("title").replace(aTitleCompl, ""));
			$("#rechercheautocomplete-list").data("term", "");
			$("#recherche").focus();
			$("#frm_search").submit()
		}
	});
	$("#recherche").focus();
	$(".menuAnim").on("click", function() {
		$(".menuIcone").toggleClass("is-clicked");
		if ($(this).attr("aria-expanded") == "false") {
			$(this).attr("aria-expanded",
				"true");
			$(this).attr("title", "Fermer le menu")
		} else {
			$(this).attr("aria-expanded", "false");
			$(this).attr("title", "Ouvrir le menu")
		}
		$(".transform").toggleClass("transform-active");
		$(".menuTopCell").toggleClass("ouvertureMenuCell")
	});
	if ($("#blocCookies").length > 0) {
		$("#blocCookies .boutonCookies").first().focus();
		$("#blocCookies .boutonCookies").click(function($event) {
			var $aTarget = $($event.currentTarget);
			var aResponse = $aTarget.data("response");
			$event.preventDefault();
			if (aResponse == "1") acceptCookies(true);
			else if (aResponse == "0") acceptCookies(false);
			$("#blocCookies").fadeOut()
		})
	}
	$("#txMoins").mouseover(function() {
		$(this).addClass("picto-itemFocus")
	}).mouseout(function() {
		$(this).removeClass("picto-itemFocus")
	}).click(function() {
		$(this).removeClass("picto-itemFocus")
	}).focus(function() {
		$(this).addClass("picto-itemFocus")
	}).focusout(function() {
		$(this).removeClass("picto-itemFocus")
	});
	$("#txPlus").mouseover(function() {
		$(this).addClass("picto-itemFocus")
	}).mouseout(function() {
		$(this).removeClass("picto-itemFocus")
	}).click(function() {
		$(this).removeClass("picto-itemFocus")
	}).focus(function() {
		$(this).addClass("picto-itemFocus")
	}).focusout(function() {
		$(this).removeClass("picto-itemFocus")
	});
	$("#imprime").mouseover(function() {
		$(this).addClass("picto-itemFocus")
	}).mouseout(function() {
		$(this).removeClass("picto-itemFocus")
	}).click(function() {
		$(this).removeClass("picto-itemFocus")
	}).focus(function() {
		$(this).addClass("picto-itemFocus")
	}).focusout(function() {
		$(this).removeClass("picto-itemFocus")
	});
	$("#partage").mouseover(function() {
		$(this).addClass("picto-itemFocus")
	}).mouseout(function() {
		$(this).removeClass("picto-itemFocus")
	}).click(function() {
		$(this).removeClass("picto-itemFocus")
	}).focus(function() {
		$(this).addClass("picto-itemFocus")
	}).focusout(function() {
		$(this).removeClass("picto-itemFocus")
	});
	var aFontSize = readCookie("fontSize");
	if (aFontSize) $(".s_Article, .s_Notice, .colGaucheFond .a_Annexe").css("font-size", aFontSize);
	else aFontSize = "";
	initFontSize(aFontSize);
	initToobarButton();
	var tabPreloadImg = ["images/logoAFHighlight.png", "images/logoAFMobileHighlight.png", "images/rechercheInfo2Accueil@3x.png", "images/rechercheInfo2@3x.png", "images/loupeFocus.png", "images/logoAFMobileHighlight.png", "images/logoAFHighlight.png", "images/infoAccessibiliteSurvol.png", "images/accessibilite_survol.png",
		"images/xFermerInfoBulleSurvol.png"
	];
	var basePath = $("#preload").data("basepath");
	if (typeof basePath != "undefined")
		for (var i = 0; i < tabPreloadImg.length; i++) $("<img />").attr("src", basePath + tabPreloadImg[i]);

	function fermerNiveaux() {
		if ($("#rechDomaines ul li.niveauxUN > ul").hasClass("ouvert")) {
			$("#rechDomaines ul li.niveauxUN > ul").removeClass("ouvert");
			$("#rechDomaines ul li.niveauxUN").removeClass("fondBleu");
			$("#rechDomaines ul li.niveauxUN > label").removeClass("sectionActive");
			$("#rechDomaines ul li.niveauxUN > label").removeClass("active")
		}
		if ($("#rechDomaines ul li.niveauxUN > div").hasClass("rotation")) $("#rechDomaines ul li.niveauxUN > div").removeClass("rotation");
		if ($("#rechDomaines ul li.niveauxDEUX > label").hasClass("active")) {
			$("#rechDomaines ul li.niveauxDEUX > label").removeClass("active");
			$("#rechDomaines ul li.niveauxDEUX").removeClass("fondBleu");
			$("#rechDomaines ul li.niveauxDEUX > label").removeClass("sectionActive");
			$("#rechDomaines ul li.niveauxDEUX > label").removeClass("active")
		}
		if ($("#rechDomaines ul li.niveauxDEUX > ul").hasClass("ouvert")) $("#rechDomaines ul li.niveauxDEUX > ul").removeClass("ouvert");
		if ($("#rechDomaines ul li.niveauxDEUX > div").hasClass("rotation")) $("#rechDomaines ul li.niveauxDEUX > div").removeClass("rotation");
		if ($("#rechDomaines ul li.niveauxDEUX").hasClass("active")) $("#rechDomaines ul li.niveauxDEUX").removeClass("active");
		if ($("#rechDomaines ul li.niveauxTROIS > label").hasClass("active")) {
			$("#rechDomaines ul li.niveauxTROIS > label").removeClass("active");
			$("#rechDomaines ul li.niveauxTROIS").removeClass("fondBleu");
			$("#rechDomaines ul li.niveauxTROIS > label").removeClass("sectionActive");
			$("#rechDomaines ul li.niveauxTROIS > label").removeClass("active")
		}
		if ($("#rechDomaines ul li.niveauxTROIS > ul").hasClass("ouvert")) $("#rechDomaines ul li.niveauxTROIS > ul").removeClass("ouvert");
		if ($("#rechDomaines ul li.niveauxTROIS > div").hasClass("rotation")) $("#rechDomaines ul li.niveauxTROIS > div").removeClass("rotation");
		if ($("#rechDomaines ul li.niveauxTROIS").hasClass("active")) $("#rechDomaines ul li.niveauxTROIS").removeClass("active");
		if ($("#rechCatGram ul li.niveauxUN > ul").hasClass("ouvert")) {
			$("#rechCatGram ul li.niveauxUN > ul").removeClass("ouvert");
			$("#rechCatGram ul li.niveauxUN").removeClass("fondBleu");
			$("#rechCatGram ul li.niveauxUN > label").removeClass("sectionActive");
			$("#rechCatGram ul li.niveauxUN > label").removeClass("active")
		}
		if ($("#rechCatGram ul li.niveauxUN > div").hasClass("rotation")) $("#rechCatGram ul li.niveauxUN > div").removeClass("rotation");
		if ($("#rechCatGram ul li.niveauxDEUX > label").hasClass("active")) {
			$("#rechCatGram ul li.niveauxDEUX > label").removeClass("active");
			$("#rechCatGram ul li.niveauxDEUX").removeClass("fondBleu");
			$("#rechCatGram ul li.niveauxDEUX > label").removeClass("sectionActive");
			$("#rechCatGram ul li.niveauxDEUX > label").removeClass("active")
		}
		if ($("#rechCatGram ul li.niveauxDEUX > ul").hasClass("ouvert")) $("#rechCatGram ul li.niveauxDEUX > ul").removeClass("ouvert");
		if ($("#rechCatGram ul li.niveauxDEUX > div").hasClass("rotation")) $("#rechCatGram ul li.niveauxDEUX > div").removeClass("rotation");
		if ($("#rechCatGram ul li.niveauxDEUX").hasClass("active")) $("#rechCatGram ul li.niveauxDEUX").removeClass("active");
		if ($("#rechCatGram ul li.niveauxTROIS > label").hasClass("active")) {
			$("#rechCatGram ul li.niveauxTROIS > label").removeClass("active");
			$("#rechCatGram ul li.niveauxTROIS").removeClass("fondBleu");
			$("#rechCatGram ul li.niveauxTROIS > label").removeClass("sectionActive");
			$("#rechCatGram ul li.niveauxTROIS > label").removeClass("active")
		}
		if ($("#rechCatGram ul li.niveauxTROIS > ul").hasClass("ouvert")) $("#rechCatGram ul li.niveauxTROIS > ul").removeClass("ouvert");
		if ($("#rechCatGram ul li.niveauxTROIS > div").hasClass("rotation")) $("#rechCatGram ul li.niveauxTROIS > div").removeClass("rotation");
		if ($("#rechCatGram ul li.niveauxTROIS").hasClass("active")) $("#rechCatGram ul li.niveauxTROIS").removeClass("active");
		if ($("#rechEtymoParLangue ul li.niveauxUN > ul").hasClass("ouvert")) {
			$("#rechEtymoParLangue ul li.niveauxUN > ul").removeClass("ouvert");
			$("#rechEtymoParLangue ul li.niveauxUN").removeClass("fondBleu");
			$("#rechEtymoParLangue ul li.niveauxUN > label").removeClass("sectionActive");
			$("#rechEtymoParLangue ul li.niveauxUN > label").removeClass("active")
		}
		if ($("#rechEtymoParLangue ul li.niveauxUN > div").hasClass("rotation")) $("#rechEtymoParLangue ul li.niveauxUN > div").removeClass("rotation");
		if ($("#rechEtymoParLangue ul li.niveauxDEUX > label").hasClass("active")) {
			$("#rechEtymoParLangue ul li.niveauxDEUX > label").removeClass("active");
			$("#rechEtymoParLangue ul li.niveauxDEUX").removeClass("fondBleu");
			$("#rechEtymoParLangue ul li.niveauxDEUX > label").removeClass("sectionActive");
			$("#rechEtymoParLangue ul li.niveauxDEUX > label").removeClass("active")
		}
		if ($("#rechEtymoParLangue ul li.niveauxDEUX > ul").hasClass("ouvert")) $("#rechEtymoParLangue ul li.niveauxDEUX > ul").removeClass("ouvert");
		if ($("#rechEtymoParLangue ul li.niveauxDEUX > div").hasClass("rotation")) $("#rechEtymoParLangue ul li.niveauxDEUX > div").removeClass("rotation");
		if ($("#rechEtymoParLangue ul li.niveauxDEUX").hasClass("active")) $("#rechEtymoParLangue ul li.niveauxDEUX").removeClass("active");
		if ($("#rechEtymoParLangue ul li.niveauxTROIS > label").hasClass("active")) {
			$("#rechEtymoParLangue ul li.niveauxTROIS > label").removeClass("active");
			$("#rechEtymoParLangue ul li.niveauxTROIS").removeClass("fondBleu");
			$("#rechEtymoParLangue ul li.niveauxTROIS > label").removeClass("sectionActive");
			$("#rechEtymoParLangue ul li.niveauxTROIS > label").removeClass("active")
		}
		if ($("#rechEtymoParLangue ul li.niveauxTROIS > ul").hasClass("ouvert")) $("#rechEtymoParLangue ul li.niveauxTROIS > ul").removeClass("ouvert");
		if ($("#rechEtymoParLangue ul li.niveauxTROIS > div").hasClass("rotation")) $("#rechEtymoParLangue ul li.niveauxTROIS > div").removeClass("rotation");
		if ($("#rechEtymoParLangue ul li.niveauxTROIS").hasClass("active")) $("#rechEtymoParLangue ul li.niveauxTROIS").removeClass("active");
		var rDomaines = document.querySelector("#rechDomaines");
		rDomaines.scrollTop = 0;
		var rCatGram = document.querySelector("#rechCatGram");
		rCatGram.scrollTop = 0;
		var rLangue = document.querySelector("#rechEtymoParLangue");
		rLangue.scrollTop =
			0
	}

	function fermerMenuRechDomaine() {
		$("#rechDomaines").toggleClass("options");
		$("#listeDomaine").next("ul").toggleClass("ouvert");
		$("#listeDomaine span").toggleClass("rotation");
		$("#listeDomaine").toggleClass("active");
		var rDomaines = document.querySelector("#rechDomaines");
		rDomaines.scrollTop = 0
	}

	function fermerMenuRechCatGram() {
		$("#rechCatGram").toggleClass("options");
		$("#listeCatGram").next("ul").toggleClass("ouvert");
		$("#listeCatGram span").toggleClass("rotation");
		$("#listeCatGram").toggleClass("active");
		var rCatGram = document.querySelector("#rechCatGram");
		rCatGram.scrollTop = 0
	}

	function fermerMenuRechListeLangue() {
		$("#rechEtymoParLangue").toggleClass("options");
		$("#listeLangue").next("ul").toggleClass("ouvert");
		$("#listeLangue span").toggleClass("rotation");
		$("#listeLangue").toggleClass("active");
		var rLangue = document.querySelector("#rechEtymoParLangue");
		rLangue.scrollTop = 0
	}
	window.addEventListener("click", function(e) {
		if (document.getElementById("rechDomaines").contains(e.target));
		else if ($("#listeDomaine").next("ul").hasClass("ouvert")) {
			fermerMenuRechDomaine();
			fermerNiveaux();
			var rDomaines = document.querySelector("#rechDomaines");
			rDomaines.scrollTop = 0
		}
		if (document.getElementById("rechCatGram").contains(e.target));
		else if ($("#listeCatGram").next("ul").hasClass("ouvert")) {
			fermerMenuRechCatGram();
			fermerNiveaux();
			var rCatGram = document.querySelector("#rechCatGram");
			rCatGram.scrollTop = 0
		}
		if (document.getElementById("rechEtymoParLangue").contains(e.target));
		else if ($("#listeLangue").next("ul").hasClass("ouvert")) {
			fermerMenuRechListeLangue();
			fermerNiveaux();
			var rLangue =
				document.querySelector("#rechEtymoParLangue");
			rLangue.scrollTop = 0
		}
	});
	$("#rechercheAvance #frm_search_Av #fermeture").on("click", function() {
		$("#rechercheAvance").removeClass("active");
		$("#btLoupeAv").removeClass("active");
		fermerNiveaux()
	});
	$("#rechercheFixed #contenantRecherche #btLoupeAv").on("click", function() {
		$("#btLoupeAv").toggleClass("active");
		$("#rechercheAvance").toggleClass("active");
		if ($("#rechercheAvance").hasClass("active")) fermerNiveaux();
		if ($("#sansResultat").hasClass("divSansResultatsIconesOuvert")) {
			$("#sansResultat").removeClass("divSansResultatsIconesOuvert");
			$("#sansResultat").addClass("divSansResultatsIconesFermer")
		}
	});

	function checkFrmAdvSearch() {
		if ($("#filtreEditions").data("forced") || $("#rechIntegral").val() != "" || $("#rechEntrees").val() != "" || $('input[name="chk_domains"]:checked').length > 0 || $('input[name="chk_gramcat"]:checked').length > 0) {
			$("#btRechercher").prop("disabled", false).addClass("criteresActifs");
			return true
		}
		$("#btRechercher").prop("disabled", true).removeClass("criteresActifs");
		return false
	}
	$('#rechDans, #rechEtymologieDatation, input[name="chk_langs"]').on("change",
		function() {
			if ($("#rechDans").val() != "" || $("#rechEtymologieDatation").val() != "" || $('input[name="chk_langs"]:checked').length > 0) {
				$("#filtreEditions").val("9").prop("disabled", true).data("forced", true);
				$("#chk_appear, #chk_disappear").prop("disabled", true)
			} else {
				$("#filtreEditions").prop("disabled", false).removeData("forced");
				$("#chk_appear, #chk_disappear").prop("disabled", false)
			}
			$("#filtreEditions").trigger("change")
		});
	$("#filtreEditions").on("change", function() {
		switch ($(this).val()) {
			case "0":
				$("#chk_appear, #chk_disappear").prop("disabled",
					true);
				break;
			case "1":
				$("#chk_appear").prop("disabled", true);
				$("#chk_disappear").prop("disabled", false);
				break;
			case "9":
				if (!$(this).data("forced")) {
					$("#chk_appear").prop("disabled", false);
					$("#chk_disappear").prop("disabled", true)
				}
				break;
			default:
				$("#chk_appear").prop("disabled", false);
				$("#chk_disappear").prop("disabled", false)
		}
		checkFrmAdvSearch()
	});
	$("#rechIntegral, #rechEntrees").on("keyup", function() {
		checkFrmAdvSearch()
	});
	$("#rechIntegral, #rechEntrees").on("change", function() {
		checkFrmAdvSearch()
	});
	$('#frm_search_Av input[name="chk_domains"], #frm_search_Av input[name="chk_gramcat"]').on("change", function() {
		checkFrmAdvSearch()
	});
	$("#frm_search_Av").on("submit", function($event) {
		if (!checkFrmAdvSearch()) {
			$event.preventDefault();
			$event.stopPropagation()
		}
	});
	$("#btReinitialiser").on("click", function($event) {
		$event.preventDefault();
		$("#rechIntegral, #rechEntrees").val("");
		$('#frm_search_Av input[type="checkbox"]').prop("checked", false);
		$("#frm_search_Av select option:selected").prop("selected", false);
		$("#chk_allform").prop("checked", true);
		$("#filtreEditions").val("9");
		$("#rechDans").trigger("change")
	});
	$("#rechDans").trigger("change");
	$(".elementUnMenu").on("click", function() {
		$(this).next("ul").toggleClass("ouvert");
		$(this).children("span").toggleClass("rotation");
		$("li").removeClass("fondBleu");
		$("label").removeClass("sectionActive");
		$(this).parent().toggleClass("options");
		$(this).children("div").toggleClass("rotation")
	});
	$("#listeDomaine").on("click", function() {
		/*$(this).parent().toggleClass("options");
		$(this).next("ul").toggleClass("ouvert");*/
		$(this).children("span").toggleClass("rotation");
		if ($("#rechCatGram").hasClass("options")) fermerMenuRechCatGram();
		if ($("#rechEtymoParLangue").hasClass("options")) fermerMenuRechListeLangue()
	});
	$("#listeCatGram").on("click", function() {
		$(this).parent().toggleClass("options");
		$(this).next("ul").toggleClass("ouvert");
		$(this).children("span").toggleClass("rotation");
		if ($("#rechDomaines").hasClass("options")) fermerMenuRechDomaine();
		if ($("#rechEtymoParLangue").hasClass("options")) fermerMenuRechListeLangue()
	});
	$("#listeLangue").on("click", function() {
		$(this).parent().toggleClass("options");
		$(this).next("ul").toggleClass("ouvert");
		$(this).children("span").toggleClass("rotation");
		if ($("#rechDomaines").hasClass("options")) fermerMenuRechDomaine();
		if ($("#rechCatGram").hasClass("options")) fermerMenuRechCatGram()
	});
	$("#rechDomaines ul li.niveauxUN > label").on("click", function() {
		$(this).toggleClass("active");
		$(this).toggleClass("sectionActive");
		$(this).next("ul").toggleClass("ouvert");
		$(this).parent().children("div").toggleClass("rotation");
		$(this).parent("li").toggleClass("fondBleu")
	});
	$("#rechDomaines ul li.niveauxUN > div.fa").on("click", function() {
		$(this).next("label").toggleClass("active");
		$(this).next("label").toggleClass("sectionActive");
		$(this).next().next("ul").toggleClass("ouvert");
		$(this).toggleClass("rotation");
		$(this).parent("li").toggleClass("fondBleu")
	});
	$("#rechDomaines ul li.niveauxDEUX > label").click(function() {
		$(this).toggleClass("active");
		$(this).toggleClass("sectionActive");
		$(this).next("ul").toggleClass("ouvert");
		$(this).parent().children("div").toggleClass("rotation");
		$(this).parent("li").toggleClass("fondBleu")
	});
	$("#rechDomaines ul li.niveauxDEUX > div.fa").click(function() {
		$(this).next("label").toggleClass("active");
		$(this).next("label").toggleClass("sectionActive");
		$(this).next().next("ul").toggleClass("ouvert");
		$(this).toggleClass("rotation");
		$(this).parent("li").toggleClass("fondBleu")
	});
	$("#rechDomaines ul li.niveauxTROIS > label").click(function() {
		$(this).toggleClass("active");
		$(this).toggleClass("sectionActive");
		$(this).next("ul").toggleClass("ouvert");
		$(this).parent().children("div").toggleClass("rotation");
		$(this).parent("li").toggleClass("fondBleu")
	});
	$("#rechDomaines ul li.niveauxTROIS > div.fa").click(function() {
		$(this).next("label").toggleClass("active");
		$(this).next("label").toggleClass("sectionActive");
		$(this).next().next("ul").toggleClass("ouvert");
		$(this).toggleClass("rotation");
		$(this).parent("li").toggleClass("fondBleu")
	});
	$("#rechCatGram ul li.niveauxUN > label").on("click", function() {
		$(this).toggleClass("active");
		$(this).toggleClass("sectionActive");
		$(this).next("ul").toggleClass("ouvert");
		$(this).parent().children("div").toggleClass("rotation");
		$(this).parent("li").toggleClass("fondBleu")
	});
	$("#rechCatGram ul li.niveauxUN > div.fa").on("click", function() {
		$(this).next("label").toggleClass("active");
		$(this).next("label").toggleClass("sectionActive");
		$(this).next().next("ul").toggleClass("ouvert");
		$(this).toggleClass("rotation");
		$(this).parent("li").toggleClass("fondBleu")
	});
	$("#rechCatGram ul li.niveauxDEUX > label").click(function() {
		$(this).toggleClass("active");
		$(this).toggleClass("sectionActive");
		$(this).next("ul").toggleClass("ouvert");
		$(this).parent().children("div").toggleClass("rotation");
		$(this).parent("li").toggleClass("fondBleu")
	});
	$("#rechCatGram ul li.niveauxDEUX > div.fa").click(function() {
		$(this).next("label").toggleClass("active");
		$(this).next("label").toggleClass("sectionActive");
		$(this).next().next("ul").toggleClass("ouvert");
		$(this).toggleClass("rotation");
		$(this).parent("li").toggleClass("fondBleu")
	});
	$("#rechCatGram ul li.niveauxTROIS > label").click(function() {
		$(this).toggleClass("active");
		$(this).toggleClass("sectionActive");
		$(this).next("ul").toggleClass("ouvert");
		$(this).parent().children("div").toggleClass("rotation");
		$(this).parent("li").toggleClass("fondBleu")
	});
	$("#rechCatGram ul li.niveauxTROIS > div.fa").click(function() {
		$(this).next("label").toggleClass("active");
		$(this).next("label").toggleClass("sectionActive");
		$(this).next().next("ul").toggleClass("ouvert");
		$(this).toggleClass("rotation");
		$(this).parent("li").toggleClass("fondBleu")
	});
	$("#rechEtymoParLangue ul li.niveauxUN > label").on("click",
		function() {
			$(this).toggleClass("active");
			$(this).toggleClass("sectionActive");
			$(this).next("ul").toggleClass("ouvert");
			$(this).parent().children("div").toggleClass("rotation");
			$(this).parent("li").toggleClass("fondBleu")
		});
	$("#rechEtymoParLangue ul li.niveauxUN > div.fa").on("click", function() {
		$(this).next("label").toggleClass("active");
		$(this).next("label").toggleClass("sectionActive");
		$(this).next().next("ul").toggleClass("ouvert");
		$(this).toggleClass("rotation");
		$(this).parent("li").toggleClass("fondBleu")
	});
	$("#rechEtymoParLangue ul li.niveauxDEUX > label").click(function() {
		$(this).toggleClass("active");
		$(this).toggleClass("sectionActive");
		$(this).next("ul").toggleClass("ouvert");
		$(this).parent().children("div").toggleClass("rotation");
		$(this).parent("li").toggleClass("fondBleu")
	});
	$("#rechEtymoParLangue ul li.niveauxDEUX > div.fa").click(function() {
		$(this).next("label").toggleClass("active");
		$(this).next("label").toggleClass("sectionActive");
		$(this).next().next("ul").toggleClass("ouvert");
		$(this).toggleClass("rotation");
		$(this).parent("li").toggleClass("fondBleu")
	});
	$("#rechEtymoParLangue ul li.niveauxTROIS > label").click(function() {
		$(this).toggleClass("active");
		$(this).toggleClass("sectionActive");
		$(this).next("ul").toggleClass("ouvert");
		$(this).parent().children("div").toggleClass("rotation");
		$(this).parent("li").toggleClass("fondBleu")
	});
	$("#rechEtymoParLangue ul li.niveauxTROIS > div.fa").click(function() {
		$(this).next("label").toggleClass("active");
		$(this).next("label").toggleClass("sectionActive");
		$(this).next().next("ul").toggleClass("ouvert");
		$(this).toggleClass("rotation");
		$(this).parent("li").toggleClass("fondBleu")
	});
	$('.menuPerso input[type="checkbox"], .menuPersoEtroit input[type="checkbox"]').change(function(e) {
		var checked = $(this).prop("checked"),
			container = $(this).parent(),
			siblings = container.siblings();
		container.find('input[type="checkbox"]').prop({
			indeterminate: false,
			checked: checked
		});

		function checkSiblings(el) {
			var parent = el.parent().parent(),
				all = true;
			el.siblings().each(function() {
				var returnValue = all = $(this).children('input[type="checkbox"]').prop("checked") ===
					checked;
				return returnValue
			});
			if (all && checked) {
				parent.children('input[type="checkbox"]').prop({
					indeterminate: false,
					checked: checked
				});
				checkSiblings(parent)
			} else if (all && !checked) {
				parent.children('input[type="checkbox"]').prop("checked", checked);
				parent.children('input[type="checkbox"]').prop("indeterminate", parent.find('input[type="checkbox"]:checked').length > 0);
				checkSiblings(parent)
			} else el.parents("li").children('input[type="checkbox"]').prop({
				indeterminate: true,
				checked: false
			})
		}
		checkSiblings(container)
	})
});