# Recommandations d'accessibilité

## Dictionnaire de l'Académie française Module de recherche avancée 

Itération : 2

Date d'inspection : 04/07/2022

Url : http://testv4.dictionnaire-academie.fr/

Auditeur : [Carsten Meyer](mailto:cm@mobyou.net)

Norme : Référentiel Général d'Amélioration de l'Accessibilité (RGAA) 4.1 - équivalent WCAG niveau AA


## Contexte

Un premier essai a été intégré sur le dépliement accessible au clavier des listes multi-niveaux.
Le dépliement/repliement du premier niveau fonctionne bien au clavier, en revanche le second niveau n'est pas encore fonctionnel avec ce paramétrage. D'autre part des bugs d'affichage apparaissent sur les cases à cocher.

Une modification du script est effectuée pour réaliser ce fonctionnement à plusieurs niveaux.



### Liste des domaines en premier niveau

#### Code actuel

* On a bien un bouton comme prévu;
* Le paramètre **aria-controls** pointe nulle part, or il doit s'agir de l'id de la liste principale qu'on va nommer **ulDomaines**.
* Il est nécessaire de prévoir la balise sémantique **fieldset**

```html
<button type="button" id="listeDomaine" class="disclosure" aria-expanded="true" aria-controls="ulDomaines" title="Utilisez cette liste déroulante et hiérarchique pour effectuer une recherche basée sur les marqueurs de domaines (mathématiques, philosophie…). Vous pouvez utiliser les «&nbsp;super-domaines&nbsp;» pour effectuer des recherches groupées.">Liste des domaines<span class="fa fa-chevron-right"></span></button>
<ul id="ulDomaines">...
```
#### Code proposé

* On entoure la liste d'une balise **fieldset** avec sa légende pour les **non-voyants**
* On ajoute un id "ulDomaines" à la liste à déplier, id qu'on cible avec aria-controls;
* Le script a11y.js a été corrigé pour éviter des bugs d'affichages sur les éléments enfants;
* Des paramétrages supplémentaires permettent d'activer ou non la touche ESC (data-escape="false" ici on souhaite l'activation d'ESC donc on ne met rien) , attribuer des classes au bloc parent (data-parent-class="options")  ou au paneau à déplier (data-panel-class="ouvert").

```html
<h3>Recherche par domaines :</h3>
  <fieldset>
	<legend class="sr-only">Liste hierarchique des domaines</legend>
	<div class="menuPerso" role="none" name="rechDomaines" id="rechDomaines">
		<buttontype="button" id="listeDomaine" class="disclosure" aria-expanded="false"
        data-parent-class="options" data-panel-class="ouvert" aria-controls="ulDomaines"title="Utilisez cette liste déroulante et hiérarchique pour effectuer une recherche basée sur les marqueurs de domaines (mathématiques, philosophie…). Vous pouvez utiliser les «&nbsp;super-domaines&nbsp;» pour effectuer des recherches groupées.">Liste des domaines<span class="fa fa-chevron-right"></span></button>
        <ul class="" id="ulDomaines">...</ul>
      </div>
    </fieldset>
```

### Liste des domaines de second niveau

#### Code actuel

```html
<li class="niveauxUN">
	<input id="chk_grpDomains-7" type="checkbox" name="chk_grpDomains" class="indeterminate-checkbox" aria-expanded="false" aria-controls="ulDomaines" style="visibility: visible;">
	<button type="button" class="fa fa-chevron-right disclosure"></button>
	<label class="elementsListe" for="chk_grpDomains-7">ALIMENTATION</label>
	<ul class="sous-groupe-liste-UN">
        <li class="niveauUn">
            <input id="chk_grpDomains-8" name="chk_domains" access="false" value="160" type="checkbox" class="indeterminate-checkbox">
            <label class="elementsListe" for="chk_grpDomains-8">Boucherie</label>
        </li>
        <li class="niveauUn">
            <input id="chk_grpDomains-9" name="chk_domains" access="false" value="161" type="checkbox" class="indeterminate-checkbox" style="visibility: visible;">
            <label class="elementsListe" for="chk_grpDomains-9">Boulangerie</label>
        </li>
        ...
    </ul>
</li>
```
#### Code proposé

Ici on précise qu'on ne souhaite pas l'activation de la touche 'ESC' puisqu'elle fonctionne pour les 2 niveaux, on ajoute donc  data-escape="false".

```html
<li class="niveauxUN">

    <input id="chk_grpDomains-7" type="checkbox" name="chk_grpDomains"
           class="indeterminate-checkbox"
           title="Sélectionnez toutes les thématiques ALIMENTATION">
    <h4>ALIMENTATION</h4>
    <button type="button" data-escape="false" data-parent-class="fondBleu"
            data-panel-class="ouvert" aria-expanded="false" aria-controls="ulSousDomaines1"
            class="fa fa-chevron-right disclosure"></button>

    <ul class="sous-groupe-liste-UN" id="ulSousDomaines1">
        <li class="niveauUn">
            <input id="chk_grpDomains-8" name="chk_domains" value="160" type="checkbox" class="indeterminate-checkbox">
            <label class="elementsListe" for="chk_grpDomains-8">Boucherie</label>
        </li>
        <li class="niveauUn">
            <input id="chk_grpDomains-9" name="chk_domains" value="161" type="checkbox" class="indeterminate-checkbox" style="visibility: visible;">
            <label class="elementsListe" for="chk_grpDomains-9">Boulangerie</label>
        </li>
		...
    </ul>
</li>



```

JavaScript et CSS proposés :

* [JavaScript du bouton d'ouverture](https://carstenmeyerweb.gitlab.io/a11y/at/academie/code/recherche-avancee-v2/js/a11y.js)
* [CSS adapté](https://carstenmeyerweb.gitlab.io/a11y/at/academie/code/recherche-avancee-v2/css/a11y.css)

#### Démonstration

Le script et le CSS ont été testés sur la recherche des domaines avec seulement le premier sous-niveau ALIMENTATION 

[Exemple d'implémentation d'un bouton de type plier/déplier ou disclosure dans la recherche par domaine](https://carstenmeyerweb.gitlab.io/a11y/at/academie/code/recherche-avancee-v2/recherche_avancee_access-2.html)



### Volet de résultats

#### Code actuel

```html
<div id="colGaucheResultat" class="sectionToggle">
    <div class="sectionToggleGauche" title="9e édition, 6 résultats">
        <p>9<sup>e</sup> édition<span> 6 résultats</span><img src="/images/sectionOuvert.png" alt="section" width="24" height="20"></p>
    </div>
    <ul class="listColGauche" style="">
```

#### Code proposé

```html
<div id="colGaucheResultat" class="sectionToggle">
    <button class="sectionToggleGauche disclosure" aria-expanded="true" aria-controls="listeColGauche" title="9e édition, 6 résultats">
        <span aria-hidden="true">9<sup>e</sup> édition<span> 6 résultats</span>
            <img class="pic-open" src="/images/sectionOuvert.png"
                 alt="" width="24" height="20">
            <img class="pic-closed" src="/images/sectionFerme.png"
                 alt="" width="24" height="20">
        </span>
    </button>
    <ul class="listColGauche" id="listeColGauche">


```

JavaScript et CSS proposés communs avec ceux utilisés pour la recherche avancée :

* [JavaScript du bouton d'ouverture](https://carstenmeyerweb.gitlab.io/a11y/at/academie/code/recherche-avancee-v2/js/a11y.js)
* [CSS adapté](https://carstenmeyerweb.gitlab.io/a11y/at/academie/code/recherche-avancee-v2/css/a11y.css)

#### Démonstration

[Exemple d'implémentation d'un bouton de type plier/déplier ou disclosure dans le volet résultat](https://carstenmeyerweb.gitlab.io/a11y/at/academie/code/recherche-avancee-v2/resultat_access-2.html)

### Ouverture/fermeture du bloc de recherche avancée

#### Code actuel

```html
<button type="button" title="Recherche avancée" id="btLoupeAv" class="active">
    <img id="imgLoupeAv" src="./images/loupeAv.png" alt="Recherche avancée" width="26" height="26">
    <div id="titreBtRechAv">RECHERCHE AVANCÉE</div>
</button>
...
<button type="button" id="fermeture" title="Fermer le formulaire de recherche avancée."><img
						src="./images/xFermeture.png" alt="Information" width="17" height="17"></button>

```

#### Code proposé

Ici on précise qu'on ne souhaite pas l'activation de la touche 'ESC' puisqu'elle fonctionne pour les 2 niveaux, on ajoute donc  data-escape="false".

```html
<button type="button" title="Recherche avancée" id="btLoupeAv" class="disclosure"
        aria-controls="rechercheAvance" aria-expanded="false" data-escape="false">
    <img id="imgLoupeAv" src="./images/loupeAv.png" alt="Recherche avancée" width="26" height="26">
    <div id="titreBtRechAv">RECHERCHE AVANCÉE</div>
</button>
...
<button class="close-disclosure" type="button" id="fermeture"
        title="Fermer le formulaire de recherche avancée."><img src="images/xFermeture.png"
                                                                alt="Fermer le formulaire de recherche avancée." width="17" height="17"></button>
```

JavaScript et CSS proposés communs avec ceux utilisés pour la recherche avancée :

* [JavaScript du bouton d'ouverture](https://carstenmeyerweb.gitlab.io/a11y/at/academie/code/recherche-avancee-v2/js/a11y.js)
* [CSS adapté](https://carstenmeyerweb.gitlab.io/a11y/at/academie/code/recherche-avancee-v2/css/a11y.css)

#### Démonstration

Url identique à la liste des domaines en premier niveau
[Exemple d'implémentation d'un bouton de type plier/déplier ou disclosure dans la recherche avancée](https://carstenmeyerweb.gitlab.io/a11y/at/academie/code/recherche-avancee-v2/recherche_avancee_access-2.html)

## Fenêtres de dialogue en cas d'erreur

### Anomalie

Lorsqu'une alerte apparaît, le focus ne se positionne pas sur celle-ci.

### Solution proposée

Le composant Dialog de la librairie JQuery UI est accessible. 

Nous recommandons cet exemple : 
https://jqueryui.com/dialog/#modal-form



## Support du zoom texte

### Barre de recherche

#### Anomalie

Sous Firefox en Zoom texte seulement à 200%, le bouton de recherche avancée chevauche la recherche simple.

#### Solution proposée

Utiliser un position relatif et flexible en remplacement du positionnement absolu du bouton de recherche avancée.
Cette modification ne concerne que la feuille de style.

### Scroll dans le panneau de recherche

#### Solution apportée dans le CSS de démo

```css
#rechercheAvance {
  width: 36em;
  height: 90vh;
  overflow: auto;
}
```


## Qualité du code

Le passage au validateur du W3C nous indique environ 660 erreurs à corriger. 

* L'attribut "access" dans les champs de case à cocher : il devient valide si on le préfixe avec data comme ceci :
```html
  	<input name="chk_domains" data-access="false" value="266" type="checkbox" class="indeterminate-checkbox" style="visibility: hidden;">
```

* Les étiquettes sont désormais bien reliées à leurs champs respectifs avec un paramètre "for" pointant l'id de ce champ.



Une image de décoration est dépourvue d'alternative :

```html
<img id="imgInfoRecherche" src="images/rechercheInfo@3x.png" width="30" height="30">
```
Correction proposée :

Un attribut alt vide permettra de ne pas vocaliser cette image de décoration
```html
<img alt="" id="imgInfoRecherche" src="images/rechercheInfo@3x.png" width="30" height="30">
```









