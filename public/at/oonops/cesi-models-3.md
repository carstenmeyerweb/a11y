# Recommandations d'accessibilité

## CESI modèles 08/06/2022

url : https://studio.stpo.fr/cesi/www/models/

Auditeur : [Carsten Meyer](mailto:cm@mobyou.net)

Référentiel : RGAA 4.1 - WCAG niveau AA





### 03 page-recherche 

https://studio.stpo.fr/cesi/www/models/03.page-recherche.php

#### Filtres

Il manque du contexte quand on tabule sur les boutons de suppression des filtres avec NVDA.

##### Solution

Ajouter l'intitulé du filtre dans le bouton

Texte actuel : Supprimer ce filtre

Texte recommandé : Supprimer le filtre Lycéen·ne ou étudiant·e

Idem pour les filtres de la page Actualités 07.liste

![Filtres de recherche](assets/filtres.png)



#### Bouton favori

L'intitulé pourrait restituer plus de contexte.

Les non-voyants n'auront toutefois pas le feedback de l'état "ajouté aux favoris"

##### Solution

Ajout du param aria-pressed=false par défaut et intitulé actuel "Ajouter aux favoris"

Lorsque le favori est ajouté, le bouton voit son param aria-pressed passer à true et son intitulé passer à "Supprimer ce favori"

Ajouter du contexte avec un aria-describedby qui pointerait vers l'id du titre.

![Bouton d'ajout au Favoris](assets/favoris.png)



### 04 - formulaire

https://studio.stpo.fr/cesi/www/models/04.formulaire.php

#### Checkbox d'acceptation des mentions légales

Comme pour le champ texte en démo d'erreur plus haut, j'ajouterais les params required avec un nouvel id pour le message d'erreur.



### 08 - liste-agenda

https://studio.stpo.fr/cesi/www/models/08.liste-agenda.php

#### Flèches mois précédent et suivant

Les flèches n'ont pas d'intitulé pertinent :

Ajouter un intitulé comme : "aller en juillet 2022"

![boutons mois](assets/agenda.png)

### 09 - module contact 2

https://studio.stpo.fr/cesi/www/models/09.module-contact-2.php

Il manque un fieldset avec legend entourant les boutons radios



 ### 09 - module contact 3

https://studio.stpo.fr/cesi/www/models/09.module-contact-3.php

#### Auto-suggestion

Dans le dernier RGAA il faut maintenant préciser l'attribut auto-complete pour les champs génériques nom et email.

* Champ votre nom : autocomplete="family-name"
* Champ email : autocomplete="email"

##### Pour les messages d'erreur de saisie de l'email

Le message d'erreur pour l'email est à expliciter, le message d'erreur de l'API HTML5 ne suffit plus pour le RGAA. Il faut utiliser la méthode de surcharge de cette API : https://developer.mozilla.org/en-US/docs/Web/API/HTMLObjectElement/setCustomValidity



```javascript

        const emailField = document.querySelector("#input-2");
        emailField.addEventListener("input", () => {
          emailField.setCustomValidity('');
          emailField.checkValidity();
        });
        emailField.addEventListener("invalid", () => {
          emailField.setCustomValidity('Veuillez saisir une adresse valide (exemple : nom@exemple.fr)');
        });
    

```



### 09 - module contact campus 3

https://studio.stpo.fr/cesi/www/models/09.module-contact-campus-3.php

#### iframe du Google maps

Ajouter un title en param de la balise iframe avec un intitulé pertinent du style :

"Carte Google du CESI"