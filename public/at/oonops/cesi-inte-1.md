# CESI : Checkup rapide RGAA inté



## Menus

* Ajouter un menu d'évitement disponible à la tabulation à placer avant le burger menu au début du header.

  * Aller au contenu
  * Aller à la navigation
  * Aller à la recherche

* Lien-logo-h1 CESI : prévoir cet intitulé pour préciser la destination du lien :

  * span.sr-only : "CESI - École d’ingénieurs - Accueil"
  * a[title] : "CESI - École d’ingénieurs - Accueil"
  * supprimer le aria-current sur le h1

* Lien EN :

  * Éviter la balise abbr ici car c'est un lien et les synthèses vocales ne vont pas forcément énoncer le title de l'<abbr> dans ce contexte :
    * span.sr-only[lang="en"] : "English"
    * span[aria-hidden="true"] : "EN"

  * Afficher ce lien dans la nav mobile

  

## Footer

* Liens partenaires : les alts devront être renseignés et s'ils ouvrent une nouvelle fenêtre il faudra un alt du style "Partenaire 1 (nouvelle fenêtre)"



