# CESI : Checkup rapide RGAA maquettes



## Menus

![image-20220411162537959](C:\Users\carsten.meyer\AppData\Roaming\Typora\typora-user-images\image-20220411162537959.png)

Dans tous les menus et le fil d'ariane distinguer visuellement autrement que par la couleur la rubrique ou la thématique en cours.

* Menu top : prévoir un liseré, une bordure ou un picto qui indique que c'est la thématique en cours de consultation.

* Menu secondaire et tertiaire : idem, prévoir un effet graphique en plus de la couleur pour signifier la page ou la rubrique en cours.

* Fil d'ariane : signifier les éléments cliquables et distinguer la page en cours (gras, surligné, picto...)

## Contrastes de couleurs

### Formulaire de recherche

![image-20220411163126575](C:\Users\carsten.meyer\AppData\Roaming\Typora\typora-user-images\image-20220411163126575.png)

Prévoir un rapport de contraste entre la bordure des champs et le fond du champ d'au moins 3, le violet est actuellement à 2.3:1. Il est possible de simplement ajouter une vraie bordure à ces champs.

### Sous-menus

![image-20220411164325103](C:\Users\carsten.meyer\AppData\Roaming\Typora\typora-user-images\image-20220411164325103.png)

Augmenter le contraste de la bordure des sous-menus à 3:1



### Filtres de recherche

![image-20220411164456271](C:\Users\carsten.meyer\AppData\Roaming\Typora\typora-user-images\image-20220411164456271.png)

Augmenter le contraste des flèches cliquables à au moins 3:1
