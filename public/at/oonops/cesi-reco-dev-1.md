# Recommandations d'accessibilité

## CESI modèles 10/06/2022

url : https://studio.stpo.fr/cesi/www/models/

Auditeur : [Carsten Meyer](mailto:cm@mobyou.net)

Référentiel : RGAA 4.1 - WCAG niveau AA



### Bouton favori

https://studio.stpo.fr/cesi/www/models/03.page-recherche.php

L'intitulé doit restituer son état actif ou non actif avec des alternatives textuelles et un paramétrage ARIA adapté.

*	aria-pressed (true/false) : précise l'état du bouton 
*	aria-describedby : restitue le titre de la formation en faisant référence à son id 
*	```<span class="sr-only">Ajouter aux favoris</span>``` : alternative pour les non-voyants

#### Bouton inactif (fond jaune)

##### HTML

```html

<button type="button" class="button button--primary button--icon" aria-pressed="false" aria-describedby="result-title-1">
    <span class="sr-only">Ajouter aux favoris</span>
    <span class="icon icon_like" aria-hidden="true"></span>
</button>

```

#### Bouton actif (fond noir)

##### HTML

```html

<button type="button" class="button button--primary button--icon button--selected" aria-pressed="true" aria-describedby="result-title-2">
    <span class="sr-only">Supprimer des favoris</span>
    <span class="icon icon_like" aria-hidden="true"></span>
</button>

```





### Saisie de l'email

https://studio.stpo.fr/cesi/www/models/09.module-contact-3.php

Le message d'erreur pour l'email proproser par l'API HTML est à expliciter selon le  RGAA. Il faut utiliser la méthode de surcharge de cette API : https://developer.mozilla.org/en-US/docs/Web/API/HTMLObjectElement/setCustomValidity


#### HTML

```html

<div class="form-item">
    <label for="input-email">Votre email&nbsp;<b class="required-mark">*</b></label>
    <input type="email" id="input-email" autocomplete="email" placeholder="" required="">
</div>

```

#### JavaScript

```javascript

const emailField = document.querySelector("#input-email");
emailField.addEventListener("input", () => {
    emailField.setCustomValidity('');
    emailField.checkValidity();
});
emailField.addEventListener("invalid", () => {
emailField.setCustomValidity('Veuillez saisir une adresse valide (exemple : nom@exemple.fr)');
});
    
```

#### Critère RGAA ciblé : 11.11

https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode-rgaa/criteres/#crit-11-11


