# Recommandations d'accessibilité

## CESI modele 07/05/2022

url : https://studio.stpo.fr/test2/models/

Auditeur : [Carsten Meyer](mailto:cm@mobyou.net)

Référentiel : RGAA 4.1 - WCAG niveau AA





### Header

#### Module de recherche

* Ajouter un role="search" dans la balise form

#### Fil d'ariane

* Supprimer le param **aria-labelledby="system-breadcrumb"**



### Tableau

* Prévoir un caption, c'est obligatoire
* Avec une classe **sr-only** si on ne veut rien afficher aux voyants.

```html
<table>
    <caption class="sr-only">Grille des équivalences</caption>
    <tr>...
```

### Carrousel

* Conforme. 
* Si des liens sont présents sur certaines slides, ils doivent l'être dans la partie prévue pour les non-voyants.



### Plier-déplier - faux accordéon

* Conforme.
* Le motif disclosure est adapté à un usage plier-déplier individuel. Si le client souhaite un fonctionnement en accordéon, il faudra utiliser un motif d'onglet beaucoup plus complexe.



### Structure de titres

* Conforme.



### Footer

#### Logo CESI

* Tu peux ajouter un title pour décrire le logo

  ```html
  <svg ...>
  	<title>Logo CESI, école d'ingénieurs</title>
      <path></path>
  </svg>
  ```

  

### Code HTML

#### Optimisation 

* Ajouter un type="button" à tous les boutons de menus et d'interaction dans la page. Objectif : éviter le bug du submit au clavier dans certaines configurations matérielles.



