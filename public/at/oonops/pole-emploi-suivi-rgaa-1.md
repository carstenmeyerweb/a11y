

# Recommandations d'accessibilité

## Fiches Pôle emploi 04/02/2022

url : local / ngrok

Auditeur : [Carsten Meyer](mailto:cm@mobyou.net)

Référentiel : RGAA 4.1 - WCAG niveau AA



### Liens

#### Lien logo

Prévoir un attribut alt équivalent au visuel : "Pôle emploi"

Option plus explicite :

Spécifier que la destination est bien l'accueil avec un  title sur le lien

```html
	<a href="/" title="Accueil fiches pôle emploi">
		<img src="" alt="Accueil fiches pôle emploi">
	</a>
```



#### Lien intranet

Signaler qu'il s'agit d'un lien en nouvelle fenêtre :

```html
	<a href="https://intranet" target="_blank" title="Intranet (ouverture en nouvelle fenêtre)">Intranet</a>
```



#### Navigation entre fiches en bas de page

Inclure cette navigation dans une balise nav

```html
<nav class="content" aria-label="accéder aux autres fiches">
    <ul class="list-links">
   ... 	
```



#### Liens d'évitement

Revoir le CSS pour que la cible du lien "Aller au contenu" ne soit pas cachée par le header fixe.

Impact majeur pour les personnes à mobilité réduite.



### Trieur de fiches

1. Réactiver le focus avec tabindex à 0.

2. Accentuer l'état focus sur les filtres avec la propriété CSS outline.

3. Ajouter du feedback pour les non-voyants :

   Après la sélection d'une option, ajouter une alerte de statut au dessus des résultats :

   ```html
   <p role="status" class="sr-only">Votre sélection de fiches :</p>
   ```

   À tester avec nos synthèses vocales, je pense que ça devrait suffire, si ce n'est pas le cas, il faudra peut-être déplacer le focus à la première fiche. On verra.



### Responsive design et zoom texte



Prévoir des points de rupture en taille relative pour associer le colonage au zoom texte :

```` CSS
/* mobile first */
.column {
    ...
}

@media (min-width: 48em) {   
    /* tablet > eq 768px */
    .column {
        ...
    }
}

@media (min-width: 75em) {
    /* desktop > eq 1200px */
    .column {
        ...
    }
}
````



Conserver des points de rupture en pixels uniquement pour ajuster des tailles de texte en mobile :

````css
@media (max-width: 767px) {   
    /* mobile font size */ 
   h1 {
        ...
    }
}
````

