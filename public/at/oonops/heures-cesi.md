

| Date       | Temps | Type                                                         | Détail                                                       |
| ---------- | ----- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| 2022-04-11 | 1     | Analyses maquettes                                           | Analyse et rédaction d'un checkup rapide                     |
| 2022-04-25 | 1.5   | Recommandation intégration du menu                           | Pattern disclosure                                           |
| 2022-04-26 | 2.5   | Recommandation intégration du menu                           | Calage des 3 menus dans une nav explicite                    |
| 2022-05-02 | 1.5   | Tests et correctifs HTML du menu                             | Tests desktop, test NVDA et VoiceOver, test mobile           |
| 2022-05-03 | 2     | Analyse du carrousel                                         | Analyse et test du carrousel, proposition d'alternative pour les non-voyants |
| 2022-05-07 | 2     | Analyse d'un modèle de page complet                          | Analyse et rédaction d'un court document de correctifs       |
| 2022-05-13 | 1     | Débug carrousel avec le menu                                 | Test et écoute, recommandation technique                     |
| 2022-05-16 | 0.5   | Débug carrousel avec le menu                                 | Test en configuration mobile                                 |
|            | 12h   | Facturé/payé                                                 |                                                              |
| 2022-06-08 | 2.5   | Recommandations sur l'intégration globale                    | Boutons favoris, formulaires                                 |
| 2022-06-10 | 0.5   | Recommandation pour les développeurs                         | Bouton favori, champ email                                   |
| 2022-06-20 | 2     | Vidéos de restitution clavier et NVDA, optims synthèse vocale |                                                              |
|            |       |                                                              |                                                              |
|            |       |                                                              |                                                              |
|            |       |                                                              |                                                              |
|            |       |                                                              |                                                              |
|            |       |                                                              |                                                              |
|            |       |                                                              |                                                              |
|            |       |                                                              |                                                              |
|            |       |                                                              |                                                              |
|            |       |                                                              |                                                              |
|            |       |                                                              |                                                              |
|            |       |                                                              |                                                              |
|            |       |                                                              |                                                              |
|            |       |                                                              |                                                              |
|            |       |                                                              |                                                              |
|            |       |                                                              |                                                              |
|            |       |                                                              |                                                              |
|            |       |                                                              |                                                              |
|            |       |                                                              |                                                              |
|            |       |                                                              |                                                              |
|            |       |                                                              |                                                              |
|            |       |                                                              |                                                              |
|            |       |                                                              |                                                              |
|            |       |                                                              |                                                              |
|            |       |                                                              |                                                              |
|            |       |                                                              |                                                              |
|            |       |                                                              |                                                              |
|            |       |                                                              |                                                              |
|            |       |                                                              |                                                              |
|            |       |                                                              |                                                              |
|            |       |                                                              |                                                              |
|            |       |                                                              |                                                              |
|            |       |                                                              |                                                              |
|            |       |                                                              |                                                              |
|            |       |                                                              |                                                              |
|            |       |                                                              |                                                              |
|            |       |                                                              |                                                              |

