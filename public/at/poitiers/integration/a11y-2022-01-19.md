# Recommandations d'accessibilité

## Design system 19/01/2022

Auditeur : [Carsten Meyer](mailto:cm@mobyou.net)

Référentiel : RGAA 4.1 - WCAG niveau AA

### Composants

#### Push évènement

https://poitiersfr.poi-linweb-03.sos-data.fr/components/detail/push-evt.html

Prévoir des paragraphes autour des liens.
Objectif : meilleure restitution par les synthèses vocales.
La classe p-link est arbitraire, il s'agit de prévoir des styles neutres pour cette mise en forme.

* Avant : 

    ```Html
	<p class="pushEvt__theme">Thématique</p>
    <a href="#" class="pushEvt__link">Rugby | Stade Poitevin - Sarcelles</a>
    <p class="pushEvt__date">Du 21 novembre au 29 décembre</p>
    ```


* Après : 

    ```html
    <p class="pushEvt__theme">Thématique</p>
    <p class="p-link"><a href="#" class="pushEvt__link">Rugby | Stade Poitevin - Sarcelles</a></p>
    <p class="pushEvt__date">Du 21 novembre au 29 décembre</p>
    ```

Idem pour L'agenda des sports

* Avant :

    ```html
    <a href="#" class="btn btn5">
        L&#x27;agenda des sports
    
        <svg class="icon" aria-hidden="true">
            <use href="../../assets/svg/sprite-global.svg#arrow" xlink:href="../../assets/svg/sprite-global.svg#arrow"></use>
        </svg>
    
    </a>
    ```
* Après :

    ```html
    <a href="#" class="btn btn5">
        <p class="p-link">L&#x27;agenda des sports</p>
        <svg class="icon" aria-hidden="true">
            <use href="../../assets/svg/sprite-global.svg#arrow" xlink:href="../../assets/svg/sprite-global.svg#arrow"></use>
        </svg>
    
    </a>
    ```



#### Push actu

https://poitiersfr.poi-linweb-03.sos-data.fr/components/detail/push-actu--default.html

Idem push évènement

* Avant :

  ```html
  <a href="#" class="pushActu__link">Ceci est un push actu format XL</a>
  ```

* Après

  ```html
  <p class="p-link"><a href="#" class="pushActu__link">Ceci est un push actu format XL</a></p>
  ```
  



#### Push

https://poitiersfr.poi-linweb-03.sos-data.fr/components/detail/push.html

1. Ajouter du contexte au lien avec **aria-label**, par exemple, en utilisant le contenu de  **p.push__title**.
2. Comme précédemment, ajouter un paragraphe autour du lien.

* Avant :
  ```html
  	  <p class="push__title">L’assemblée citoyenne</p>
  
        <p class="push__desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non risus. Suspendisse lectus tortor, dignissim sit amet, adipiscing nec, ultricies sed, dolor. Cras elementum ultrices diam. Maecenas ligula massa.</p>
  
        <a href="#" class="btn btn4">
            En savoir plus
  
            <svg class="icon" aria-hidden="true">
                <use href="../../assets/svg/sprite-global.svg#arrow" xlink:href="../../assets/svg/sprite-global.svg#arrow"></use>
            </svg>
  
        </a>
  ```
  
* Après :
  
  * Solution la plus appropriée :
  
  ```html
  	  <p class="push__title">L’assemblée citoyenne</p>
  
        <p class="push__desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non risus. Suspendisse lectus tortor, dignissim sit amet, adipiscing nec, ultricies sed, dolor. Cras elementum ultrices diam. Maecenas ligula massa.</p>
  
        <a href="#" class="btn btn4" aria-label="En savoir plus sur l'assemblée citoyenne">
            <p class="p-link">En savoir plus</p>
  
            <svg class="icon" aria-hidden="true">
                <use href="../../assets/svg/sprite-global.svg#arrow" xlink:href="../../assets/svg/sprite-global.svg#arrow"></use>
            </svg>
  
        </a>
  ```
  
  * Solution alternative :
  
  ```html
  	  <p class="push__title" id="Pushtitle-125">L’assemblée citoyenne</p>
  
        <p class="push__desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non risus. Suspendisse lectus tortor, dignissim sit amet, adipiscing nec, ultricies sed, dolor. Cras elementum ultrices diam. Maecenas ligula massa.</p>
  
        <a href="#" class="btn btn4" aria-label="En savoir plus sur" aria-describedby="Pushtitle-125">
            <p class="p-link">En savoir plus</p>
  
            <svg class="icon" aria-hidden="true">
                <use href="../../assets/svg/sprite-global.svg#arrow" xlink:href="../../assets/svg/sprite-global.svg#arrow"></use>
            </svg>
  
        </a>
  ```



#### Push grands projets

https://poitiersfr.poi-linweb-03.sos-data.fr/components/detail/push-grands-projets.html

Idem push évènement, prévoir une balise p pour entourer le lien



#### Push conseil

https://poitiersfr.poi-linweb-03.sos-data.fr/components/detail/push.html

Prévoir plus de contexte dans les liens pour les personnes non-voyantes.

* Avant :

  ```html  
	<p class="pushConseil__title">
        <span class="pushConseil__subtitle">
                <svg class="icon" aria-hidden="true">
                    <use href="../../assets/svg/sprite-global.svg#calendrier-check" xlink:href="../../assets/svg/sprite-global.svg#calendrier-check"></use>
                </svg>
                Prochain conseil
        </span>
  
        Le mercredi 8 décembre 2021
        <span class="pushConseil__desc">à l’Hôtel de ville de Poitiers</span>
    </p>
    
    <p class="pushConseil__link"><a href="#" class="btn btn4">
                Lire l&#x27;ordre du jour
    
                <svg class="icon" aria-hidden="true">
                    <use href="../../assets/svg/sprite-global.svg#arrow" xlink:href="../../assets/svg/sprite-global.svg#arrow"></use>
                </svg>
    
            </a>
    </p>
    
    <p class="pushConseil__link"><a href="#" class="btn btn4">
                Visionner le conseil en vidéo
    
                <svg class="icon" aria-hidden="true">
                    <use href="../../assets/svg/sprite-global.svg#arrow" xlink:href="../../assets/svg/sprite-global.svg#arrow"></use>
                </svg>
    
            </a>
    </p>
  ```
  
* Après :

  ```html  
	<p class="pushConseil__title">
        <span class="pushConseil__subtitle">
                <svg class="icon" aria-hidden="true">
                    <use href="../../assets/svg/sprite-global.svg#calendrier-check" xlink:href="../../assets/svg/sprite-global.svg#calendrier-check"></use>
                </svg>
                Prochain conseil
        </span>
  
        Le mercredi 8 décembre 2021
        <span class="pushConseil__desc">à l’Hôtel de ville de Poitiers</span>
    </p>
    
    <p class="pushConseil__link"><a href="#" class="btn btn4" aria-label="Lire l&#x27;ordre du jour du prochain conseil du mercredi 8 décembre 2021">
                Lire l&#x27;ordre du jour
    
                <svg class="icon" aria-hidden="true">
                    <use href="../../assets/svg/sprite-global.svg#arrow" xlink:href="../../assets/svg/sprite-global.svg#arrow"></use>
                </svg>
    
            </a>
    </p>
    
    <p class="pushConseil__link"><a href="#" class="btn btn4" aria-label="Visionner le conseil en vidéo du prochain conseil du mercredi 8 décembre 2021">
                Visionner le conseil en vidéo
    
                <svg class="icon" aria-hidden="true">
                    <use href="../../assets/svg/sprite-global.svg#arrow" xlink:href="../../assets/svg/sprite-global.svg#arrow"></use>
                </svg>
    
            </a>
    </p>
  ```



### Sections

#### À la une

https://poitiersfr.poi-linweb-03.sos-data.fr/components/detail/a-la-une.html

* **Responsive et zoom texte** : corriger le placement du titre "À la une"
  ![Screenshot de l'accueil avec un zoom texte de 150%](assets/section-une-1.jpg)

* Risque de **contraste insuffisant** en superposition de zone claire

  * Fiabiliser les cas d'usages sans image : appliquer un fond gris sous l'image de Poitiers si l'utilisateur désactive les images ou si la connexion est lente, hors résolutions mobiles.
    Exemple de gris contrastant avec le blanc de la police : **#737373**

  * Pour limiter les risques avec une image trop claire, ajouter un contour de texte à l'ombré pour obtenir un contraste > 3 sur les coins supérieurs des hampes, de l'accent et du "u".

    ![](assets/section-une-2.jpg)

    Solution de contour en CSS désormais compatible pour tous les navigateurs actuels :
    ```css
    #une .section__title {
      -webkit-text-stroke: 1px rgba(5, 5, 5, 0.45);
    }
    ```

  * Conserver le rendu prévu pour le mobile (sapin sans ombre)

